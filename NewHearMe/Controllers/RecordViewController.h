//
//  RecordViewController.h
//  NewHearMe
//
//  Created by IUILab Air on 2016/3/5.
//  Copyright © 2016年 IUILAB. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <AudioToolbox/AudioToolbox.h>
#import <CoreLocation/CoreLocation.h>
//#import <GoogleAnalytics/GAI.h>
//#import <GoogleAnalytics/GAIDictionaryBuilder.h>
#import "SavingView.h"

@interface RecordViewController : UIViewController<AVAudioRecorderDelegate, AVAudioPlayerDelegate, CLLocationManagerDelegate, UIAlertViewDelegate, SavingViewDelegate>

@property (nonatomic) BOOL isPaused;
@property (strong, nonatomic) AVAudioRecorder *recorder;
@property (strong, nonatomic) AVAudioPlayer *audioPlayer;
@property (strong, nonatomic) SavingView *savingView;

- (void)magicStart;

@end
