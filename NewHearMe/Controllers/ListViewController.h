//
//  ListViewController.h
//  NewHearMe
//
//  Created by IUILab Air on 2016/3/6.
//  Copyright © 2016年 IUILAB. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <AudioToolbox/AudioToolbox.h>
//#import <GoogleAnalytics/GAI.h>
//#import <GoogleAnalytics/GAIDictionaryBuilder.h>
//#import <GoogleAnalytics/GAIFields.h>
#import "ListTableViewCell.h"
#import "NavigationView.h"
#import "BottomToolBar.h"

#define Time_before_Preview 0.4
#define Time_before_Play    1.8
#define Time_for_Sound      0.6
#define Time_for_Content    4.0

@interface ListViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, AVAudioPlayerDelegate, ListTableViewCellDelegate>

@property (weak, nonatomic) IBOutlet NavigationView *navigationView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet BottomToolBar *bottomToolBar;

@property (strong, nonatomic) AVAudioPlayer* audioPlayer;
@property (strong, nonatomic) NSTimer* previewTimer;
@property (strong, nonatomic) NSTimer* timer;
@property (strong, nonatomic) NSIndexPath* editIndex;
@property (strong, nonatomic) NSString* viewTitle;
@property (strong, nonatomic) NSString* tempSpeech;
@property (strong, nonatomic) NSString* trackTag;
@property (strong, nonatomic) NSString *fromView;

@property int previewTime;
@property int timeCount;
@property BOOL isSpeaking;
@property BOOL isBreakSend;
@property BOOL hasNearest;
@property NSTimeInterval contentTime;

@property (strong, nonatomic) NSMutableArray* items;
@property (strong, nonatomic) NSMutableArray* itemTypes;
@property (strong, nonatomic) NSMutableArray* itemTitles;
@property (strong, nonatomic) NSMutableArray* itemSubs;
@property (strong, nonatomic) NSMutableArray* itemCode;
@property (strong, nonatomic) NSMutableArray* itemFiles;
@property (strong, nonatomic) NSMutableArray* itemID;
@property (strong, nonatomic) NSMutableArray* itemVoice;
@property (strong, nonatomic) NSMutableArray* itemGroup;

- (void)resetBtnPreview;
- (void)playIntro:(NSURL*)url;
- (void)playFile:(SoundFile*)file;
- (void)playSound;

- (void)showEditActions;
- (void)showShareActions;
- (void)showAlertDelete;
- (void)showAlertRename;

@end
