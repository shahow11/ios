//
//  DateMainViewController.m
//  NewHearMe
//
//  Created by IUILab Air on 2016/3/5.
//  Copyright © 2016年 IUILAB. All rights reserved.
//

#import "DateMainViewController.h"
#import "DateEachViewController.h"
#import "FileViewController.h"
#import "FMDatabase.h"
#import "FMDatabaseAdditions.h"
#import "SoundFile.h"
#import "UIUtility.h"

@interface DateMainViewController ()
{
    FMDatabase *db;
    
    int refreshFlag;    
}

@end

@implementation DateMainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.trackTag = @"date";
    self.navigationView.lblViewTitle.text = NSLocalizedString(@"Date", nil);
    
    // 搜尋資料庫
    NSArray *pathsToDocuments = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [pathsToDocuments objectAtIndex:0];
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"HearMe.db"];
    db = [FMDatabase databaseWithPath:filePath];
    if (![db open]) {
        NSLog(@"Could not open db.");
    }
    
    refreshFlag = 0;
    
    [self refreshData];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (refreshFlag)
        [self refreshData];
    refreshFlag ++;
}

- (void)viewDidAppear:(BOOL)animated
{
    /*
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:SCREEN_NAME_DATE];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    */
    [super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Local functions

- (void)refreshData
{
    self.items = [[NSMutableArray alloc] init];
    self.itemTypes = [[NSMutableArray alloc] init];
    self.itemTitles = [[NSMutableArray alloc] init];
    self.itemSubs = [[NSMutableArray alloc] init];
    self.itemCode = [[NSMutableArray alloc] init];
    
    // 整理所有檔案日期
    NSDateFormatter *dateFormatter = [UIUtility dateFormatterYYMMdd];
    NSDateFormatter *subDateFormatter = [UIUtility dateFormatterMMMM_dd];
    
    NSString *today = [dateFormatter stringFromDate:[NSDate date]];
    NSString *yesterday = [dateFormatter stringFromDate:[[NSDate date] dateByAddingTimeInterval:-(24*60*60)]];
    NSString *last2day = [dateFormatter stringFromDate:[[NSDate date] dateByAddingTimeInterval:-(24*60*60*2)]];
    NSString *last3day = [dateFormatter stringFromDate:[[NSDate date] dateByAddingTimeInterval:-(24*60*60*3)]];
    NSString *last4day = [dateFormatter stringFromDate:[[NSDate date] dateByAddingTimeInterval:-(24*60*60*4)]];
    NSString *last5day = [dateFormatter stringFromDate:[[NSDate date] dateByAddingTimeInterval:-(24*60*60*5)]];
    NSString *last6day = [dateFormatter stringFromDate:[[NSDate date] dateByAddingTimeInterval:-(24*60*60*6)]];
    
    NSString *preDateCode = @"";
    NSString *query = [NSString stringWithFormat:@"SELECT DISTINCT v_date FROM VOICE_LIST WHERE v_public = 1 ORDER BY v_id DESC"];
    FMResultSet *rs = [db executeQuery:query];
    while ([rs next]) {
        NSString *dateTitle = @"";
        NSString *dateSub = @"";
        NSString *dateType = @"";
        NSString *dateCode = @"";
        NSString *dateQuery = @"";
        NSString *thisDate = [rs stringForColumn:@"v_date"];
        
        if ([thisDate isEqualToString:today]) {
            // 今天
            dateTitle = NSLocalizedString(@"Today", nil);
            dateSub = [subDateFormatter stringFromDate:[dateFormatter dateFromString:thisDate]];
            dateType = @"day";
            dateCode = thisDate;
        } else if ([thisDate isEqualToString:yesterday]) {
            // 昨天
            dateTitle = NSLocalizedString(@"Yesterday", nil);
            dateSub = [subDateFormatter stringFromDate:[dateFormatter dateFromString:thisDate]];
            dateType = @"day";
            dateCode = thisDate;
        } else if ([thisDate isEqualToString:last2day] || [thisDate isEqualToString:last3day] || [thisDate isEqualToString:last4day] || [thisDate isEqualToString:last5day] || [thisDate isEqualToString:last6day]) {
            dateTitle = NSLocalizedString([[UIUtility dateFormatterEEEE] stringFromDate:[dateFormatter dateFromString:thisDate]], nil);
            dateSub = [subDateFormatter stringFromDate:[dateFormatter dateFromString:thisDate]];
            dateType = @"day";
            dateCode = thisDate;
        } else {
            // 月份
            dateTitle = [[UIUtility dateFormatterYYYY_MMMM] stringFromDate:[dateFormatter dateFromString:thisDate]];
            dateSub = @"";
            dateType = @"mon";
            dateCode = [[UIUtility dateFormatterYYMM] stringFromDate:[dateFormatter dateFromString:thisDate]];
        }
        
        // 列出當天的前三筆file
        if ([dateType isEqualToString:@"day"]) {
            dateQuery = [NSString stringWithFormat:@"SELECT * FROM VOICE_LIST WHERE v_date = %@ AND v_public = 1 ORDER BY v_id DESC LIMIT 3", dateCode];
        }
        
        // 列出當月的前三筆file
        if ([dateType isEqualToString:@"mon"] && ![preDateCode isEqualToString:dateCode]) {
            dateQuery = [NSString stringWithFormat:@"SELECT * FROM VOICE_LIST WHERE v_date LIKE '%@%%' AND v_public = 1 ORDER BY v_id DESC LIMIT 3", dateCode];
            
            preDateCode = dateCode;
        }
        
        if (![dateQuery isEqualToString:@""]) {
            if (![db executeQuery:dateQuery]){
                NSLog(@"ERROR %@", [db lastErrorMessage]);
            }
            FMResultSet *rsDay = [db executeQuery:dateQuery];
            NSMutableArray *files = [[NSMutableArray alloc] init];
            while ([rsDay next])
            {
                SoundFile *sound = [SoundFile fileOfGroup:[rsDay intForColumn:@"v_gid"]
                                                      vid:[rsDay intForColumn:@"v_id"]
                                                     name:[rsDay stringForColumn:@"v_name"]
                                                   author:[rsDay stringForColumn:@"v_author"]
                                                 latitude:[rsDay stringForColumn:@"v_lat"]
                                                longitude:[rsDay stringForColumn:@"v_lon"]
                                                      lid:[rsDay intForColumn:@"v_lid"]
                                                 duration:[rsDay intForColumn:@"v_duration"]
                                                highlight:[rsDay intForColumn:@"v_highlight"]
                                                     date:[rsDay stringForColumn:@"v_date"]
                                                     time:[rsDay stringForColumn:@"v_time"]
                                                 isPublic:[rsDay intForColumn:@"v_public"]];
                [files addObject:sound];
            }
            
            [self.items addObject:files];
            [self.itemTypes addObject:dateType];
            [self.itemTitles addObject:dateTitle];
            [self.itemSubs addObject:dateSub];
            [self.itemCode addObject:dateCode];
        }
    }
    
    [self.tableView reloadData];
}

@end
