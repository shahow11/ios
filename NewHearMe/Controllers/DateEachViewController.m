//
//  DateEachViewController.m
//  NewHearMe
//
//  Created by IUILab Air on 2016/3/5.
//  Copyright © 2016年 IUILAB. All rights reserved.
//

#import "DateEachViewController.h"
#import "FileViewController.h"
#import "FMDatabase.h"
#import "FMDatabaseAdditions.h"
#import "SoundFile.h"
#import "UIUtility.h"

@interface DateEachViewController ()
{
    FMDatabase *db;
    
    int refreshFlag;
    BOOL isLoadEnough;
}

@end

@implementation DateEachViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.trackTag = @"dateinmonth";
    self.navigationView.lblViewTitle.text = self.viewTitle;
    self.navigationView.btnBack.accessibilityLabel = NSLocalizedString(@"Back to Dates", nil);
    
    isLoadEnough = NO;
    self.previewTimer = [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(loading) userInfo:nil repeats:YES];
    
    // 搜尋資料庫
    NSArray *pathsToDocuments = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [pathsToDocuments objectAtIndex:0];
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"HearMe.db"];
    db = [FMDatabase databaseWithPath:filePath];
    if (![db open]) {
        NSLog(@"Could not open db.");
    }
    
    refreshFlag = 0;
    
    [self refreshData];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (refreshFlag)
        [self refreshData];
    refreshFlag ++;
}

- (void)viewDidAppear:(BOOL)animated
{
    /*
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:SCREEN_NAME_DATE_IN_MONTH];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    */
    [super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)loading
{
    isLoadEnough = YES;
    [self.previewTimer invalidate];
}

#pragma mark - Local function

- (void)refreshData
{
    self.items = [[NSMutableArray alloc]init];
    self.itemTitles = [[NSMutableArray alloc]init];
    self.itemSubs = [[NSMutableArray alloc]init];
    self.itemCode = [[NSMutableArray alloc]init];
    
    NSDate *vDate;
    NSString *query;
    // 找出這個月中有檔案的日期
    query = [NSString stringWithFormat:@"SELECT DISTINCT v_date FROM VOICE_LIST WHERE v_date LIKE '%@%%' AND v_public = 1 ORDER BY v_id", self.month];
    FMResultSet *rs = [db executeQuery:query];
    while ([rs next])
    {
        query = [NSString stringWithFormat:@"SELECT * FROM VOICE_LIST WHERE v_date = %@ AND v_public = 1 ORDER BY v_id LIMIT 3", [rs stringForColumn:@"v_date"]];
        FMResultSet *rsFile = [db executeQuery:query];
        if (![db executeQuery:query]){
            NSLog(@"ERROR %@", [db lastErrorMessage]);
        }
        
        NSMutableArray *s = [[NSMutableArray alloc]init];
        while ([rsFile next])
        {
            vDate = [[UIUtility dateFormatterYYMMdd] dateFromString:[rsFile stringForColumn:@"v_date"]];
            SoundFile *sound = [SoundFile fileOfGroup:[rsFile intForColumn:@"v_gid"]
                                                  vid:[rsFile intForColumn:@"v_id"]
                                                 name:[rsFile stringForColumn:@"v_name"]
                                               author:[rsFile stringForColumn:@"v_author"]
                                             latitude:[rsFile stringForColumn:@"v_lat"]
                                            longitude:[rsFile stringForColumn:@"v_lon"]
                                                  lid:[rsFile intForColumn:@"v_lid"]
                                             duration:[rsFile intForColumn:@"v_duration"]
                                            highlight:[rsFile intForColumn:@"v_highlight"]
                                                 date:[rsFile stringForColumn:@"v_date"]
                                                 time:[rsFile stringForColumn:@"v_time"]
                                             isPublic:[rsFile intForColumn:@"v_public"]];
            [s addObject:sound];
        }
        
        [self.items addObject:s];
        [self.itemTitles addObject:[[UIUtility dateFormatterMMMM_dd] stringFromDate:vDate]];
        [self.itemSubs addObject:@""];
        [self.itemCode addObject:[rs stringForColumn:@"v_date"]];
    }
    [self.tableView reloadData];
}

@end
