//
//  FileViewController.m
//  NewHearMe
//
//  Created by IUILab Air on 2016/3/6.
//  Copyright © 2016年 IUILAB. All rights reserved.
//

#import "FileViewController.h"
#import "PlayViewController.h"
#import "EditCategoryView.h"
#import "FMDatabase.h"
#import "FMDatabaseAdditions.h"
#import "HTTPServer.h"
#import "SoundFile.h"
#import "SoundGroup.h"
#import "UIUtility.h"


@interface FileViewController () <EditCategoryViewDelegate>
{
    BOOL isNameValid;
    int refreshFlag;
    NSString *newFileName;
    NSString *fileSize;
    
  //  UIAlertView *alertRename;
    UIAlertController *alertRename1;
    FMDatabase *db;
    HTTPServer *httpServer;
}

@property (strong, nonatomic) EditCategoryView *editCategoryView;

@end

@implementation FileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    isNameValid = YES;
    
    self.trackTag = @"file";
    self.navigationView.lblViewTitle.text = self.viewTitle;
    self.navigationView.btnBack.accessibilityLabel = self.backText;
    
    // 搜尋資料庫
    NSArray *pathsToDocuments = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [pathsToDocuments objectAtIndex:0];
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"HearMe.db"];
    db = [FMDatabase databaseWithPath:filePath];
    if (![db open]) {
        NSLog(@"Could not open db.");
    }
    
    [self queryList];
}

- (void)viewDidAppear:(BOOL)animated {
    /*
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:[NSString stringWithFormat:@"%@%@", self.fromView, SCREEN_NAME_FILE]];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    */
    [super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Edit Actions

- (void)showEditActions
{
    UIAlertController* alertController = [UIAlertController
              alertControllerWithTitle:nil
              message:NSLocalizedString(@"Change category", nil)
              preferredStyle:UIAlertControllerStyleActionSheet];

           UIAlertAction* item = [UIAlertAction actionWithTitle:NSLocalizedString(@"Rename file", nil)
              style:UIAlertActionStyleDefault
              handler:^(UIAlertAction *action) {
              [self showAlertRename];
           }];
    
        UIAlertAction* renameFile = [UIAlertAction actionWithTitle:NSLocalizedString(@"Change category", nil)
                                                        style:UIAlertActionStyleDefault
                        handler:^(UIAlertAction *action) {
                            [self showAlertRegroup];
            }];

           UIAlertAction* cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
               [alertController dismissViewControllerAnimated:YES completion:nil];
           }];

           [alertController addAction:item];
           [alertController addAction:renameFile];
           [alertController addAction:cancelAction];
    
    /*
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                             delegate:self
                                                    cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:NSLocalizedString(@"Rename file", nil), NSLocalizedString(@"Change category", nil), nil];
    [actionSheet setTag:21];
    [actionSheet showInView:self.view];
     */
}

- (void)showShareActions
{
    SoundFile *deS = (SoundFile*)self.items[self.editIndex.row];
    NSURL *fileURL = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/Documents/%@.mp4", NSHomeDirectory(), deS.name]];
    unsigned long long filesize = [[NSFileManager defaultManager] attributesOfItemAtPath:fileURL.path error:nil].fileSize;
    fileSize = [NSByteCountFormatter stringFromByteCount:filesize countStyle:NSByteCountFormatterCountStyleFile];
    
    NSString *menuTitle = [NSString stringWithFormat:@"%@ %@, %@", NSLocalizedString(@"Sharing file", nil), fileSize, NSLocalizedString(@"Sharing tip", nil)];
    
    UIAlertController* alertController = [UIAlertController
                 alertControllerWithTitle:menuTitle
                 message:nil
                 preferredStyle:UIAlertControllerStyleActionSheet];

              UIAlertAction* item = [UIAlertAction actionWithTitle:NSLocalizedString(@"ShareByEmail", nil)
                 style:UIAlertActionStyleDefault
                 handler:^(UIAlertAction *action) {
                 [self showEmail];
              }];
     UIAlertAction* renameFile = [UIAlertAction actionWithTitle:NSLocalizedString(@"ShareByWifi", nil)
                                                        style:UIAlertActionStyleDefault
                        handler:^(UIAlertAction *action) {
                             SoundFile *deS = (SoundFile*)self.items[self.editIndex.row];
                                   NSURL *fileURL = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/Documents/%@.mp4", NSHomeDirectory(), deS.name]];
                                   unsigned long long filesize = [[NSFileManager defaultManager] attributesOfItemAtPath:fileURL.path error:nil].fileSize;
         self->fileSize = [NSByteCountFormatter stringFromByteCount:filesize countStyle:NSByteCountFormatterCountStyleFile];
                                   [self showWifi];
            }];

              UIAlertAction* cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
                  [alertController dismissViewControllerAnimated:YES completion:nil];
              }];

              [alertController addAction:item];
     [alertController addAction:renameFile];
              [alertController addAction:cancelAction];
    
    
    /*
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:menuTitle
                                                             delegate:self
                                                    cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:NSLocalizedString(@"ShareByEmail", nil), NSLocalizedString(@"ShareByWifi", nil), nil];
    [actionSheet setTag:22];
    [actionSheet showInView:self.view];
     */
}

- (void)showAlertDelete
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Delete this file?", nil) message:@"" preferredStyle:UIAlertControllerStyleAlert];
          
          UIAlertAction *cancel =[UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction *action)
                                  {
              // GA
              /*
                    [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:@"editing" action:@"delete_cancel" label:@"file_menu" value:nil] build]];
                    
                    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
                    [tracker set:kGAIScreenName value:[NSString stringWithFormat:@"%@%@", self.fromView, SCREEN_NAME_FILE]];
                    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
          */
               }];

          UIAlertAction *sure =[UIAlertAction actionWithTitle:NSLocalizedString(@"Confirm", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){[self deleteFile];}];
          [alertController addAction:sure];
          [alertController addAction:cancel];
          [self presentViewController:alertController animated:YES completion:nil];

    
/*
    UIAlertView *alertDelete = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Delete this file?", nil)
                                                          message:@""
                                                         delegate:self
                                                cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                                                otherButtonTitles:NSLocalizedString(@"Confirm", nil), nil];
    alertDelete.tag = 10;
    [alertDelete show];
 
 if (buttonIndex == 1) {
          [self deleteFile];
      } else {
          // GA
          [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:@"editing" action:@"delete_cancel" label:@"file_menu" value:nil] build]];
          
          id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
          [tracker set:kGAIScreenName value:[NSString stringWithFormat:@"%@%@", self.fromView, SCREEN_NAME_FILE]];
          [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
 
  */
    // GA
  //  [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:@"editing" action:@"delete_start" label:@"file_menu" value:nil] build]];
}

- (void)showAlertRename
{
    alertRename1 = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"RenameTitle", nil) message:@"" preferredStyle:UIAlertControllerStyleAlert];
    SoundFile *deS = (SoundFile*)self.items[self.editIndex.row];
     __weak __typeof(self)weakSelf = self;
    [alertRename1 addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        // optionally configure the text field
        textField.keyboardType = UIKeyboardTypeDefault;
        textField.text=deS.name;
        __strong __typeof(weakSelf)strongSelf = weakSelf;
        textField.delegate = strongSelf;

    }];
    
    
    UIAlertAction *cancel =[UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){
  //      [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:@"editing" action:@"rename_cancel" label:@"file_menu" value:nil] build]];
          /*
             id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
             [tracker set:kGAIScreenName value:[NSString stringWithFormat:@"%@%@", self.fromView, SCREEN_NAME_FILE]];
             [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
           */
    }];

    UIAlertAction *sure =[UIAlertAction actionWithTitle:NSLocalizedString(@"Confirm", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            [self renameFile];
    }];
    [alertRename1 addAction:sure];
    [alertRename1 addAction:cancel];
    [self presentViewController:alertRename1 animated:YES completion:nil];
    /*
     
     if (buttonIndex == 1) {
         [self renameFile];
     } else {
         // GA
         [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:@"editing" action:@"rename_cancel" label:@"file_menu" value:nil] build]];
         
         id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
         [tracker set:kGAIScreenName value:[NSString stringWithFormat:@"%@%@", self.fromView, SCREEN_NAME_FILE]];
         [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
     }
     
    alertRename =[[UIAlertView alloc ] initWithTitle:NSLocalizedString(@"RenameTitle", nil)
                                             message:@""
                                            delegate:self
                                   cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                                   otherButtonTitles:NSLocalizedString(@"Confirm", nil), nil];
    alertRename.alertViewStyle = UIAlertViewStylePlainTextInput;
    alertRename.tag = 11;
    
    
    UITextField *inputField = (UITextField*)[alertRename textFieldAtIndex:0];
    inputField.delegate = self;
    inputField.returnKeyType = UIReturnKeyDone;
    inputField.text = deS.name;
    
    [alertRename show];
     // GA
       [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:@"editing" action:@"rename_start" label:@"file_menu" value:nil] build]];
    */
  
}

- (void)showAlertRegroup
{    
    ListTableViewCell *pc = (ListTableViewCell*)[self.tableView cellForRowAtIndexPath:self.editIndex];
    
    self.editCategoryView = [[EditCategoryView alloc] init];
    self.editCategoryView.delegate = self;
    self.editCategoryView.categories = self.itemGroup;
    self.editCategoryView.currentCategoryID = pc.soundFile.gid;
    
    [self.editCategoryView show];
    
    // GA
  //  [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:@"editing" action:@"regroup_start" label:@"file_menu" value:nil] build]];
}

#pragma mark - Email

- (void)showEmail
{
    // 偵測網路狀態？沒網路還是可以sent，等有網路再送達
    
    SoundFile *deS = (SoundFile*)self.items[self.editIndex.row];
    NSString *emailTitle = NSLocalizedString(@"Email title", nil);
    NSString *messageBody = NSLocalizedString(@"Email body", nil);
    NSArray *toRecipents = nil;
    
    MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
    if ([MFMailComposeViewController canSendMail]) {
        mc.mailComposeDelegate = self;
        [mc setSubject:emailTitle];
        [mc setMessageBody:messageBody isHTML:NO];
        [mc setToRecipients:toRecipents];
        
        // Get the resource path and read the file using NSData
        NSString *fileName = [NSString stringWithFormat:@"%@.mp4", deS.name];
        NSURL *fileURL = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/Documents/%@.mp4", NSHomeDirectory(), deS.name]];
        NSData *fileData = [NSData dataWithContentsOfFile:fileURL.path];
        
        // Add attachment
        [mc addAttachmentData:fileData mimeType:@"application/mp4" fileName:fileName];
        
        // Present mail view controller on screen
        [self presentViewController:mc animated:YES completion:NULL];
        // GA
        //[[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:@"editing" action:@"email_start" label:deS.name value:nil] build]];
    } else {
        // 系統自動彈出提示視窗
        NSLog(@"Can't send email");
        // GA
        //[[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:@"editing" action:@"email_error" label:deS.name value:nil] build]];
    }
}

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
   // SoundFile *deS = (SoundFile*)self.items[self.editIndex.row];
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            UIAccessibilityPostNotification(UIAccessibilityAnnouncementNotification, NSLocalizedString(@"Mail cancelled", nil));
            // GA
          //  [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:@"editing" action:@"email_cancel" label:deS.name value:nil] build]];
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            UIAccessibilityPostNotification(UIAccessibilityAnnouncementNotification, NSLocalizedString(@"Mail saved", nil));
            // GA
         //   [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:@"editing" action:@"email_saved" label:deS.name value:nil] build]];
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            UIAccessibilityPostNotification(UIAccessibilityAnnouncementNotification, NSLocalizedString(@"Mail sent", nil));
            // GA
         //   [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:@"editing" action:@"email_send" label:deS.name value:nil] build]];
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            UIAccessibilityPostNotification(UIAccessibilityAnnouncementNotification,
                                            [NSString stringWithFormat:@"%@, %@", NSLocalizedString(@"Mail sent failure", nil), [error localizedDescription]]);
            // GA
     //       [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:@"editing" action:@"email_error" label:deS.name value:nil] build]];
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
    
    // GA Screen
    /*
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:[NSString stringWithFormat:@"%@%@", self.fromView, SCREEN_NAME_FILE]];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
     */
}

#pragma mark - Wi-Fi

- (void)showWifi
{
    // GA
   // [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:@"sharing" action:@"wifi_start" label:@"share" value:nil] build]];
    
    // Create server using our custom MyHTTPServer class
    httpServer = [[HTTPServer alloc] init];
    
    // Tell the server to broadcast its presence via Bonjour.
    // This allows browsers such as Safari to automatically discover our service.
    [httpServer setType:@"_http._tcp."];
    
    // Normally there's no need to run our server on any specific port.
    // Technologies like Bonjour allow clients to dynamically discover the server's port at runtime.
    // However, for easy testing you may want force a certain port so you can just hit the refresh button.
    [httpServer setPort:80];
    
    // Serve files from our embedded Web folder
    //NSString *webPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"Web"];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    // 自動生成網頁內容，代入檔案資訊與下載位置
    [self preparePage];
    
    [httpServer setDocumentRoot:documentsDirectory];
    
    [self startServer];
    
    NSString *hostURL = [NSString stringWithFormat:@"http://%@", [httpServer hostName]];
    NSString *alertMessage = [NSString stringWithFormat:@"%@:\n %@\n%@", NSLocalizedString(@"DownloadAddress", nil), hostURL, NSLocalizedString(@"Wifi tip", nil)];
    
    UIAlertController *alertHost = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"ShareByWifi", nil) message:alertMessage preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *cancel =[UIAlertAction actionWithTitle:NSLocalizedString(@"Terminate", nil) style:UIAlertActionStyleCancel handler:nil];
    [alertHost addAction:cancel];
    [self presentViewController:alertHost animated:YES completion:nil];
    /*
    UIAlertView *alertHost = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"ShareByWifi", nil)
                                                        message:alertMessage
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"Terminate", nil)
                                              otherButtonTitles:nil];
    [alertHost setTag:13];
    [alertHost show];
     */
}

- (void)startServer
{
    NSError *error;
    //SoundFile *deS = (SoundFile*)self.items[self.editIndex.row];
    
    if ([httpServer start:&error])
    {
        // GA
      //  [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:@"editing" action:@"wifi_start" label:deS.name value:nil] build]];
        NSLog(@"Started HTTP Server on port %hu", [httpServer listeningPort]);
    }
    else
    {
        // GA
      //  [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:@"editing" action:@"wifi_error" label:deS.name value:nil] build]];
        NSLog(@"Error starting HTTP Server: %@", error);
    }
}

- (void)stopServer
{
    if (httpServer)
        [httpServer stop];
    
  //  SoundFile *deS = (SoundFile*)self.items[self.editIndex.row];
    // GA
   // [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:@"editing" action:@"wifi_stop" label:deS.name value:nil] build]];
    NSLog(@"Stop HTTP Server");
}

// 準備下載網頁
- (void)preparePage
{
    SoundFile *deS = (SoundFile*)self.items[self.editIndex.row];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"index.html"];
    NSString *sourcePath = [[NSBundle mainBundle] pathForResource:@"index" ofType:@"html"];
    
    NSString *fileName = [NSString stringWithFormat:@"%@.mp4", deS.name];
    
    NSDate *vDate = [[UIUtility dateFormatterYYMMddHHmmss] dateFromString:[NSString stringWithFormat:@"%@%@", deS.date, deS.time]];
    NSString *recordDate = [NSString stringWithFormat:@"%@, %@", [[UIUtility dateFormatterMMMM_dd] stringFromDate:vDate], [[UIUtility dateFormatterhhmm_a] stringFromDate:vDate]];
    
    NSString *recordDetail = fileSize;
    
    //直接下載 <script type='text/javascript'>\n<!--\nwindow.location='%@'\n//-->\n</script>
    NSString *content = [NSString stringWithFormat:@"<html>\n<head>\n<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />\n<title>HearMe App</title>\n</head>\n<body bgcolor='#001A50' style='text-align:center;font-family:Helvetica;padding-top:300px'>\n<a style='font-size:24px;color: #F2F3F6;' href='%@'>%@ %@</a><br/>\n<p style='font-size:18px;color: #808080;line-height:25px;'>%@<br/>%@</p>\n<div style='font-size:14px;color: #F2F3F6;margin-top:100px;'>HearMe © Copyright 2015</div>\n</body>\n</html>", fileName, NSLocalizedString(@"Click to download", nil), fileName, recordDate, recordDetail];
    
    NSError *error = nil;
    
    // 先檢查刪除
    if ([[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
        if (![[NSFileManager defaultManager] removeItemAtPath:filePath error:&error]) {NSLog(@"Remove page Error: %@", error);}
    }
    
    // 複製預設網頁(error page)
    if (![[NSFileManager defaultManager] copyItemAtPath:sourcePath toPath:filePath error:&error]) {
        NSLog(@"Copy page file Error: %@", error);
    } else {
        // 覆寫下載網頁
        [content writeToFile:filePath atomically:YES encoding:NSUTF8StringEncoding error:&error];
        
        if (error) {
            NSLog(@"Rewrite page Error: %@", error);
        }
    }
}

- (void)actionSheetCancelAction{
    
}

/*
#pragma mark - AlertView ActionSheet delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 10) {
        if (buttonIndex == 1) {
            [self deleteFile];
        } else {
            // GA
            [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:@"editing" action:@"delete_cancel" label:@"file_menu" value:nil] build]];
            
            id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
            [tracker set:kGAIScreenName value:[NSString stringWithFormat:@"%@%@", self.fromView, SCREEN_NAME_FILE]];
            [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
        }
    }
    else if (alertView.tag == 11) {
        if (buttonIndex == 1) {
            [self renameFile];
        } else {
            // GA
            [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:@"editing" action:@"rename_cancel" label:@"file_menu" value:nil] build]];
            
            id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
            [tracker set:kGAIScreenName value:[NSString stringWithFormat:@"%@%@", self.fromView, SCREEN_NAME_FILE]];
            [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
        }
    }
    else if (alertView.tag == 13) {
        [self stopServer];
        
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker set:kGAIScreenName value:[NSString stringWithFormat:@"%@%@", self.fromView, SCREEN_NAME_FILE]];
        [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    }
}

- (BOOL)alertViewShouldEnableFirstOtherButton:(UIAlertView *)alertView
{
    if (alertView.tag == 11) {
        UITextField *temp = (UITextField*)[alertView textFieldAtIndex:0];
        if (temp.text.length > 0 && [self validateName:temp.text])
            return YES;
        else
            return NO;
    }
    return YES;
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSString *title = [actionSheet buttonTitleAtIndex:buttonIndex];
    
    if ([title isEqualToString:NSLocalizedString(@"Delete file", nil)]) {
        [self showAlertDelete];
    }
    else if ([title isEqualToString:NSLocalizedString(@"Rename file", nil)]) {
        [self showAlertRename];
    }
    else if ([title isEqualToString:NSLocalizedString(@"Change category", nil)]) {
        [self showAlertRegroup];
    }
    else if ([title isEqualToString:NSLocalizedString(@"ShareByEmail", nil)]) {
        [self showEmail];
    }
    else if ([title isEqualToString:NSLocalizedString(@"ShareByWifi", nil)]) {
        SoundFile *deS = (SoundFile*)self.items[self.editIndex.row];
        NSURL *fileURL = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/Documents/%@.mp4", NSHomeDirectory(), deS.name]];
        unsigned long long filesize = [[NSFileManager defaultManager] attributesOfItemAtPath:fileURL.path error:nil].fileSize;
        fileSize = [NSByteCountFormatter stringFromByteCount:filesize countStyle:NSByteCountFormatterCountStyleFile];
        [self showWifi];
    }
    else if ([title isEqualToString:NSLocalizedString(@"Cancel", nil)])  {
        // GA
        [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:@"editing" action:@"edit_menu_cancel" label:@"file_menu" value:nil] build]];
        
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker set:kGAIScreenName value:[NSString stringWithFormat:@"%@%@", self.fromView, SCREEN_NAME_FILE]];
        [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    }
}
 */

#pragma mark - TextField

- (BOOL)validateName:(NSString*)name
{
    // 檢查檔案名稱是否重複
    SoundFile *deS = (SoundFile*)self.items[self.editIndex.row];
    if (![name isEqualToString:deS.name]) {
        NSString *docPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
        NSString *filePath = [docPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.mp4", name]];
        if ([[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
            [alertRename1 setMessage:[NSString stringWithFormat:@"%@ %@", name, NSLocalizedString(@"NameExistVoice3", nil)]];
            UIAccessibilityPostNotification(UIAccessibilityAnnouncementNotification, [NSString stringWithFormat:@"%@ %@ %@", NSLocalizedString(@"NameExistVoice1", nil), name, NSLocalizedString(@"NameExistVoice3", nil)]);
            return NO;
        }
    }
    
    newFileName = name;
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    isNameValid = YES;
    [alertRename1 setMessage:@""];
    
    // 過濾非法的字元
    NSCharacterSet *cs = [NSCharacterSet characterSetWithCharactersInString:RESTRICT_CHARACTERS];
    NSString *filtered = [string stringByTrimmingCharactersInSet:cs];
    filtered = [filtered stringByTrimmingCharactersInSet:[NSCharacterSet newlineCharacterSet]];
    
    // 限制輸入字元長度(拼注音當中最後會被擋)
    NSUInteger oldLength = [textField.text length];
    NSUInteger replacementLength = [string length];
    NSUInteger rangeLength = range.length;
    
    NSUInteger newLength = oldLength - rangeLength + replacementLength;
    
    BOOL returnKey = [string rangeOfString: @"\n"].location != NSNotFound;
    
    return [string isEqualToString:filtered] && (newLength <= MAX_TEXT_LENGTH || returnKey);
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField.text.length > 0 && [self validateName:textField.text])
        return YES;
    else
        return NO;
}

#pragma mark - Database action

- (void)queryList
{
    self.items = [[NSMutableArray alloc]init];
    self.itemTitles = [[NSMutableArray alloc]init];
    self.itemSubs = [[NSMutableArray alloc]init];
    self.itemVoice = [[NSMutableArray alloc]init];
    self.itemGroup = [[NSMutableArray alloc]init];

    NSDateFormatter *dateFormatter = [UIUtility dateFormatterMMMM_dd];
    NSDateFormatter *subDateFormatter = [UIUtility dateFormatterhhmm_a];
    
    // 列出Query的所有檔案
    FMResultSet *rs = [db executeQuery:self.query];
    while ([rs next])
    {
        NSString *voice = [rs stringForColumn:@"v_name"];
        NSDate *vDate = [[UIUtility dateFormatterYYMMddHHmmss] dateFromString:[NSString stringWithFormat:@"%@%@", [rs stringForColumn:@"v_date"],[rs stringForColumn:@"v_time"]]];
        NSString *defaultName = [NSString stringWithFormat:@"%@_%@",[rs stringForColumn:@"v_date"],[rs stringForColumn:@"v_time"]];
        // 如果是預設檔名，報讀口語日期時間
        if ([defaultName isEqualToString:[rs stringForColumn:@"v_name"]]) {
            voice = [NSString stringWithFormat:@"%@ , %@", [dateFormatter stringFromDate:vDate], [subDateFormatter stringFromDate:vDate]];
        }
        
        SoundFile *sound = [SoundFile fileOfGroup:[rs intForColumn:@"v_gid"]
                                              vid:[rs intForColumn:@"v_id"]
                                             name:[rs stringForColumn:@"v_name"]
                                           author:[rs stringForColumn:@"v_author"]
                                         latitude:[rs stringForColumn:@"v_lat"]
                                        longitude:[rs stringForColumn:@"v_lon"]
                                              lid:[rs intForColumn:@"v_lid"]
                                         duration:[rs intForColumn:@"v_duration"]
                                        highlight:[rs intForColumn:@"v_highlight"]
                                             date:[rs stringForColumn:@"v_date"]
                                             time:[rs stringForColumn:@"v_time"]
                                         isPublic:[rs intForColumn:@"v_public"]];
        [self.items addObject:sound];
        [self.itemTitles addObject:[rs stringForColumn:@"v_name"]];
        [self.itemSubs addObject:[NSString stringWithFormat:@"%@\r%@", [dateFormatter stringFromDate:vDate], [subDateFormatter stringFromDate:vDate]]];
        [self.itemVoice addObject:voice];
    }
    
    // 準備群組清單
    FMResultSet *rsg = [db executeQuery:@"SELECT g_id, g_name, g_sequence FROM GROUP_TABLE ORDER BY g_sequence"];
    while ([rsg next])
    {
        SoundGroup *groupItem = [SoundGroup groupByID:[rsg intForColumn:@"g_id"] name:NSLocalizedString([rsg stringForColumn:@"g_name"], nil) seq:[rsg intForColumn:@"g_sequence"]];
        [self.itemGroup addObject:groupItem];
    }
    
    [self.tableView reloadData];
}

- (void)deleteFile
{
    // 刪除資料庫
    SoundFile *deS = (SoundFile*)self.items[self.editIndex.row];
    //    NSString *qry = [NSString stringWithFormat:@"DELETE FROM VOICE_LIST WHERE v_id = %d", deS.vid];
    //    if (![db executeUpdate:qry]){
    //        NSLog(@"ERROR DELETE voice_list: %@", [db lastErrorMessage]);
    //        return;
    //    }
    NSString *qry = [NSString stringWithFormat:@"UPDATE VOICE_LIST SET v_public = 0 WHERE v_id = %d", deS.vid];
    if (![db executeUpdate:qry]){
        NSLog(@"ERROR UPDATE DELETE voice_list: %@", [db lastErrorMessage]);
        return;
    }
    
    // 刪除標記資料(應把voice_list留著，開欄位是否已刪)
    //    qry = [NSString stringWithFormat:@"DELETE FROM SECTION_TABLE WHERE s_vid = %d", deS.vid];
    //    if (![db executeUpdate:qry]){
    //        NSLog(@"ERROR DELETE section_table: %@", [db lastErrorMessage]);
    //        return;
    //    }
    
    // 刪除檔案,標題檔案
    NSString *pathIntro = [NSString stringWithFormat:@"%@/Documents/%@_Intro.mp4", NSHomeDirectory(), deS.name];
    NSString *pathFile = [NSString stringWithFormat:@"%@/Documents/%@.mp4", NSHomeDirectory(), deS.name];
    NSError *error = nil;
    if ([[NSFileManager defaultManager] fileExistsAtPath:pathIntro]) {
        if (![[NSFileManager defaultManager] removeItemAtPath:pathIntro error:&error]) {
            NSLog(@"Error delete: %@", [error localizedDescription]);
        }
    }
    if ([[NSFileManager defaultManager] fileExistsAtPath:pathFile]) {
        if (![[NSFileManager defaultManager] removeItemAtPath:pathFile error:&error]) {
            NSLog(@"Error delete: %@", [error localizedDescription]);
        }
    }
    
    //    // 刪除陣列, 動畫
    //    [files removeObjectAtIndex:self.editIndex.row];
    //    [fileTitles removeObjectAtIndex:self.editIndex.row];
    //    [fileSubs removeObjectAtIndex:self.editIndex.row];
    //
    //    [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:self.editIndex] withRowAnimation:UITableViewRowAnimationFade];
    
    // 刪除成功後，語音回饋
    UIAccessibilityPostNotification(UIAccessibilityAnnouncementNotification,
                                    NSLocalizedString(@"Delete complete", nil));
    
    [self performSelector:@selector(queryList) withObject:nil afterDelay:1.0];
    
    //    // 若為唯一一筆 則回上層
    //    if (files.count == 0) {
    //        [self.navigationController popViewControllerAnimated:YES];
    //    }
    /*
    // GA
    [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:@"editing" action:@"delete_file" label:deS.name value:nil] build]];
    
    // GA Screen
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:[NSString stringWithFormat:@"%@%@", self.fromView, SCREEN_NAME_FILE]];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
*/
     }

- (void)renameFile
{
    SoundFile *deS = (SoundFile*)self.items[self.editIndex.row];
    if (![newFileName isEqualToString:deS.name]) {
        // 更新資料庫v_name
        NSString *qry = [NSString stringWithFormat:@"UPDATE VOICE_LIST SET v_name = '%@' WHERE v_id = %d", newFileName, deS.vid];
        if (![db executeUpdate:qry]){
            NSLog(@"ERROR UPDATE name: %@", [db lastErrorMessage]);
            return;
        }
        
        // 重新命名檔案
        NSError *error = nil;
        NSFileManager *manager = [NSFileManager defaultManager];
        NSString *docPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
        
        // 重新命名檔案
        NSString *filePathSrc = [docPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.mp4", deS.name]];
        NSString *filePathDst = [docPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.mp4", newFileName]];
        if ([manager fileExistsAtPath:filePathSrc]) {
            [manager moveItemAtPath:filePathSrc toPath:filePathDst error:&error];
            if (error) {
                NSLog(@"Rename file Error: %@", [error localizedDescription]);
            }
        } else {
            NSLog(@"File %@.mp4 doesn't exists", deS.name);
        }
        
        // 重新命名語音簡介(if none it's ok)
        filePathSrc = [docPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@_Intro.mp4", deS.name]];
        filePathDst = [docPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@_Intro.mp4", newFileName]];
        if ([manager fileExistsAtPath:filePathSrc]) {
            error = nil;
            [manager moveItemAtPath:filePathSrc toPath:filePathDst error:&error];
            if (error) {
                NSLog(@"Rename audio intro Error: %@", error);
            }
        } else {
            NSLog(@"File %@_Intro.mp4 doesn't exists", deS.name);
        }
        
        // 執行成功後，語音回饋
        UIAccessibilityPostNotification(UIAccessibilityAnnouncementNotification,
                                        NSLocalizedString(@"Rename complete", nil));
        
        // 更新Table
        [self performSelector:@selector(queryList) withObject:nil afterDelay:1.0];
        
        // GA
        /*
        [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:@"editing" action:@"rename_file" label:deS.name value:nil] build]];
        
        // GA Screen
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker set:kGAIScreenName value:[NSString stringWithFormat:@"%@%@", self.fromView, SCREEN_NAME_FILE]];
        [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
         */
    }
}

#pragma mark - EditCategoryView Delegate

- (void)cancelAction:(id)editCategoryView
{
    [self.editCategoryView close];
    self.editCategoryView = nil;
    
    // GA
    /*
    [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:@"editing" action:@"regroup_cancel" label:@"file_menu" value:nil] build]];
    
    // GA Screen
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:[NSString stringWithFormat:@"%@%@", self.fromView, SCREEN_NAME_FILE]];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
     */
}

- (void)confirmAction:(id)editCategoryView
{
    [self.editCategoryView close];
    
    // 更新資料庫v_gid
    SoundFile *deS = (SoundFile*)self.items[self.editIndex.row];
    NSString *qry = [NSString stringWithFormat:@"UPDATE VOICE_LIST SET v_gid = %d WHERE v_id = %d", self.editCategoryView.newCategoryID, deS.vid];
    if (![db executeUpdate:qry]){
        NSLog(@"ERROR UPDATE group: %@", [db lastErrorMessage]);
        return;
    }
    
    // 執行成功後，語音回饋
    UIAccessibilityPostNotification(UIAccessibilityAnnouncementNotification, NSLocalizedString(@"Change category complete", nil));
    
    [self performSelector:@selector(queryList) withObject:nil afterDelay:1.0];
    
    // GA
    /*
    [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:@"editing" action:@"regroup_file" label:deS.name value:nil] build]];
    
    // GA Screen
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:[NSString stringWithFormat:@"%@%@", self.fromView, SCREEN_NAME_FILE]];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
     */
}

@end
