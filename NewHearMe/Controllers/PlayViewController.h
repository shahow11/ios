//
//  PlayViewController.h
//  NewHearMe
//
//  Created by IUILab Air on 2016/3/5.
//  Copyright © 2016年 IUILAB. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <AudioToolbox/AudioToolbox.h>
//#import <GoogleAnalytics/GAI.h>
//#import <GoogleAnalytics/GAIDictionaryBuilder.h>
//#import <GoogleAnalytics/GAIFields.h>
#import <MessageUI/MessageUI.h>
#import "PartTableViewCell.h"
#import "SoundFile.h"

@interface PlayViewController : UIViewController< UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate, AVAudioPlayerDelegate,UIActionSheetDelegate, MFMailComposeViewControllerDelegate, PartTableViewCellDelegate>

@property (nonatomic, weak) NSString *viewTitle;
@property (nonatomic, weak) SoundFile *soundFile;

@end
