//
//  CategoryViewController.m
//  NewHearMe
//
//  Created by IUILab Air on 2016/3/5.
//  Copyright © 2016年 IUILAB. All rights reserved.
//

#import "CategoryViewController.h"
#import "EditTableViewController.h"
#import "FileViewController.h"
#import "FMDatabase.h"
#import "FMDatabaseAdditions.h"
#import "SoundGroup.h"
#import "SoundFile.h"
#import "UIUtility.h"

@interface CategoryViewController ()
{
    FMDatabase *db;
    
    int refreshFlag;
}

@end

@implementation CategoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.trackTag = @"category";
    
    self.navigationView.lblViewTitle.text = NSLocalizedString(@"Category", nil);
    self.navigationView.btnCategory.hidden = NO;
    self.navigationView.btnCategory.accessibilityLabel = NSLocalizedString(@"Custom category", nil);
    [self.navigationView.btnCategory addTarget:self action:@selector(openEditMode) forControlEvents:UIControlEventTouchUpInside];
    
    // 搜尋資料庫
    NSArray *pathsToDocuments = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [pathsToDocuments objectAtIndex:0];
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"HearMe.db"];
    db = [FMDatabase databaseWithPath:filePath];
    if (![db open]) {
        NSLog(@"Could not open db.");
    }
    
    refreshFlag = 0;
    [self refreshData];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if (refreshFlag)
        [self refreshData];
    refreshFlag ++;
}

- (void)viewDidAppear:(BOOL)animated {
    /*
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:SCREEN_NAME_CATEGORY];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
*/
    [super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Local functions

- (void)refreshData
{
    self.items = [[NSMutableArray alloc]init];
    self.itemFiles = [[NSMutableArray alloc]init];
    
    NSString *query = @"";
    FMResultSet *rs = [db executeQuery:@"SELECT DISTINCT v_gid, g_id, g_name, g_sequence FROM VOICE_LIST, GROUP_TABLE WHERE v_gid = g_id AND v_public = 1 ORDER BY g_sequence"];
    while ([rs next])
    {
        SoundGroup *groupItem = [SoundGroup groupByID:[rs intForColumn:@"g_id"] name:NSLocalizedString([rs stringForColumn:@"g_name"], nil) seq:[rs intForColumn:@"g_sequence"]];
        [self.items addObject:groupItem];
        
        query = [NSString stringWithFormat:@"SELECT * FROM VOICE_LIST WHERE v_gid = %@ AND v_public = 1 ORDER BY v_id DESC LIMIT 3", [rs stringForColumn:@"g_id"]];
        FMResultSet *rsFile = [db executeQuery:query];
        
        NSMutableArray *s = [[NSMutableArray alloc]init];
        while ([rsFile next])
        {
            SoundFile *sound = [SoundFile fileOfGroup:[rsFile intForColumn:@"v_gid"]
                                                  vid:[rsFile intForColumn:@"v_id"]
                                                 name:[rsFile stringForColumn:@"v_name"]
                                               author:[rsFile stringForColumn:@"v_author"]
                                             latitude:[rsFile stringForColumn:@"v_lat"]
                                            longitude:[rsFile stringForColumn:@"v_lon"]
                                                  lid:[rsFile intForColumn:@"v_lid"]
                                             duration:[rsFile intForColumn:@"v_duration"]
                                            highlight:[rsFile intForColumn:@"v_highlight"]
                                                 date:[rsFile stringForColumn:@"v_date"]
                                                 time:[rsFile stringForColumn:@"v_time"]
                                             isPublic:[rsFile intForColumn:@"v_public"]];
            [s addObject:sound];
        }
        [self.itemFiles addObject:s];
    }
    
    [self.tableView reloadData];
}

/// 開啟清單編輯視窗
- (void)openEditMode
{
    [self cancelPlay];
    
    EditTableViewController *et = [[EditTableViewController alloc] init];
    UINavigationController *navC = [[UINavigationController alloc] initWithRootViewController:et];
    
    [self presentViewController:navC animated:YES completion:nil];
}

@end
