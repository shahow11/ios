//
//  ListViewController.m
//  NewHearMe
//
//  Created by IUILab Air on 2016/3/6.
//  Copyright © 2016年 IUILAB. All rights reserved.
//

#import "ListViewController.h"
#import "DateEachViewController.h"
#import "FileViewController.h"
#import "PlayViewController.h"
#import "SoundFile.h"
#import "SoundGroup.h"
#import "UIUtility.h"


NSString * const kReuseIdentifierDateTableCell = @"dateTableCell";
NSString * const kReuseIdentifierMonthTableCell = @"monthTableCell";
NSString * const kReuseIdentifierEachTableCell = @"eachTableCell";
NSString * const kReuseIdentifierCategoryTableCell = @"categoryTableCell";
NSString * const kReuseIdentifierLocationTableCell = @"locationTableCell";
NSString * const kReuseIdentifierFileTableCell = @"fileTableCell";
NSString * const kReuseIdentifierSearchTableCell = @"searchTableCell";
NSString * const kReuseIdentifierResultTableCell = @"resultTableCell";

@interface ListViewController ()

@end

@implementation ListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.isSpeaking = NO;
    self.isBreakSend = NO;
    self.tempSpeech = @"";
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    [self.tableView registerNib:[UINib nibWithNibName:@"ListTableViewCell" bundle:nil] forCellReuseIdentifier:kReuseIdentifierDateTableCell];
    [self.tableView registerNib:[UINib nibWithNibName:@"ListTableViewCell" bundle:nil] forCellReuseIdentifier:kReuseIdentifierMonthTableCell];
    [self.tableView registerNib:[UINib nibWithNibName:@"ListTableViewCell" bundle:nil] forCellReuseIdentifier:kReuseIdentifierEachTableCell];
    [self.tableView registerNib:[UINib nibWithNibName:@"ListTableViewCell" bundle:nil] forCellReuseIdentifier:kReuseIdentifierCategoryTableCell];
    [self.tableView registerNib:[UINib nibWithNibName:@"ListTableViewCell" bundle:nil] forCellReuseIdentifier:kReuseIdentifierLocationTableCell];
    [self.tableView registerNib:[UINib nibWithNibName:@"ListTableViewCell" bundle:nil] forCellReuseIdentifier:kReuseIdentifierFileTableCell];
    [self.tableView registerNib:[UINib nibWithNibName:@"ListTableViewCell" bundle:nil] forCellReuseIdentifier:kReuseIdentifierSearchTableCell];
    [self.tableView registerNib:[UINib nibWithNibName:@"ListTableViewCell" bundle:nil] forCellReuseIdentifier:kReuseIdentifierResultTableCell];
    
    self.navigationView.btnBack.accessibilityLabel = NSLocalizedString(@"Back to Main page", nil);
    [self.navigationView.btnBack addTarget:self action:@selector(popViewControllerAnimated) forControlEvents:UIControlEventTouchUpInside];
    
    [self.bottomToolBar.btnHome setAction:@selector(homeAction)];
    [self.bottomToolBar.btnHelp setAction:@selector(helpAction)];
}

- (void)viewWillAppear:(BOOL)animated {
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    // 設定VoiceOver
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didFinishAnnouncement:) name:UIAccessibilityAnnouncementDidFinishNotification object:nil];
}

- (void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [self cancelPlay];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIAccessibilityAnnouncementDidFinishNotification object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TableView Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [self.items count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    long int rowNumber = [indexPath row];
    ListTableViewCell *cell;
    
    if ([self.trackTag isEqualToString:@"date"]) {
        if ([self.itemTypes[rowNumber] isEqualToString:@"day"]) {
            cell = [tableView dequeueReusableCellWithIdentifier:kReuseIdentifierDateTableCell forIndexPath:indexPath];
            cell.lblSubTitle.text = self.itemSubs[rowNumber];
        } else {
            cell = [tableView dequeueReusableCellWithIdentifier:kReuseIdentifierMonthTableCell forIndexPath:indexPath];
            cell.lblSubTitle.text = @"";
        }
        cell.dateTag = self.itemCode[rowNumber];
        cell.lblTitle.text = self.itemTitles[rowNumber];
        cell.playList = self.items[rowNumber];
    }
    if ([self.trackTag isEqualToString:@"dateinmonth"]) {
        cell = [tableView dequeueReusableCellWithIdentifier:kReuseIdentifierEachTableCell forIndexPath:indexPath];
        cell.lblTitle.text = self.itemTitles[rowNumber];
        cell.lblSubTitle.text = self.itemSubs[rowNumber];
        cell.day = self.itemCode[rowNumber];
        cell.playList = self.items[rowNumber];
    }
    if ([self.trackTag isEqualToString:@"category"]) {
        cell = [tableView dequeueReusableCellWithIdentifier:kReuseIdentifierCategoryTableCell forIndexPath:indexPath];
        cell.lblSubTitle.text = @"";
        
        SoundGroup *sg = self.items[rowNumber];
        cell.lblTitle.text = sg.name;
        cell.gid = sg.gid;
        cell.playList = self.itemFiles[rowNumber];
    }
    if ([self.trackTag isEqualToString:@"location"]) {
        cell = [tableView dequeueReusableCellWithIdentifier:kReuseIdentifierLocationTableCell forIndexPath:indexPath];
        cell.lkey = self.itemID[rowNumber];
        cell.lblTitle.text = self.items[rowNumber];
        if (self.hasNearest && rowNumber == 0) {
            cell.lblSubTitle.text = NSLocalizedString(@"Nearest", nil);
        } else {
            [cell.lblSubTitle setHidden:YES];
        }
        cell.playList = self.itemFiles[rowNumber];
    }
    if ([self.trackTag isEqualToString:@"file"] || [self.trackTag isEqualToString:@"search"] || [self.trackTag isEqualToString:@"result"]) {
        if ([self.trackTag isEqualToString:@"file"])
            cell = [tableView dequeueReusableCellWithIdentifier:kReuseIdentifierFileTableCell forIndexPath:indexPath];
        else if ([self.trackTag isEqualToString:@"search"])
            cell = [tableView dequeueReusableCellWithIdentifier:kReuseIdentifierSearchTableCell forIndexPath:indexPath];
        else if ([self.trackTag isEqualToString:@"result"])
            cell = [tableView dequeueReusableCellWithIdentifier:kReuseIdentifierResultTableCell forIndexPath:indexPath];
            
        cell.lblTitle.text = self.itemTitles[rowNumber];
        cell.lblSubTitle.text = self.itemSubs[rowNumber];
        cell.soundFile = self.items[rowNumber];
        cell.voiceTitle = self.itemVoice[rowNumber];
    }
    
    cell.trackTag = self.trackTag;
    cell.delegate = self;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    long int rowNumber = [indexPath row];
    
    if ([self.trackTag isEqualToString:@"date"])
    {
        if ([self.itemTypes[rowNumber] isEqualToString:@"day"]) {
        FileViewController *filesView = [self.storyboard instantiateViewControllerWithIdentifier:@"FileView"];
        
        //取得選擇的日期
        filesView.viewTitle = self.itemTitles[rowNumber];
        filesView.backText = NSLocalizedString(@"Back to Dates", nil);
        filesView.query = [NSString stringWithFormat:@"SELECT * FROM VOICE_LIST WHERE v_date = %@ AND v_public = 1 ORDER BY v_id DESC", self.itemCode[rowNumber]];
        
        filesView.fromView = @"日期";
        
        // GA
     //   [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:@"tracking" action:@"segue_file_list" label:@"date" value:nil] build]];
            [self.navigationController pushViewController:filesView animated:YES];
        } else {
            DateEachViewController *daysView = [self.storyboard instantiateViewControllerWithIdentifier:@"DateEachView"];
            
            //取得選擇的月份
            daysView.month = self.itemCode[rowNumber];
            daysView.viewTitle = self.itemTitles[rowNumber];
            daysView.fromView = self.viewTitle;
            
            // GA
        //    [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:@"tracking" action:@"segue_day_list" label:@"date" value:nil] build]];
            [self.navigationController pushViewController:daysView animated:YES];
        }
    }
    if ([self.trackTag isEqualToString:@"dateinmonth"])
    {
        FileViewController *filesView = [self.storyboard instantiateViewControllerWithIdentifier:@"FileView"];
        
        //取得選擇的日期
        filesView.viewTitle = self.itemTitles[rowNumber];
        filesView.backText = [NSString stringWithFormat:@"%@ %@", NSLocalizedString(@"Back to", nil), self.viewTitle];
        filesView.query = [NSString stringWithFormat:@"SELECT * FROM VOICE_LIST WHERE v_date = %@ AND v_public = 1 ORDER BY v_id DESC", self.itemCode[rowNumber]];
        filesView.fromView = @"date";
        
        // GA
     //   [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:@"tracking" action:@"segue_file_list" label:@"day" value:nil] build]];
        [self.navigationController pushViewController:filesView animated:YES];
    }
    
    if ([self.trackTag isEqualToString:@"category"])
    {
        FileViewController *filesView = [self.storyboard instantiateViewControllerWithIdentifier:@"FileView"];
        
        //取得選擇的日期
        SoundGroup *sg = self.items[rowNumber];
        filesView.viewTitle = sg.name;
        filesView.backText = NSLocalizedString(@"Back to Category", nil);
        filesView.query = [NSString stringWithFormat:@"SELECT * FROM VOICE_LIST WHERE v_gid = %d AND v_public = 1 ORDER BY v_id DESC", sg.gid];
        filesView.fromView = @"群組";
        
        // GA
       // [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:@"tracking" action:@"segue_file_list" label:@"category" value:nil] build]];
        [self.navigationController pushViewController:filesView animated:YES];
    }
    if ([self.trackTag isEqualToString:@"location"])
    {
        FileViewController *filesView = [self.storyboard instantiateViewControllerWithIdentifier:@"FileView"];
        
        //取得選擇的地點
        filesView.viewTitle = self.items[rowNumber];
        filesView.backText = NSLocalizedString(@"Back to Locations", nil);
        
        if ([self.itemID[rowNumber] isEqualToString:self.items[rowNumber]])
            filesView.query = [NSString stringWithFormat:@"SELECT * FROM VOICE_LIST, LOCATION_TABLE WHERE v_lid = l_id AND v_public = 1 AND l_landmark = '%@' ORDER BY v_id DESC", self.itemID[rowNumber]];
        else
            filesView.query = [NSString stringWithFormat:@"SELECT * FROM VOICE_LIST, LOCATION_TABLE WHERE v_lid = l_id AND v_public = 1 AND l_id = %@ ORDER BY v_id DESC", self.itemID[rowNumber]];
        
        filesView.fromView = @"地點";
        
        // GA
  //      [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:@"tracking" action:@"segue_file_list" label:@"location" value:nil] build]];
        [self.navigationController pushViewController:filesView animated:YES];
    }
    if ([self.trackTag isEqualToString:@"file"] || [self.trackTag isEqualToString:@"search"] || [self.trackTag isEqualToString:@"result"])
    {
        PlayViewController *playView = [self.storyboard instantiateViewControllerWithIdentifier:@"PlayView"];
        
        //取得選擇的日期
        playView.viewTitle = self.itemVoice[rowNumber];
        playView.soundFile = self.items[rowNumber];
        
        // GA
     //   NSString *eventCategory = [self.trackTag isEqualToString:@"file"]? @"reviewing" : @"searching";
      //  [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:eventCategory action:@"segue_play" label:self.fromView value:nil] build]];
        
        if ([self.trackTag isEqualToString:@"result"])
        {
            [self.presentingViewController.navigationController pushViewController:playView animated:YES];
        } else {
            [self.navigationController pushViewController:playView animated:YES];
        }
    }
    
    // note: should not be necessary but current iOS 8.0 bug (seed 4) requires it
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50.0;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // 預設「未定位」不可修改
    long int rowNumber = [indexPath row];
    if (([self.trackTag isEqualToString:@"location"] && ![self.items[rowNumber] isEqualToString:NSLocalizedString(@"Not Located", nil)]) || [self.trackTag isEqualToString:@"file"])
        return YES;
    else
        return NO;
}

- (NSString*)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self cancelPlay];
    return NSLocalizedString(@"DeleteButton", nil);
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
}

- (NSArray*)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self.trackTag isEqualToString:@"file"]) {
        UITableViewRowAction *editAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:NSLocalizedString(@"EditAction", nil) handler:^(UITableViewRowAction *action, NSIndexPath *indexPath){
         /*
            id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
            [tracker set:kGAIScreenName value:SCREEN_NAME_FILE_EDIT];
            [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
           */
            // 編輯選單
            self.editIndex = indexPath;
            [self.tableView setEditing:NO];
            [self showEditActions];
        }];
        editAction.backgroundColor = [UIColor grayColor];
        
        UITableViewRowAction *shareAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:NSLocalizedString(@"ShareAction", nil) handler:^(UITableViewRowAction *action, NSIndexPath *indexPath){
       /*
            id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
            [tracker set:kGAIScreenName value:SCREEN_NAME_FILE_EDIT];
            [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
         */
            // 分享選單
            self.editIndex = indexPath;
            [self.tableView setEditing:NO];
            [self showShareActions];
        }];
        shareAction.backgroundColor = [UIColor lightGrayColor];
        //regroupAction.backgroundEffect = [UIVibrancyEffect effectForBlurEffect:[UIBlurEffect effectWithStyle:UIBlurEffectStyleExtraLight]];
        
        UITableViewRowAction *deleteAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDestructive title:NSLocalizedString(@"DeleteAction", nil)  handler:^(UITableViewRowAction *action, NSIndexPath *indexPath){
          /*
            id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
            [tracker set:kGAIScreenName value:SCREEN_NAME_FILE_EDIT];
            [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
            */
            self.editIndex = indexPath;
            [self.tableView setEditing:NO];
            [self showAlertDelete];
        }];
        
        return @[deleteAction, editAction, shareAction];
    }
    if ([self.trackTag isEqualToString:@"location"]) {
        UITableViewRowAction *reameAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:NSLocalizedString(@"RenameAction", nil) handler:^(UITableViewRowAction *action, NSIndexPath *indexPath){
            /*
            id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
            [tracker set:kGAIScreenName value:SCREEN_NAME_LOCATION_EDIT];
            [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
            */
            self.editIndex = indexPath;
            [self.tableView setEditing:NO];
            [self showAlertRename];
        }];
        reameAction.backgroundColor = [UIColor grayColor];
        
        return @[reameAction];
    }
    
    return nil;
}

#pragma mark - TableCell Delegate

- (void)playCellList:(ListTableViewCell*)cell
{
    if (!UIAccessibilityIsVoiceOverRunning()) {
        [self cancelPlay];
    }
    
    if (cell.playList.count || cell.soundFile) {
        // 目前改為暫停圖案
        [cell.btnPreview setImage:[UIImage imageNamed:@"item_pause_btn.png"] forState:UIControlStateNormal];
    }
    if ([cell.trackTag isEqualToString:@"file"] || [cell.trackTag isEqualToString:@"search"] || [cell.trackTag isEqualToString:@"result"])
        [self playPreviewBySoundFile:cell.soundFile];
    else
        [self playPreviewByPlaylist:cell.playList];
}

- (void)cancelPlay
{
    if (self.audioPlayer.isPlaying) {
        [self.audioPlayer stop];
    }
    if (self.timer.valid) {
        [self.timer invalidate];
    }
    // All暫停圖案改為播放圖案
    [self resetBtnPreview];
    // 取消所有待播放
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
}

- (void)beginSpeak
{
    self.isSpeaking = YES;
    self.isBreakSend = NO;
}

#pragma mark - Previewing

- (void)resetBtnPreview
{
    for(int i = 0; i < [self.tableView numberOfRowsInSection:0]; i++) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];
        ListTableViewCell *cell = (ListTableViewCell*)[self.tableView cellForRowAtIndexPath:indexPath];
        [cell.btnPreview setImage:[UIImage imageNamed:@"item_play_btn.png"] forState:UIControlStateNormal];
    }
}

- (void)playPreviewByPlaylist:(NSMutableArray*)soundFile
{
    NSTimeInterval timeToDelayed = 0;
    for(int i = 0; i < soundFile.count; i++) {
        
        if (i) {
            //NSLog(@"%d Din: %.1f",i+1, timeToDelayed); // 播放分隔音效
            timeToDelayed += 0.2;
            [self performSelector:@selector(playSound) withObject:nil afterDelay:timeToDelayed];
            timeToDelayed += Time_for_Sound;
        }
        
        SoundFile *file = (SoundFile*)soundFile[i];
        NSString *defaultName = [NSString stringWithFormat:@"%@_%@", file.date, file.time];
        NSURL *urlIntro = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/Documents/%@_Intro.mp4", NSHomeDirectory(), file.name]];
        NSURL *urlFile = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/Documents/%@.mp4", NSHomeDirectory(), file.name]];
        
        if (![file.name isEqualToString:defaultName] && UIAccessibilityIsVoiceOverRunning()) {
            // 如果有自定檔名，VoiceOver報讀
            //NSLog(@"%d playTitle: %@ - %.1f",i+1, file.name, timeToDelayed);
            [self performSelector:@selector(playTitle:) withObject:file.name afterDelay:timeToDelayed];
            timeToDelayed += 2; // 根據字數、速度計算延遲時間？
            
        } else if ([[NSFileManager defaultManager] fileExistsAtPath:[urlIntro path]]) {
            // 如果有語音簡介，播放
            //NSLog(@"%d playIntro: %@_Intro - %.1f",i+1, file.name, timeToDelayed);
            [self performSelector:@selector(playIntro:) withObject:urlIntro afterDelay:timeToDelayed];
            self.audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:urlIntro error:nil];
            timeToDelayed += self.audioPlayer.duration;
            
        } else if ([[NSFileManager defaultManager] fileExistsAtPath:[urlFile path]]) {
            // 都沒有，播重點內容
            //NSLog(@"%d playFile: %@ - %.1f",i+1, file.name, timeToDelayed);
            [self performSelector:@selector(playFile:) withObject:file afterDelay:timeToDelayed];
            
            if (file.duration < Time_for_Content) {
                timeToDelayed += file.duration;
            } else {
                timeToDelayed += Time_for_Content;
            }
        }
        
        if (i == soundFile.count-1) {
            [self performSelector:@selector(resetBtnPreview) withObject:nil afterDelay:timeToDelayed];
        }
    }
}

- (void)playPreviewBySoundFile:(SoundFile*)file
{
    NSTimeInterval timeToDelayed = 0;
    NSURL *urlIntro = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/Documents/%@_Intro.mp4",NSHomeDirectory(),file.name]];
    NSURL *urlFile = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/Documents/%@.mp4", NSHomeDirectory(), file.name]];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:[urlIntro path]]) {
        // 如果有語音簡介，播放
        //NSLog(@"Intro: %@_Intro - %.1f", file.name, timeToDelayed);
        [self performSelector:@selector(playIntro:) withObject:urlIntro afterDelay:timeToDelayed];
        self.audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:urlIntro error:nil];
        timeToDelayed += self.audioPlayer.duration;
        
        //NSLog(@"Din: %.1f", timeToDelayed); // 播放分隔音效
        timeToDelayed += 0.2;
        [self performSelector:@selector(playSound) withObject:nil afterDelay:timeToDelayed];
        timeToDelayed += Time_for_Sound;
    }
    if ([[NSFileManager defaultManager] fileExistsAtPath:[urlFile path]]) {
        // 播檔案重點內容
        //NSLog(@"Content: %@ - %.1f", file.name, timeToDelayed);
        [self performSelector:@selector(playFileInContentTime:) withObject:file afterDelay:timeToDelayed];
        
        self.contentTime = file.duration - file.highlight;
        if (self.contentTime > Time_for_Content) {
            self.contentTime = Time_for_Content;
        }
        
        timeToDelayed += self.contentTime;
    }
    
    [self performSelector:@selector(resetBtnPreview) withObject:nil afterDelay:timeToDelayed];
}

// 預聽標題
- (void)playTitle:(NSString*)title
{
    if (!self.isSpeaking) {
        if (![self.trackTag isEqualToString:@"file"] && ![self.trackTag isEqualToString:@"search"] && ![self.trackTag isEqualToString:@"result"]) {
            // 開始計時
            self.previewTime = 0;
            self.previewTimer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(ticking) userInfo:nil repeats:YES];
        }
        UIAccessibilityPostNotification(UIAccessibilityAnnouncementNotification, title);
        self.tempSpeech = title;
        self.isSpeaking = YES;
        
        // GA
    //    [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:@"tracking" action:@"preview_text" label:self.trackTag value:nil] build]];
    } else {
        if (!self.isBreakSend) {
            // GA
      //      [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:@"tracking" action:@"preview_break" label:self.trackTag value:nil] build]];
            self.isBreakSend = YES;
        }
    }
}

// 預聽簡介
- (void)playIntro:(NSURL*)url
{
    if (!self.isSpeaking) {
        self.audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:nil];
        [self.audioPlayer setDelegate:self];
        [self.audioPlayer prepareToPlay];
        [self.audioPlayer play];
        
        // GA
   //     [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:@"tracking" action:@"preview_intro" label:self.trackTag value:nil] build]];
    } else {
        if (!self.isBreakSend) {
            // GA
     //       [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:@"tracking" action:@"preview_break" label:self.trackTag value:nil] build]];
            self.isBreakSend = YES;
        }
    }
}

// 預聽檔案
- (void)playFile:(SoundFile*)file
{
    if (!self.isSpeaking) {
        NSURL *urlFile = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/Documents/%@.mp4", NSHomeDirectory(), file.name]];
        self.audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:urlFile error:nil];
        [self.audioPlayer setDelegate:self];
        [self.audioPlayer prepareToPlay];
        [self.audioPlayer setCurrentTime:file.highlight];
        [self.audioPlayer play];
        
        // 只播Time_for_Content秒
        NSTimeInterval timeLimit = 0;
        if (self.audioPlayer.duration < Time_for_Content) {
            timeLimit = self.audioPlayer.duration;
        } else {
            timeLimit = Time_for_Content;
        }
        self.timeCount = 0;
        self.timer = [NSTimer scheduledTimerWithTimeInterval:timeLimit target:self selector:@selector(updatetimer) userInfo:nil repeats:YES];
        [self.timer setFireDate:[NSDate date]];
        
        // GA
     //   [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:@"tracking" action:@"preview_file" label:self.trackTag value:nil] build]];
    } else {
        if (!self.isBreakSend) {
            // GA
      //      [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:@"tracking" action:@"preview_break" label:self.trackTag value:nil] build]];
            self.isBreakSend = YES;
        }
    }
}

// 預聽檔案
- (void)playFileInContentTime:(SoundFile*)file
{
    if (!self.isSpeaking) {
        NSURL *urlFile = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/Documents/%@.mp4", NSHomeDirectory(), file.name]];
        self.audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:urlFile error:nil];
        [self.audioPlayer setDelegate:self];
        [self.audioPlayer prepareToPlay];
        [self.audioPlayer setCurrentTime:file.highlight];
        [self.audioPlayer play];
        
        // 只播contentTime秒
        self.timeCount = 0;
        self.timer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(updateTimerByContent) userInfo:nil repeats:YES];
        [self.timer setFireDate:[NSDate date]];
        
        // GA
   //     [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:@"reviewing" action:@"preview_file" label:self.trackTag value:nil] build]];
    } else {
        if (!self.isBreakSend) {
            // GA
        //    [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:@"reviewing" action:@"preview_break" label:self.trackTag value:nil] build]];
            self.isBreakSend = YES;
        }
    }
}

- (void)updateTimerByContent
{
    NSTimeInterval playTime = self.contentTime - self.timeCount;
    if (playTime <= 1.0 && playTime > 0.6) { self.audioPlayer.volume = 0.5; }
    if (playTime <= 0.6 && playTime > 0.3) { self.audioPlayer.volume = 0.3; }
    if (playTime <= 0.3) { self.audioPlayer.volume = 0.2; }
    
    if (self.timeCount >= self.contentTime) {
        [self.audioPlayer stop];
        [self.timer invalidate];
    }
    self.timeCount += 0.1;
}

- (void)updatetimer
{
    if (self.timeCount) {
        [self.audioPlayer stop];
        [self.timer invalidate];
    }
    self.timeCount ++;
}

- (void)playSound
{
    if (!self.isSpeaking) {
        NSURL *url = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"part_rl" ofType:@"mp4"]];
        self.audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:nil];
        [self.audioPlayer setDelegate:self];
        [self.audioPlayer prepareToPlay];
        [self.audioPlayer play];
    }
}

- (void)ticking
{
    self.previewTime ++;
}

#pragma mark - VoiceOver

// VoiceOver結束事件
- (void)didFinishAnnouncement:(NSNotification *)dict
{
    NSString *valueSpoken = [[dict userInfo] objectForKey:UIAccessibilityAnnouncementKeyStringValue];
    
    for(int i = 0; i < self.items.count; i++) {
        ListTableViewCell *cell = (ListTableViewCell*)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
        NSString *itemTitle = [NSString stringWithFormat:@"%@ , %@",cell.lblTitle.text,cell.lblSubTitle.text];
        if (cell.voiceTitle)
            itemTitle = cell.voiceTitle;
        if ([itemTitle isEqualToString:valueSpoken] && cell.isAccessibilityFocused) {
            self.isSpeaking = NO;
            // 念完標題，等一下再預聽
            [self performSelector:@selector(playTitle:) withObject:NSLocalizedString(@"Preview", nil) afterDelay:Time_before_Preview];
            [self performSelector:@selector(playCellList:) withObject:cell afterDelay:Time_before_Play];
            break;
        }
    }
    if ([self.tempSpeech isEqualToString:valueSpoken]) {
        self.isSpeaking = NO;
        self.tempSpeech = @"";
        
        if (![valueSpoken isEqualToString:NSLocalizedString(@"Preview", nil)] && ![self.trackTag isEqualToString:@"file"] && ![self.trackTag isEqualToString:@"search"] && ![self.trackTag isEqualToString:@"result"]) {
            if (self.previewTimer)
                [self.previewTimer invalidate];
            
            // GA
     //       [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:@"tracking" action:@"preview_text" label:self.trackTag value:[NSNumber numberWithInt:self.previewTime]] build]];
            
            self.previewTime = 0;
        }
    }
    
    // 聆聽location editActions
    if ([valueSpoken isEqualToString:NSLocalizedString(@"DeleteAction", nil)] || [valueSpoken isEqualToString:NSLocalizedString(@"RenameAction", nil)] || [valueSpoken isEqualToString:NSLocalizedString(@"DeleteButton", nil)]) {
        [self cancelPlay];
    }
}

- (BOOL)accessibilityPerformMagicTap
{
    // GA
    //[[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:@"tracking" action:@"magic_tap" label:self.trackTag value:nil] build]];
    
    //self.shareSettings.doubleTapped = YES;
    [self.navigationController popViewControllerAnimated:YES];
    return YES;
}

- (BOOL)accessibilityPerformEscape
{
    // GA
   // [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:@"tracking" action:@"escape" label:self.trackTag value:nil] build]];
    
    [self.navigationController popViewControllerAnimated:YES];
    return true;
}

#pragma mark - Navigation

- (void)popViewControllerAnimated
{
    // GA
 //   [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:@"tracking" action:@"press_back" label:self.trackTag value:nil] build]];
    
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - ToolBar Actions

- (void)homeAction
{
    // GA
  //  [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:@"tracking" action:@"back_home" label:self.trackTag value:nil] build]];
    
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)helpAction
{
    // 先停止預聽
    [self cancelPlay];
    
    NSString *helpKey = @"";
    if ([self.trackTag isEqualToString:@"date"] || [self.trackTag isEqualToString:@"dateinmonth"]) {
        helpKey = @"HelpDateList";
    } else if ([self.trackTag isEqualToString:@"category"]) {
        helpKey = @"HelpGroupList";
    } else if ([self.trackTag isEqualToString:@"location"]) {
        helpKey = @"HelpLocationList";
    } else if ([self.trackTag isEqualToString:@"file"]) {
        helpKey = @"HelpFile";
    }
    /*
    UIAlertView *alertHelp = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Instructions", nil)
                                                        message:NSLocalizedString(helpKey, nil)
                                                       delegate:self
                                              cancelButtonTitle:nil
                                              otherButtonTitles:NSLocalizedString(@"Close", nil), nil];
    [alertHelp show];
    */
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Instructions", nil) message:NSLocalizedString(helpKey, nil) preferredStyle:UIAlertControllerStyleAlert];
          
          UIAlertAction *close =[UIAlertAction actionWithTitle:NSLocalizedString(@"Close", nil) style:UIAlertActionStyleCancel handler:nil];

          [alertController addAction:close];
          [self presentViewController:alertController animated:YES completion:nil];
          
    
    // GA
  //  [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:@"tracking" action:@"get_help" label:self.trackTag value:nil] build]];
}

#pragma mark - Edit Actions

- (void)showEditActions {}
- (void)showShareActions {}
- (void)showAlertDelete {}
- (void)showAlertRename {}

@end
