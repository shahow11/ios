//
//  MainViewController.h
//  NewHearMe
//
//  Created by IUILab Air on 2016/3/5.
//  Copyright © 2016年 IUILAB. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
//#import <GoogleAnalytics/GAI.h>
//#import <GoogleAnalytics/GAIDictionaryBuilder.h>
//#import <GoogleAnalytics/GAIFields.h>

@interface MainViewController : UIViewController<UIAlertViewDelegate, CLLocationManagerDelegate>

@end
