//
//  FileViewController.h
//  NewHearMe
//
//  Created by IUILab Air on 2016/3/6.
//  Copyright © 2016年 IUILAB. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <AudioToolbox/AudioToolbox.h>
#import <MessageUI/MessageUI.h>
#import "ListViewController.h"

@interface FileViewController : ListViewController<UIAlertViewDelegate, UIActionSheetDelegate, UITextFieldDelegate, AVAudioPlayerDelegate, MFMailComposeViewControllerDelegate>

@property BOOL isLoadEnough;
@property (nonatomic, strong) NSString *backText;
@property (nonatomic, strong) NSString *query;

@end
