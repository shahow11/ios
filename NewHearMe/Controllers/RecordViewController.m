//
//  RecordViewController.m
//  NewHearMe
//
//  Created by IUILab Air on 2016/3/5.
//  Copyright © 2016年 IUILAB. All rights reserved.
//

#import "RecordViewController.h"
#import "MainViewController.h"
#import "AFRecordView.h"
#import "AFAlertView.h"
#import "SoundGroup.h"
#import "SoundSection.h"
#import "ShareSettings.h"
#import "FMDatabase.h"
#import "FMDatabaseAdditions.h"
#import "UIUtility.h"
#import "PushUpSegue.h"
@import Firebase;

#define MAX_RECORD_TIME         10800 // 3小時
#define SECONDS_AHEAD           0
#define SECONDS_BETWEEN         3
#define WAVE_UPDATE_FREQUENCY   0.05
#define SOUND_METER_COUNT       40

typedef NS_ENUM(NSInteger, REC_STATUS) {
    RECORD = 0,
    PAUSE,
    RESUME,
    FINISH,
    STOP,
    CLOSE
};

@interface RecordViewController ()
{
    FMDatabase *db;
    
    BOOL isRecordingStart;
    float recordTime;
    float fileTimeStamp;
    float newDuration;
    float lastSectionTime;
    int namingTime;
    int waitingTime;
    int newVoiceID;
    int newSectionID;
    
    NSDate *nowDate;
    NSDate *lastDate;
    NSTimer *timerMeter;
    NSTimer *timerWait;
    AVAudioSession *audioSession;
    
    NSMutableArray *categories;
    NSMutableArray *sections;
    NSMutableDictionary *recordSetting;
    NSString *defaultFileName;
    NSString *recorderFilePath;
    NSString *dateStamp;
    NSString *lengthString;
}

@property (weak, nonatomic) IBOutlet UIView *actionView;
@property (weak, nonatomic) IBOutlet AFRecordView *displayPanel;
@property (weak, nonatomic) IBOutlet UIView *infoPanel;
@property (weak, nonatomic) IBOutlet UILabel *lblPart;
@property (weak, nonatomic) IBOutlet UILabel *lblHint;
@property (weak, nonatomic) IBOutlet UILabel *lblState;
@property (weak, nonatomic) IBOutlet UILabel *lblTime;
@property (weak, nonatomic) IBOutlet UIImageView *imgRecord;
@property (weak, nonatomic) IBOutlet UIButton *btnFinish;

@property (strong, nonatomic) ShareSettings *shareSettings;
@property (strong, nonatomic) CLLocationManager *locationManager;

- (IBAction)doubleTapGesture:(id)sender;
- (IBAction)oneTapGesture:(id)sender;
- (IBAction)oneTwiceGesture:(id)sender;
- (IBAction)swipeRightGesture:(id)sender;


@end

@implementation RecordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // 初始化設定(only once)
    self.shareSettings = [ShareSettings sharedSettings];
    
    categories = [[NSMutableArray alloc] init];
    self.lblPart.textColor = [UIUtility BLUE];
    self.lblState.textColor = [UIUtility BLUE];
    self.lblTime.textColor = [UIUtility BLUE];
    self.lblHint.text = NSLocalizedString(@"Double tap to resume", nil);
    self.infoPanel.accessibilityHint = NSLocalizedString(@"Double fingers double tap to record", nil);
    self.actionView.accessibilityTraits = UIAccessibilityTraitAllowsDirectInteraction;
    [self.displayPanel setNeedsDisplay];
    
    self.infoPanel.backgroundColor = [UIUtility BLUE_XD];
    
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
    
    // 設定VoiceOver
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didFinishAnnouncement:) name:UIAccessibilityAnnouncementDidFinishNotification object:nil];
    
    self.btnFinish.hidden = UIAccessibilityIsVoiceOverRunning();
    
    db = self.openDatabase;
    if (![db open]) {
        NSLog(@"Could not open db.");
    }
    
    // 準備群組清單
    FMResultSet *rs = [db executeQuery:@"SELECT g_id, g_name, g_sequence FROM GROUP_TABLE ORDER BY g_sequence"];
    while ([rs next])
    {
        SoundGroup *groupItem = [SoundGroup groupByID:[rs intForColumn:@"g_id"] name:NSLocalizedString([rs stringForColumn:@"g_name"], nil) seq:[rs intForColumn:@"g_sequence"]];
        [categories addObject:groupItem];
    }
    
    
    // 更新地點
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    self.locationManager.delegate = self;
    [self.locationManager requestLocation];
    
    [self clearVariables];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    // VoiceOver Focus
    UIAccessibilityPostNotification(UIAccessibilityScreenChangedNotification, self.infoPanel);
    
    // 開始錄音
    [self magicStart];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Recording Functions

// 初始化設定(every new record)
- (void)clearVariables
{
    newVoiceID = 1;
    newSectionID = 1;
    recordTime = 0;
    fileTimeStamp = 0;
    lastSectionTime = 0;
    isRecordingStart = FALSE;
    self.isPaused = FALSE;
    sections = [[NSMutableArray alloc] init];
    [self.imgRecord setImage:[UIImage imageNamed:@"record_b.png"]];
    self.lblState.text = @"";
    self.lblState.textColor = [UIUtility BLUE];
    self.lblTime.text = @"00:00";
    [self.lblHint setAlpha:0.0];
    FMResultSet *rs = [db executeQuery:@"SELECT MAX(v_id) FROM VOICE_LIST;"];
    if ([rs next]){
        newVoiceID = newVoiceID + [rs intForColumnIndex:0];
    }
}

// 更新介面狀態, 音效震動
- (void)setLayoutStatus:(REC_STATUS)status
{
    switch (status) {
        case RECORD:
            isRecordingStart = TRUE;
            nowDate = [NSDate date];
            [self.lblTime setHidden:NO];
            self.lblTime.text = @"00:00";
            self.lblState.textColor = [UIUtility RED];
            [self animatelblState:NSLocalizedString(@"Recording", nil)];
            [self.imgRecord setImage:[UIImage imageNamed:@"record_r.png"]];
            
            // 防止螢幕鎖定
            [[UIApplication sharedApplication] setIdleTimerDisabled:YES];
            
            AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
            //UIAccessibilityPostNotification(UIAccessibilityAnnouncementNotification, NSLocalizedString(@"Start recording", nil));
            //if (!UIAccessibilityIsVoiceOverRunning()) {
            [self playSound:@"start_r"];
            //}
            break;
        case PAUSE:
            self.isPaused = TRUE;
            [self animatePauseLabel:TRUE];
            [self.imgRecord setImage:[UIImage imageNamed:@"record_b.png"]];
            [self animatelblState:NSLocalizedString(@"Pause", nil)];
            self.lblState.textColor = [UIUtility BLUE];
            [self playSound:@"pau_res_rl"];
            UIAccessibilityPostNotification(UIAccessibilityAnnouncementNotification,
                                            NSLocalizedString(@"Pause recording", nil));
            break;
        case RESUME:
            self.isPaused = FALSE;
            [self animatePauseLabel:FALSE];
            [self.imgRecord setImage:[UIImage imageNamed:@"record_r.png"]];
            [self animatelblState:NSLocalizedString(@"Recording", nil)];
            self.lblState.textColor = [UIUtility RED];
            break;
        case FINISH:
            self.isPaused = TRUE;
            [self.imgRecord setImage:[UIImage imageNamed:@"record_b.png"]];
            [self animatelblState:NSLocalizedString(@"Save", nil)];
            self.lblState.textColor = [UIUtility BLUE];
            [self playSound:@"finish_r"];
            AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
            break;
        case STOP:
            [self animatelblState:NSLocalizedString(@"Finish", nil)];
            break;
        case CLOSE:
            [self clearVariables];
            [self.displayPanel clearMeterItem];
            [self.lblTime setHidden:YES];
            [self.actionView setIsAccessibilityElement:NO];
            [self.infoPanel setIsAccessibilityElement:YES];
            // 恢復螢幕鎖定
            [[UIApplication sharedApplication] setIdleTimerDisabled:NO];
            UIAccessibilityPostNotification(UIAccessibilityScreenChangedNotification, self.infoPanel);
            break;
        default:
            break;
    }
}

- (void)magicStart
{
    // 開始錄音
    if (!isRecordingStart) {
        //視窗動畫
        [self.actionView setIsAccessibilityElement:YES];
        [self.infoPanel setIsAccessibilityElement:NO];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"mainDropRecord" object:nil];
        UIAccessibilityPostNotification(UIAccessibilityScreenChangedNotification, self.actionView);

        // 開始錄音
        [self startRecording];
    }
    else {
        // 結束錄音, 儲存檔案
        if ([nowDate timeIntervalSinceNow] < -1) {
            [self pauseRecording];
            [self setLayoutStatus:FINISH];
            [self performSelector:@selector(saveRecording) withObject:nil afterDelay:0.5];
        }
    }
}

- (FMDatabase*)openDatabase
{
    NSArray *pathsToDocuments = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [pathsToDocuments objectAtIndex:0];
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"HearMe.db"];
    
    return [FMDatabase databaseWithPath:filePath];
}

// 開始一段錄音, 準備介面,設定recorder
- (void)startRecording
{
    BOOL isJustFinished = FALSE;
    if (nowDate != nil) {
        if ([nowDate timeIntervalSinceNow] > -1) {
            isJustFinished = TRUE;
        }
    }
    
    if (!isJustFinished) {
        // GA (New Screen)
        /*
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker set:kGAIScreenName value:SCREEN_NAME_RECORD];
        [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
        */
        [self setLayoutStatus:RECORD];
        [self performSelector:@selector(setRecorder) withObject:nil afterDelay:0.4];
    }
}

- (void)setRecorder
{
    NSLog(@"volume: %f BEFORE", [AVAudioSession sharedInstance].outputVolume);
    audioSession = [AVAudioSession sharedInstance];
    NSError *err = nil;
    [audioSession setCategory :AVAudioSessionCategoryMultiRoute error:&err];
    if (err){
        NSLog(@"audioSession: %@ %ld %@", [err domain], (long)[err code], [[err userInfo] description]);
        return;
    }
    [audioSession setActive:YES error:&err];
    err = nil;
    if (err){
        NSLog(@"audioSession: %@ %ld %@", [err domain], (long)[err code], [[err userInfo] description]);
        return;
    }
    
    NSLog(@"volume: %f AFTER", [AVAudioSession sharedInstance].outputVolume);
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleInterruption:) name:@"InterruptionNotification" object:nil];
    
    recordSetting = [[NSMutableDictionary alloc] init];
    
    // You can change the settings for the voice quality
    [recordSetting setValue :[NSNumber numberWithInt:kAudioFormatMPEG4AAC] forKey:AVFormatIDKey];
    [recordSetting setValue:[NSNumber numberWithFloat:44100.0] forKey:AVSampleRateKey];
    [recordSetting setValue:[NSNumber numberWithInt: 2] forKey:AVNumberOfChannelsKey];
    
    // 儲存檔案名稱位置
    nowDate = [NSDate date];
    NSString *date1 =[[UIUtility dateFormatterYYMMdd] stringFromDate:nowDate];
    NSString *date2 =[[UIUtility dateFormatterHHmmss] stringFromDate:nowDate];
    
    dateStamp = [NSString stringWithFormat:@"%@%@", date1, date2];
    defaultFileName = [NSString stringWithFormat:@"%@_%@", date1, date2];
    
    recorderFilePath = [NSString stringWithFormat:@"%@/Documents/%@.mp4", NSHomeDirectory(), defaultFileName];
    NSLog(@"%@", defaultFileName);
    
    NSURL *url = [NSURL fileURLWithPath:recorderFilePath];
    
    err = nil;
    NSData *audioData = [NSData dataWithContentsOfFile:[url path] options: 0 error:&err];
    if (audioData)
    {
        NSFileManager *fm = [NSFileManager defaultManager];
        [fm removeItemAtPath:[url path] error:&err];
    }
    
    err = nil;
    self.recorder = [[ AVAudioRecorder alloc] initWithURL:url settings:recordSetting error:&err];
    if (!self.recorder){
        NSLog(@"recorder: %@ %ld %@", [err domain], (long)[err code], [[err userInfo] description]);
        /*
        UIAlertView *alert =
        [[UIAlertView alloc] initWithTitle: @"Warning"
                                   message: [err localizedDescription]
                                  delegate: nil
                         cancelButtonTitle:@"OK"
                         otherButtonTitles:nil];
        [alert show];
       */
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Warning"
                                    message:[err localizedDescription] preferredStyle:UIAlertControllerStyleAlert];
               
               UIAlertAction *cancel =[UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleCancel handler:nil];


               [alertController addAction:cancel];
               [self presentViewController:alertController animated:YES completion:nil];
        
        return;
    }
    
    [self.recorder setDelegate:self];
    [self.recorder prepareToRecord];
    self.recorder.meteringEnabled = YES;
    
    BOOL audioHWAvailable = audioSession.inputAvailable;
    if (! audioHWAvailable) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Warning" message:@"Audio input hardware not available" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *cancel =[UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleCancel handler:nil];


        [alertController addAction:cancel];
        [self presentViewController:alertController animated:YES completion:nil];
/*
        UIAlertView *cantRecordAlert =
        [[UIAlertView alloc] initWithTitle: @"Warning"
                                   message: @"Audio input hardware not available"
                                  delegate: nil
                         cancelButtonTitle:@"OK"
                         otherButtonTitles:nil];
        [cantRecordAlert show];
 */
        return;
    }
    
    // 設定錄音時間上限
    [self.recorder recordForDuration:(NSTimeInterval) MAX_RECORD_TIME];
    timerMeter = [NSTimer scheduledTimerWithTimeInterval:WAVE_UPDATE_FREQUENCY target:self selector:@selector(updateMeters) userInfo:nil repeats:YES];
    
    // GA
    /*
    NSMutableDictionary *event = [[GAIDictionaryBuilder createEventWithCategory:@"recording" action:@"record_start" label:defaultFileName value:nil] build];
    [[GAI sharedInstance].defaultTracker send:event];
     */
}

// 停止錄音
- (void)stopRecording
{
    [self setLayoutStatus:STOP];
    
    // 記錄錄音長度
    newDuration = self.recorder.currentTime;
    
    // GA Timing
   // int nDuration = floor(newDuration*10);
  //  [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createTimingWithCategory:@"recording" interval:[NSNumber numberWithInt:nDuration] name:@"record_length" label:@"record"] build]];
    
    [self.recorder stop];
    [timerMeter invalidate];
    [audioSession setActive:NO error:nil];
}

// 儲存錄音
- (void)saveRecording
{
    // GA
    /*
    NSMutableDictionary *event = [[GAIDictionaryBuilder createEventWithCategory:@"recording" action:@"record_finish" label:self.lblTime.text value:[NSNumber numberWithInt:(newDuration * 10)]] build];
    [[GAI sharedInstance].defaultTracker send:event];
    */
    self.savingView = [[SavingView alloc] init];
    self.savingView.delegate = self;
    self.savingView.categories = categories;
    self.savingView.defaultFileName = defaultFileName;
    
    [self.savingView show];
}

// 暫停錄音
- (void)pauseRecording
{
    [self.recorder pause];
    [timerMeter setFireDate:[NSDate distantFuture]];
}

// 繼續錄音
- (void)resumeRecording
{
    [self.recorder record];
    [timerMeter setFireDate:[NSDate date]];
}

// 記錄段落標記（與前一個間隔大於N秒才能再加）
- (void)createSection
{
    if (recordTime - lastSectionTime >= SECONDS_BETWEEN) {
        // 標籤動畫
        NSString *sectionName = [NSString stringWithFormat:@"%@ %d", NSLocalizedString(@"Part", nil), newSectionID];
        [self animatelblPart:sectionName];
        
        // 聲音回饋(短暫停止錄音)
        [self pauseRecording];
        [self playSound:@"part_rl"];
        [self performSelector:@selector(resumeRecording) withObject:nil afterDelay:0.4];
        
        // 提前N秒
        float sectionTime = recordTime - SECONDS_AHEAD;
        SoundSection *newSec = [SoundSection sectionOfFile:newSectionID vid:newVoiceID name:sectionName timestamp:sectionTime];
        NSLog(@"new Section %@ at %f",sectionName,sectionTime);
        [sections addObject:newSec];
        
        // GA
        /*
        NSMutableDictionary *event = [[GAIDictionaryBuilder createEventWithCategory:@"recording" action:@"record_part" label:sectionName value:nil] build];
        [[GAI sharedInstance].defaultTracker send:event];
        */
        newSectionID++;
        lastSectionTime = recordTime;
    }
}

// 播放音效
- (void)playSound:(NSString *)fileName
{
    NSURL *url = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:fileName ofType:@"mp3"]];
    NSError *err;
    self.audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&err];
    if (err) {
        NSLog(@"PlaySound Error: %@", [err localizedDescription]);
    } else {
        [self.audioPlayer setDelegate:self];
        [self.audioPlayer play];
    }
}

- (void)playTitle:(NSString *)fileName
{
    NSURL *url = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/Documents/%@.mp4", NSHomeDirectory(),fileName]];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:[url path]]) {
        NSLog(@"Audio intro file not found!!!");
    }
    
    NSError *error;
    self.audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&error];
    if (error)
    {
        NSLog(@"Error in audioPlayer: %@", [error localizedDescription]);
    } else {
        [self.audioPlayer setDelegate:self];
        [self.audioPlayer play];
    }
}

- (void)updateDatabase
{
    // 新增Voice資料庫
    NSString *sqlInsert = [NSString stringWithFormat:@"INSERT INTO VOICE_LIST (v_id, v_gid, v_name, v_author, v_lat, v_lon, v_lid, v_duration, v_highlight, v_date, v_time, v_public) VALUES (%d, %d, '%@', '%@', %.6f, %.6f, %d, %f, %.1f, '%@', '%@', %d);", newVoiceID, self.savingView.newCategoryID, self.savingView.fileNameString, @"User", self.shareSettings.userLatitude, self.shareSettings.userLongitude, self.shareSettings.lid, newDuration, fileTimeStamp, [dateStamp substringToIndex:6], [dateStamp substringFromIndex:6], 1];

    if (![db executeUpdate:sqlInsert]){
        NSLog(@"Could not INSERT VOICE_LIST: %@ SQL: %@", [db lastErrorMessage], sqlInsert);
    }
    newVoiceID += 1;
    
    // 新增Section資料庫
    for (int i = 0; i < sections.count; i++) {
        SoundSection *aSec = sections[i];
        NSString *sqlInsert = [NSString stringWithFormat:@"INSERT INTO SECTION_TABLE (s_vid, s_sid, s_name, s_timestamp) VALUES (%d, %d, '%@', %.2f);", aSec.vid, aSec.sid, aSec.name, aSec.timestamp];
        if (![db executeUpdate:sqlInsert]){
            NSLog(@"Could not INSERT SECTION_TABLE: %@ SQL: %@", [db lastErrorMessage], sqlInsert);
        }
    }
    
    // GA
    /*
    NSMutableDictionary *event = [[GAIDictionaryBuilder createEventWithCategory:@"saving" action:@"record_save" label:self.savingView.fileNameString value:nil] build];
    [[GAI sharedInstance].defaultTracker send:event];
     */
}

- (void)renameFiles:(NSString *)newFileName
{
    if (![newFileName isEqualToString:defaultFileName]) {
        NSFileManager *manager = [NSFileManager defaultManager];
        NSString *docPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
        
        // 重新命名檔案
        NSString *filePathSrc = [docPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.mp4", defaultFileName]];
        NSString *filePathDst = [docPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.mp4", newFileName]];
         NSLog(@"filename path :%@", filePathDst);
        if ([manager fileExistsAtPath:filePathSrc]) {
            NSError *error = nil;
            [manager moveItemAtPath:filePathSrc toPath:filePathDst error:&error];
            if (error) {
                NSLog(@"Rename file Error: %@", error);
            }
        } else {
            NSLog(@"File %@.mp4 doesn't exists", defaultFileName);
        }
        
        // 重新命名語音簡介
        filePathSrc = [docPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@_Intro.mp4", defaultFileName]];
        filePathDst = [docPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@_Intro.mp4", newFileName]];
        if ([manager fileExistsAtPath:filePathSrc]) {
            NSError *error = nil;
            [manager moveItemAtPath:filePathSrc toPath:filePathDst error:&error];
            if (error) {
                NSLog(@"Rename audio intro Error: %@", error);
            }
        } else {
            NSLog(@"File %@_Intro.mp4 doesn't exists", defaultFileName);
        }
        
        // GA
    //    NSMutableDictionary *event = [[GAIDictionaryBuilder createEventWithCategory:@"saving" action:@"record_naming" label:newFileName value:[NSNumber numberWithInt:namingTime]] build];
   //     [[GAI sharedInstance].defaultTracker send:event];
    }
}

- (void)uploadToGCS:(NSString *)fileName{
//    NSString *docPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    //NSString *objectname=[NSString stringWithFormat:@"%@.mp4", fileName];
    
    
    
    db = self.openDatabase;
    if (![db open]) {
        NSLog(@"Could not open db.");
    }
    
    BOOL res=false;
    /*
    if (![fileName isEqualToString:defaultFileName]) {
    }else{
          res = [db executeUpdate:@"insert into RENAME_TABLE values (?,?)", defaultFileName, defaultFileName];
    }
    */
    res = [db executeUpdate:@"insert into RENAME_TABLE values (?,?)", defaultFileName, fileName];

    
      if (res == NO)NSLog(@"插入失败");
     
      [db close];
    
   // NSString *filePath = [docPath stringByAppendingPathComponent:objectname];
    
    //NSURL *urllink = [[NSURL alloc]initFileURLWithPath:objectname];
    NSURL *urllink = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/Documents/%@.mp4", NSHomeDirectory(),fileName]];

     FIRStorage *storage = [FIRStorage storage];
    FIRStorageReference *storageRef = [storage reference];
    FIRStorageReference *voiceRef = [storageRef child:defaultFileName];
     [voiceRef putFile:urllink
                                                 metadata:nil
                                               completion:^(FIRStorageMetadata *metadata,
                                                            NSError *error) {
      if (error != nil) {
          NSLog(@"upload Error: %@", error);
          
      } else {
        
      }
    }];
}

- (void)saveAction:(id)savingView
{
    [self.savingView close];
    
    // 真正停止錄音
    [self stopRecording];
    
    // 更新資料庫
    [self updateDatabase];
    
    // 重新命名檔案
    NSString *filename=((SavingView *)savingView).fileNameString;
    [self renameFiles:filename];
    [self uploadToGCS:filename];
    //upload to Firebase storage
    
    AFAlertView *checkView = [[AFAlertView alloc] init];
    checkView.customHeight = 100;
    double x = ([[UIScreen mainScreen] bounds].size.width - 264) / 2;
    double y = ([[UIScreen mainScreen] bounds].size.height - 100) / 2;
    
    UIView *whiteBack = [[UIView alloc] initWithFrame:CGRectMake(x, y, 264, 100)];
    whiteBack.backgroundColor = [UIUtility WHITE];
    whiteBack.layer.cornerRadius = 4;
    whiteBack.layer.shadowOffset = CGSizeZero;
    whiteBack.layer.shadowRadius = 6;
    whiteBack.layer.shadowOpacity = 0.5;
    whiteBack.layer.opacity = 0.9;
    
    UILabel *lblMessage = [[UILabel alloc] initWithFrame:CGRectMake(12, 40, 240, 20)];
    lblMessage.textColor = [UIUtility BLUE_D];
    [lblMessage setFont:[UIFont systemFontOfSize:17]];
    [lblMessage setTextAlignment:NSTextAlignmentCenter];
    [lblMessage setBackgroundColor:[UIColor clearColor]];
    [lblMessage setText:NSLocalizedString(@"Successfully Saved!", nil)];
    
    [checkView show];
    [checkView addSubview:whiteBack];
    [whiteBack addSubview:lblMessage];
    
    [self performSelector:@selector(closeRecordView:) withObject:checkView afterDelay:2.5];
}

- (void)cancelAction:(id)savingView
{
    [self.savingView close];
    self.savingView = nil;
    
    [self setLayoutStatus:RESUME];
    
    // GA
    /*
    NSMutableDictionary *event = [[GAIDictionaryBuilder createEventWithCategory:@"saving" action:@"record_cancelsave" label:@"record" value:nil] build];
    [[GAI sharedInstance].defaultTracker send:event];
    
    // GA (New Screen)
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:SCREEN_NAME_RECORD];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    */
    UIAccessibilityPostNotification(UIAccessibilityAnnouncementNotification,
                                    NSLocalizedString(@"Continue recording", nil));
    
    if (!UIAccessibilityIsVoiceOverRunning()) {
        [self playSound:@"pau_res_rl"];
        [self performSelector:@selector(resumeRecording) withObject:nil afterDelay:0.5];
    }
}

- (void)deleteAction:(id)savingView
{
    [self.savingView close];
    
    // 真正停止錄音
    [self stopRecording];
    
    // 刪除檔案
    NSFileManager *manager = [NSFileManager defaultManager];
    NSString *docPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
    
    // 刪除檔案
    NSString *filePathSrc = [docPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.mp4", defaultFileName]];
    if ([manager fileExistsAtPath:filePathSrc]) {
        NSError *error = nil;
        if (![[NSFileManager defaultManager] removeItemAtPath:filePathSrc error:&error]) {
            NSLog(@"Error delete record: %@", [error localizedDescription]);
        }
    } else {
        NSLog(@"File %@.mp4 doesn't exists", defaultFileName);
    }
    
    // 刪除語音簡介
    filePathSrc = [docPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@_Intro.mp4", defaultFileName]];
    if ([manager fileExistsAtPath:filePathSrc]) {
        NSError *error = nil;
        if (![[NSFileManager defaultManager] removeItemAtPath:filePathSrc error:&error]) {
            NSLog(@"Error delete audio intro: %@", [error localizedDescription]);
        }
    } else {
        NSLog(@"File %@_Intro.mp4 doesn't exists", defaultFileName);
    }

    // Show Complete Message
    AFAlertView *checkView = [[AFAlertView alloc] init];
    checkView.customHeight = 100;
    double x = ([[UIScreen mainScreen] bounds].size.width - 264) / 2;
    double y = ([[UIScreen mainScreen] bounds].size.height - 100) / 2;
    
    UIView *whiteBack = [[UIView alloc] initWithFrame:CGRectMake(x, y, 264, 100)];
    whiteBack.backgroundColor = [UIUtility WHITE];
    whiteBack.layer.cornerRadius = 4;
    whiteBack.layer.shadowOffset = CGSizeZero;
    whiteBack.layer.shadowRadius = 6;
    whiteBack.layer.shadowOpacity = 0.5;
    whiteBack.layer.opacity = 0.9;
    
    UILabel *lblMessage = [[UILabel alloc] initWithFrame:CGRectMake(12, 40, 240, 20)];
    lblMessage.textColor = [UIUtility BLUE_D];
    [lblMessage setFont:[UIFont systemFontOfSize:17]];
    [lblMessage setTextAlignment:NSTextAlignmentCenter];
    [lblMessage setBackgroundColor:[UIColor clearColor]];
    [lblMessage setText:NSLocalizedString(@"Recording deleted", nil)];
    
    [checkView show];
    [checkView addSubview:whiteBack];
    [whiteBack addSubview:lblMessage];
    
    [self performSelector:@selector(closeRecordView:) withObject:checkView afterDelay:2.5];
    
    // GA
  //  [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:@"saving" action:@"record_delete" label:@"record" value:nil] build]];
}

- (void)closeRecordView:(AFAlertView*)alert
{
    [alert dismissAnimated:YES];
    
    [self setLayoutStatus:CLOSE];
    
    // GA (New Screen)
    /*
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:SCREEN_NAME_MAIN];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    */
    //返回首頁
    [self performSegueWithIdentifier:@"backFromRecord" sender:self];
}

#pragma mark - Animations

// 段落文字動畫
- (void)animatelblPart:(NSString*)text
{
    self.lblPart.text = text;
    
    [UIView animateWithDuration:0.3 animations:^(void) {
        self.lblPart.alpha = 0.6;
    }];
    [UIView animateWithDuration:0.3 delay:0.3 options:UIViewAnimationOptionAllowAnimatedContent animations:^(void) {
        self.lblPart.alpha = 0.0;
    } completion:nil];
}

// 切換狀態文字動畫
- (void)animatelblState:(NSString*)text
{
    // 原先文字消失
    [UIView animateWithDuration:0.1 animations:^(void) {
        self.lblState.alpha = 0;
    }];
    // 換新文字出現
    [UIView animateWithDuration:0.2 animations:^(void) {
        self.lblState.text = text;
        self.lblState.alpha = 1;
    }];
}

// 暫停畫面動畫
- (void)animatePauseLabel:(BOOL)isShow
{
    if (isShow) {
        [UIView animateWithDuration:0.3 animations:^(void) {
            self.lblHint.alpha = 1.0;
        }];
    } else {
        [UIView animateWithDuration:0.3 animations:^(void) {
            self.lblHint.alpha = 0.0;
        }];
    }
}

// 更新音量表
- (void)updateMeters
{
    [self.recorder updateMeters];
    
    recordTime += WAVE_UPDATE_FREQUENCY;
    
    float timeclip = self.recorder.currentTime;
    float hours = 0.0;
    float minutes = 0.0;
    
    if (timeclip >= 3600.0){
        hours = floor(timeclip/3600);
        timeclip -= hours*3600;
    }
    if (timeclip >= 60.0) {
        minutes = floor(timeclip/60);
        timeclip -= minutes*60;
    }
    float seconds = timeclip;
    
    self.lblTime.text = hours > 0? [NSString stringWithFormat:@"%02.0f:%02.0f:%02.0f", hours, minutes, seconds] :  [NSString stringWithFormat:@"%02.0f:%02.0f", minutes, seconds];
    NSString *tempHour = hours > 0? [NSString stringWithFormat:@"%.0f%@ ", hours, NSLocalizedString(@"hours", nil)]: @"";
    NSString *tempMinutes = minutes > 0? [NSString stringWithFormat:@"%.0f%@ ", minutes, NSLocalizedString(@"minutes", nil)]: @"";
    lengthString = [NSString stringWithFormat:@"%@ %@ %@ %.0f%@", NSLocalizedString(@"Recorded", nil), tempHour, tempMinutes, seconds, NSLocalizedString(@"seconds", nil)];
    
    int level = (((int)[self.recorder averagePowerForChannel:1]) + 120) * (5.0 / 6.0); //0
    [self.displayPanel addSoundMeterItem:level];
    
    // 記錄有聲音的節點
    if (level > 80 && self.recorder.currentTime > 0.5 && fileTimeStamp == 0) {
        fileTimeStamp = self.recorder.currentTime - 0.5;
        NSLog(@"精彩瞬間: %.2f", fileTimeStamp);
    }
    
    self.recorder.meteringEnabled = YES;
}

// 暫停/中斷計時
- (void)updateWaiting
{
    waitingTime ++;
}

#pragma mark - Accessibility

- (BOOL)accessibilityPerformMagicTap {
    [self magicStart];
    return YES;
}

- (BOOL)accessibilityPerformEscape {
    // GA
    /*
    NSMutableDictionary *event = [[GAIDictionaryBuilder createEventWithCategory:@"recording" action:@"escape" label:@"record" value:nil] build];
    [[GAI sharedInstance].defaultTracker send:event];
    */
     return YES;
}

// VoiceOver結束事件
- (void)didFinishAnnouncement:(NSNotification *)dict
{
    NSString *valueSpoken = [[dict userInfo] objectForKey:UIAccessibilityAnnouncementKeyStringValue];
    
    if ([valueSpoken isEqualToString:NSLocalizedString(@"Start recording", nil)]) {
        [self playSound:@"start_r"];
    }
    if ([valueSpoken isEqualToString:NSLocalizedString(@"Resume recording", nil)]) {
        [self playSound:@"pau_res_rl"];
        [self performSelector:@selector(resumeRecording) withObject:nil afterDelay:0.5];
    }
    if ([valueSpoken isEqualToString:NSLocalizedString(@"Continue recording", nil)]) {
        [self playSound:@"start_r"];
        [self performSelector:@selector(resumeRecording) withObject:nil afterDelay:0.7];
    }
}

#pragma mark - Actions

- (IBAction)doubleTapGesture:(id)sender {
    // 開始錄音
    if (!isRecordingStart) {
        //視窗動畫
        [self.actionView setIsAccessibilityElement:YES];
        [self.infoPanel setIsAccessibilityElement:NO];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"mainDropRecord" object:nil];
        UIAccessibilityPostNotification(UIAccessibilityScreenChangedNotification, self.actionView);
        // 開始錄音
        [self startRecording];
    }
    else {
        // 結束錄音, 儲存檔案
        if ([nowDate timeIntervalSinceNow] < -1) {
            [self pauseRecording];
            [self setLayoutStatus:FINISH];
            [self performSelector:@selector(saveRecording) withObject:nil afterDelay:0.6];
        }
    }
}

- (IBAction)oneTapGesture:(id)sender {
    if (isRecordingStart) {
        if (self.isPaused) {
            UIAccessibilityPostNotification(UIAccessibilityAnnouncementNotification,
                                            [NSString stringWithFormat:@"%@, %@", NSLocalizedString(@"Recording paused", nil), lengthString]);
            // GA
          //  NSMutableDictionary *event = [[GAIDictionaryBuilder createEventWithCategory:@"recording" action:@"record_knock" label:defaultFileName value:nil] build];
       //     [[GAI sharedInstance].defaultTracker send:event];
        } else {
            AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
        }
    }
}

- (IBAction)oneTwiceGesture:(id)sender {
    if (isRecordingStart) {
        if (self.recorder.recording) {
            // 暫停錄音
            [self pauseRecording];
            [self setLayoutStatus:PAUSE];
            
            // 開始計時
            waitingTime = 0;
            timerWait = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(updateWaiting) userInfo:nil repeats:YES];
            
            // GA
        //    [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:@"recording" action:@"record_pause" label:@"record" value:nil] build]];
        }
        else {
            // 繼續錄音
            [self setLayoutStatus:RESUME];
            
            UIAccessibilityPostNotification(UIAccessibilityAnnouncementNotification,
                                            NSLocalizedString(@"Resume recording", nil));
            
            if (!UIAccessibilityIsVoiceOverRunning()) {
                [self playSound:@"pau_res_rl"];
                [self performSelector:@selector(resumeRecording) withObject:nil afterDelay:0.5];
            }
            
            // 結束計時
            [timerWait invalidate];
           // [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createTimingWithCategory:@"recording" interval:[NSNumber numberWithInt:waitingTime] name:@"record_pause_length" label:@"record"] build]];
            
            // GA
            //[[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:@"recording" action:@"record_resume" label:@"record" value:[NSNumber numberWithInt:waitingTime]] build]];
        }
    }
}

- (IBAction)swipeRightGesture:(id)sender {
    if (isRecordingStart && !self.isPaused) {
        [self createSection];
    }
}

#pragma mark - AVAudioRecorder Delegate

- (void)audioRecorderDidFinishRecording:(AVAudioRecorder *)recorder successfully:(BOOL)flag
{
    NSLog(@"RecorderDidFinishRecording sucess %d",flag);
    if (flag) {
        // 達到錄音上限，強制錄音儲存，回到預設畫面
        if (recordTime >= MAX_RECORD_TIME) {
            // 停止錄音
            [self.imgRecord setImage:[UIImage imageNamed:@"record_b.png"]];
            self.lblState.textColor = [UIUtility BLUE];
            [self playSound:@"finish_r"];
            AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
            [self setLayoutStatus:STOP];
            
            // 記錄錄音長度
            newDuration = self.recorder.currentTime;
          //  int nDuration = floor(newDuration*10);
            
            // GA Timing
      //      [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createTimingWithCategory:@"recording" interval:[NSNumber numberWithInt:nDuration] name:@"record_length" label:@"record"] build]];
            
            [timerMeter invalidate];
            [audioSession setActive:NO error:nil];
            
            // 通知視窗
            AFAlertView *checkView = [[AFAlertView alloc] init];
            checkView.customHeight = 100;
            double y = [[UIScreen mainScreen] bounds].size.height == 480? 180:234;
            
            UIView *whiteBack = [[UIView alloc] initWithFrame:CGRectMake(28, y, 264, 100)];
            whiteBack.backgroundColor = [UIUtility WHITE];
            whiteBack.layer.cornerRadius = 4;
            whiteBack.layer.shadowOffset = CGSizeZero;
            whiteBack.layer.shadowRadius = 6;
            whiteBack.layer.shadowOpacity = 0.5;
            whiteBack.layer.opacity = 0.9;
            
            UILabel *lblMessage = [[UILabel alloc] initWithFrame:CGRectMake(12, 25, 240, 50)];
            lblMessage.textColor = [UIUtility BLUE_D];
            [lblMessage setFont:[UIFont systemFontOfSize:17]];
            [lblMessage setNumberOfLines:2];
            [lblMessage setTextAlignment:NSTextAlignmentCenter];
            [lblMessage setBackgroundColor:[UIColor clearColor]];
            [lblMessage setText:NSLocalizedString(@"Reach RecordLimit", nil)];
            
            [checkView show];
            [checkView addSubview:whiteBack];
            [whiteBack addSubview:lblMessage];
            
            [self performSelector:@selector(closeRecordView:) withObject:checkView afterDelay:4.0];
        }
    }
}
/*
// AudioSession（待測試）
- (void)audioRecorderBeginInterruption:(AVAudioRecorder *)recorder
{
    NSLog(@"Recorder beginInterruption");
    
    // 設定錄音為暫停(錄音中有電話進來)
    //    [timerMeter setFireDate:[NSDate distantFuture]];
    //    self.isPaused = TRUE;
    //    [self animatePauseLabel:TRUE];
    //    [self.imgRecord setImage:[UIImage imageNamed:@"record_b.png"]];
    //    [self animatelblState:NSLocalizedString(@"Pause", nil)];
    //    self.lblState.textColor = [UIUtility BLUE];
    
    // GA
    [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:@"recording" action:@"record_interrupt" label:defaultFileName value:nil] build]];
    
    // 強制錄音儲存，回到預設畫面(應該在確定audioSession錯誤的地方)
    // [AVAudioSession Notify Thread] AVAudioSessionPortImpl.mm:52: ValidateRequiredFields: Unknown selected data source for Port iPhone È∫•ÂÖãÈ¢® (type: MicrophoneBuiltIn)
    
    // 記錄錄音長度
    newDuration = self.recorder.currentTime;
    
    // GA Timing
    int nDuration = floor(newDuration*10);
    [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createTimingWithCategory:@"recording" interval:[NSNumber numberWithInt:nDuration] name:@"record_length" label:@"record"] build]];
    
    [self.recorder stop];
    [timerMeter invalidate];
    
    // 更新資料庫
    [self updateDatabase];
    
    [self setLayoutStatus:CLOSE];
    
    // GA (New Screen)
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:SCREEN_NAME_MAIN];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

- (void)audioRecorderEndInterruption:(AVAudioRecorder *)recorder withOptions:(NSUInteger)flags
{
    NSLog(@"Recorder EndInterruption");
    // 恢復錄音介面，使用者自行繼續錄音(拒絕接聽不會進來)
    // GA
    [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:@"recording" action:@"record_recover" label:defaultFileName value:nil] build]];
    
    //    // GA Timing
    //    [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createTimingWithCategory:@"recording" interval:[NSNumber numberWithInt:waitingTime] name:@"record_interrupt_length" label:@"record"] build]];
}
*/
- (void)handleInterruption:(NSNotification *)notification {
AudioQueuePropertyID inInterruptionState = (AudioQueuePropertyID) [notification.object unsignedIntValue];
    if (inInterruptionState == kAudioSessionBeginInterruption){
    // GA
    //   [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:@"recording" action:@"record_interrupt" label:defaultFileName value:nil] build]];
       
       // 強制錄音儲存，回到預設畫面(應該在確定audioSession錯誤的地方)
       // [AVAudioSession Notify Thread] AVAudioSessionPortImpl.mm:52: ValidateRequiredFields: Unknown selected data source for Port iPhone È∫•ÂÖãÈ¢® (type: MicrophoneBuiltIn)
       
       // 記錄錄音長度
       newDuration = self.recorder.currentTime;
       
       // GA Timing
      // int nDuration = floor(newDuration*10);
     //  [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createTimingWithCategory:@"recording" interval:[NSNumber numberWithInt:nDuration] name:@"record_length" label:@"record"] build]];
       
       [self.recorder stop];
       [timerMeter invalidate];
       
       // 更新資料庫
       [self updateDatabase];
       
       [self setLayoutStatus:CLOSE];
       
       // GA (New Screen)
        /*
       id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
       [tracker set:kGAIScreenName value:SCREEN_NAME_MAIN];
       [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
         */
    }else if (inInterruptionState == kAudioSessionEndInterruption){
       //[[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:@"recording" action:@"record_recover" label:defaultFileName value:nil] build]];

    }
}
#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(nonnull NSArray<CLLocation *> *)locations
{
    CLLocation *currentLocation = [locations lastObject];
    CLGeocoder *ceo = [[CLGeocoder alloc]init];
    CLLocation *loc = [[CLLocation alloc]initWithLatitude:currentLocation.coordinate.latitude longitude:currentLocation.coordinate.longitude];
    
    // 設定目前位置
    self.shareSettings.userLatitude = currentLocation.coordinate.latitude;
    self.shareSettings.userLongitude = currentLocation.coordinate.longitude;
    
    [ceo reverseGeocodeLocation: loc completionHandler:^(NSArray *placemarks, NSError *error) {
        if (error == nil && [placemarks count] > 0) {
            CLPlacemark *placemark = [placemarks objectAtIndex:0];
            
            self.shareSettings.postalCode = placemark.postalCode;
            self.shareSettings.country = placemark.country;
            self.shareSettings.city = placemark.subAdministrativeArea;
            self.shareSettings.district = placemark.locality;
            self.shareSettings.street = placemark.thoroughfare;
            self.shareSettings.landmark = placemark.name;
            
            // 檢查是否是新地點，若是則寫入LOCATION_TABLE
            if (self.shareSettings.city != nil) {
                // 如果已有同樣地點，取得地點id
                int count = 0;
                NSString *query = [NSString stringWithFormat:@"SELECT l_id FROM LOCATION_TABLE WHERE l_country = '%@' AND l_city = '%@' AND l_district = '%@' AND l_street = '%@'", self.shareSettings.country, self.shareSettings.city, self.shareSettings.district, self.shareSettings.street];
                FMResultSet *rs = [self->db executeQuery:query];
                while ([rs next])
                {
                    count ++;
                    self.shareSettings.lid = [rs intForColumn:@"l_id"];
                }
                
                // 如果新地點，寫入db後取得新id
                if (count == 0) {
                    FMResultSet *rs2 = [self->db executeQuery:@"SELECT MAX (l_id)+1 FROM LOCATION_TABLE"];
                    while ([rs2 next]) {
                        self.shareSettings.lid = [rs2 intForColumnIndex:0];
                    }
                    
                    NSString *insert = [NSString stringWithFormat:@"INSERT INTO LOCATION_TABLE (l_id, l_postalcode, l_country, l_city, l_district, l_street, l_landmark) VALUES (%d, %@, '%@', '%@', '%@', '%@', '%@');", self.shareSettings.lid, self.shareSettings.postalCode, self.shareSettings.country, self.shareSettings.city, self.shareSettings.district, self.shareSettings.street, self.shareSettings.landmark];
                    
                    // GA
               //     NSMutableDictionary *event = [[GAIDictionaryBuilder createEventWithCategory:@"tracking" action:@"location_discover" label:self.shareSettings.landmark value:nil] build];
              //      [[GAI sharedInstance].defaultTracker send:event];
                    
                    if (![self->db executeUpdate:insert]){
                        self.shareSettings.lid = 0;
                        NSLog(@"Could not INSERT LOCATION_TABLE: %@ SQL: %@", [self->db lastErrorMessage], insert);
                    }
                }
            }
        }
    }];
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"CLLocationManager didFailWithError: %@", error.description);
}


@end
