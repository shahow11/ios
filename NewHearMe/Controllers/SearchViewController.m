//
//  SearchViewController.m
//  NewHearMe
//
//  Created by IUILab Air on 2016/3/5.
//  Copyright © 2016年 IUILAB. All rights reserved.
//

#import "SearchViewController.h"
#import "FMDatabase.h"
#import "FMDatabaseAdditions.h"
#import "ShareSettings.h"
#import "SoundFile.h"
#import "UIUtility.h"

@interface SearchViewController ()
{
    FMDatabase *db;
    BOOL isCanceled;
}

@property BOOL searchControllerWasActive;
@property BOOL searchControllerSearchFieldWasFirstResponder;
@property (strong, nonatomic) NSString *query;

@property (strong, nonatomic) UISearchController *searchController;
@property (strong, nonatomic) ListViewController *resultsTableController; // secondary search results table view

@end

@implementation SearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    isCanceled = NO;
    
    self.searchController.searchBar.delegate = self;
    self.resultsTableController = [self.storyboard instantiateViewControllerWithIdentifier:@"ResultView"];
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:self.resultsTableController];
    self.searchController.searchResultsUpdater = self;
    [self.searchController.searchBar sizeToFit];
    self.trackTag = @"search";
    self.tableView.tableHeaderView = self.searchController.searchBar;

    //self.resultsTableController.tableView.delegate = self;
    self.resultsTableController.trackTag = @"result";
    self.searchController.delegate = self;
    self.searchController.dimsBackgroundDuringPresentation = YES; // default is YES
    self.searchController.hidesNavigationBarDuringPresentation = NO; // default is YES, will cause hidden bar show again in Main
    self.searchController.searchBar.delegate = self; // so we can monitor text changes + others
    self.searchController.searchBar.showsCancelButton = YES;
    [self.searchController.searchBar setPlaceholder:NSLocalizedString(@"Search placeholder", nil)];

    self.definesPresentationContext = YES;  // know where you want UISearchController to be displayed
    
    // 準備資料庫
    NSArray *pathsToDocuments = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [pathsToDocuments objectAtIndex:0];
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"HearMe.db"];
    db = [FMDatabase databaseWithPath:filePath];
    if (![db open]) {
        NSLog(@"Could not open db.");
    }
    
    [self resetSearch];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    // 進來直接編輯
    // restore the searchController's active state
    if (self.searchControllerWasActive) {
        self.searchController.active = self.searchControllerWasActive;
        _searchControllerWasActive = NO;
        
        if (self.searchControllerSearchFieldWasFirstResponder) {
            [self.searchController.searchBar becomeFirstResponder];
            _searchControllerSearchFieldWasFirstResponder = NO;
        }
    }
    UIAccessibilityPostNotification(UIAccessibilityLayoutChangedNotification, self.searchController.searchBar.inputView);
    
    //id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    //[tracker set:kGAIScreenName value:SCREEN_NAME_SEARCH];
   // [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UISearchBarDelegate

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    [self conductSearch:searchText scope:nil];
    // GA
  //  [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:@"tracking" action:@"input_keyword" label:searchText value:nil] build]];
}

#pragma mark - Searching

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
//    // 點搜尋背景直接回上一層
//    if (!isCanceled) {
//        // GA
//        [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:@"tracking" action:@"press_background" label:@"search" value:nil] build]];
//        
//        isCanceled = NO;
//        [self.navigationController popToRootViewControllerAnimated:YES];
//    }
    //[searchBar setShowsCancelButton:YES animated:NO];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    UISearchBar *sb = self.searchController.searchBar;
    UIView *viewTop = sb.subviews[0];
    NSString *classString = @"UINavigationButton";
    for (UIView *subView in viewTop.subviews) {
        if ([subView isKindOfClass:NSClassFromString(classString)]) {
            UIButton *cancelButton = (UIButton*)subView;
            if (cancelButton.accessibilityElementIsFocused) {
                // GA
           //     [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:@"tracking" action:@"press_cancel" label:@"search" value:nil] build]];
            }
        }
    }
    
    isCanceled = YES;
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)resetSearch
{
    self.items = [[NSMutableArray alloc]init];
    self.itemTitles = [[NSMutableArray alloc]init];
    self.itemSubs = [[NSMutableArray alloc]init];
    self.itemVoice = [[NSMutableArray alloc]init];
}

- (void)conductSearch:(NSString*)searchText scope:(NSString*)scope
{
    [self resetSearch];
    
    NSString *checkedText = [self checkWording:searchText];
    
    self.query = [NSString stringWithFormat:@"SELECT * FROM VOICE_LIST, GROUP_TABLE, LOCATION_TABLE WHERE v_gid = g_id AND v_lid = l_id AND v_public = 1 AND (v_name LIKE '%%%@%%' OR g_name LIKE '%%%@%%' OR l_city LIKE '%%%@%%' OR l_street LIKE '%%%@%%' OR l_landmark LIKE '%%%@%%') ORDER BY v_id", checkedText, checkedText, checkedText, checkedText, checkedText];
    //NSLog(@"%@",self.query);
    FMResultSet *rs = [db executeQuery:self.query];
    
    NSDateFormatter *dateFormatter = [UIUtility dateFormatterMMMM_dd];
    NSDateFormatter *subDateFormatter = [UIUtility dateFormatterhhmm_a];
    
    while ([rs next])
    {
        NSString *voice = [rs stringForColumn:@"v_name"];
        NSDate *vDate = [[UIUtility dateFormatterYYMMddHHmmss] dateFromString:[NSString stringWithFormat:@"%@%@", [rs stringForColumn:@"v_date"],[rs stringForColumn:@"v_time"]]];
        NSString *defaultName = [NSString stringWithFormat:@"%@_%@",[rs stringForColumn:@"v_date"],[rs stringForColumn:@"v_time"]];
        // 如果是預設檔名，報讀口語日期時間
        if ([defaultName isEqualToString:[rs stringForColumn:@"v_name"]]) {
            voice = [NSString stringWithFormat:@"%@ , %@", [dateFormatter stringFromDate:vDate], [subDateFormatter stringFromDate:vDate]];
        }
        
        SoundFile *sound = [SoundFile fileOfGroup:[rs intForColumn:@"v_gid"]
                                              vid:[rs intForColumn:@"v_id"]
                                             name:[rs stringForColumn:@"v_name"]
                                           author:[rs stringForColumn:@"v_author"]
                                         latitude:[rs stringForColumn:@"v_lat"]
                                        longitude:[rs stringForColumn:@"v_lon"]
                                              lid:[rs intForColumn:@"v_lid"]
                                         duration:[rs intForColumn:@"v_duration"]
                                        highlight:[rs intForColumn:@"v_highlight"]
                                             date:[rs stringForColumn:@"v_date"]
                                             time:[rs stringForColumn:@"v_time"]
                                         isPublic:[rs intForColumn:@"v_public"]];
        [self.items addObject:sound];
        [self.itemTitles addObject:[rs stringForColumn:@"v_name"]];
        [self.itemSubs addObject:[NSString stringWithFormat:@"%@\r%@", [dateFormatter stringFromDate:vDate], [subDateFormatter stringFromDate:vDate]]];
        [self.itemVoice addObject:voice];
    }
}

- (NSString *)checkWording:(NSString*)searchText
{
    searchText = [searchText stringByReplacingOccurrencesOfString:@"[" withString:@"[[]"];
    searchText = [searchText stringByReplacingOccurrencesOfString:@"_" withString:@"[_]"];
    searchText = [searchText stringByReplacingOccurrencesOfString:@"%" withString:@"[%]"];
    
    return searchText;
}

#pragma mark - UISearchResultsUpdating Delegate

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController
{
    NSString *searchString = searchController.searchBar.text;
    if (![searchString isEqualToString:@""]) {
        [self conductSearch:searchString scope:nil];
        
        ListViewController *tableController = (ListViewController *)self.searchController.searchResultsController;
        tableController.items = self.items;
        tableController.itemTitles = self.itemTitles;
        tableController.itemSubs = self.itemSubs;
        tableController.itemVoice = self.itemVoice;
        [tableController.tableView reloadData];
        
        [self.tableView reloadData];
    }
}

#pragma mark - UIStateRestoration

// we restore several items for state restoration:
//  1) Search controller's active state,
//  2) search text,
//  3) first responder

NSString *const ViewControllerTitleKey = @"ViewControllerTitleKey";
NSString *const SearchControllerIsActiveKey = @"SearchControllerIsActiveKey";
NSString *const SearchBarTextKey = @"SearchBarTextKey";
NSString *const SearchBarIsFirstResponderKey = @"SearchBarIsFirstResponderKey";

- (void)encodeRestorableStateWithCoder:(NSCoder *)coder {
    [super encodeRestorableStateWithCoder:coder];
    
    // encode the view state so it can be restored later
    
    // encode the title
    [coder encodeObject:self.title forKey:ViewControllerTitleKey];
    
    UISearchController *searchController = self.searchController;
    
    // encode the search controller's active state
    BOOL searchDisplayControllerIsActive = searchController.isActive;
    [coder encodeBool:searchDisplayControllerIsActive forKey:SearchControllerIsActiveKey];
    
    // encode the first responser status
    if (searchDisplayControllerIsActive) {
        [coder encodeBool:[searchController.searchBar isFirstResponder] forKey:SearchBarIsFirstResponderKey];
    }
    
    // encode the search bar text
    [coder encodeObject:searchController.searchBar.text forKey:SearchBarTextKey];
}

- (void)decodeRestorableStateWithCoder:(NSCoder *)coder {
    [super decodeRestorableStateWithCoder:coder];
    
    // restore the title
    self.title = [coder decodeObjectForKey:ViewControllerTitleKey];
    
    // restore the active state:
    // we can't make the searchController active here since it's not part of the view
    // hierarchy yet, instead we do it in viewWillAppear
    //
    _searchControllerWasActive = [coder decodeBoolForKey:SearchControllerIsActiveKey];
    
    // restore the first responder status:
    // we can't make the searchController first responder here since it's not part of the view
    // hierarchy yet, instead we do it in viewWillAppear
    //
    _searchControllerSearchFieldWasFirstResponder = [coder decodeBoolForKey:SearchBarIsFirstResponderKey];
    
    // restore the text in the search field
    self.searchController.searchBar.text = [coder decodeObjectForKey:SearchBarTextKey];
}

@end
