//
//  EditTableViewController.m
//  NewHearMe
//
//  Created by IUILab Air on 2016/3/6.
//  Copyright © 2016年 IUILAB. All rights reserved.
//

#import "EditTableViewController.h"
#import "FMDatabase.h"
#import "FMDatabaseAdditions.h"
#import "SoundGroup.h"
#import "UIUtility.h"

@interface EditTableViewController ()
{
    BOOL isAddingGroup;
    int newGroupID;
    NSMutableArray *groups;
    NSString *newGroupName;
    
    
 //   UIAlertView  *alertRename;
  //  UIAlertView  *alertAdd;
    UIAlertController *alertRename1;
    UIAlertController *alertAdd;
    FMDatabase *db;
}

@end

@implementation EditTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    isAddingGroup = FALSE;
    newGroupID = 99;
    newGroupName = @"";
    
    self.title = NSLocalizedString(@"Category List", nil);
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(insertNewItem)];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(closeEditView)];
    
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"editCell"];
    [self.tableView setAllowsSelectionDuringEditing:YES];
    
    // 搜尋資料庫
    NSArray *pathsToDocuments = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [pathsToDocuments objectAtIndex:0];
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"HearMe.db"];
    db = [FMDatabase databaseWithPath:filePath];
    if (![db open]) {
        NSLog(@"Could not open db.");
    }
    
    [self queryList];
    
    [self.tableView setEditing:TRUE animated:NO];
}

- (void)viewDidAppear:(BOOL)animated {
    /*
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:SCREEN_NAME_CATEGORY_EDIT];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    */
    [super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return groups.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"editCell" forIndexPath:indexPath];
    
    if ( cell == nil ) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"editCell"];
    }
    
    SoundGroup *sg = groups[[indexPath row]];
    cell.tag = sg.gid;
    cell.textLabel.text = sg.name;
    [cell.textLabel setTextColor:[UIUtility GRAY_L]];
    [cell setAccessibilityHint:NSLocalizedString(@"Double tap to edit", nil)];
    
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

// 隱藏刪除按鈕圖示
- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewCellEditingStyleNone;
}

- (BOOL)tableView:(UITableView *)tableview shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath {
    return NO;
}

- (NSString*)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return NSLocalizedString(@"DeleteButton", nil);
}

// 呼叫編輯選單
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // 記錄選擇的項目
    self.editIndex = indexPath;
    //[self.tableView setEditing:NO];
   /*
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@""
                                                             delegate:self
                                                    cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                                               destructiveButtonTitle:NSLocalizedString(@"Delete category", nil)
                                                    otherButtonTitles:NSLocalizedString(@"Rename category", nil), nil];
    [actionSheet showInView:self.view];
    */
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@""
                                                                   message:nil
                                    preferredStyle:UIAlertControllerStyleActionSheet];
    [alertController addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){
        //[[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:@"editing" action:@"edit_menu_cancel" label:@"group_menu" value:nil] build]];
        
    }]];
    [alertController addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Delete category", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){[self showAlertDelete];}]];
    [alertController addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Rename category", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){[self showAlertRename];}]];
    
     [self presentViewController:alertController animated:YES completion:nil];
    
    /*
     
       if ([title isEqualToString:NSLocalizedString(@"Delete category", nil)]) {
           [self showAlertDelete];
       }
       else if ([title isEqualToString:NSLocalizedString(@"Rename category", nil)]) {
           [self showAlertRename];
       } else if ([title isEqualToString:NSLocalizedString(@"Cancel", nil)]) {
           // GA
           [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:@"editing" action:@"edit_menu_cancel" label:@"group_menu" value:nil] build]];
       }
       
       // 取消項目的選擇狀態
       [self.tableView deselectRowAtIndexPath:self.editIndex animated:YES];
     */
}

// iOS 8
- (NSArray*)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewRowAction *reameAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:NSLocalizedString(@"RenameAction", nil) handler:^(UITableViewRowAction *action, NSIndexPath *indexPath){
        // 重新命名
        self.editIndex = indexPath;
        //[self.tableView setEditing:NO];
        [self showAlertRename];
    }];
    reameAction.backgroundColor = [UIColor grayColor];
    
    UITableViewRowAction *deleteAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDestructive title:NSLocalizedString(@"DeleteAction", nil)  handler:^(UITableViewRowAction *action, NSIndexPath *indexPath){
        self.editIndex = indexPath;
        //[self.tableView setEditing:NO];
        [self showAlertDelete];
    }];
    
    return @[deleteAction, reameAction];
}

- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath {
    NSString *stringToMove = [groups objectAtIndex:sourceIndexPath.row];
    [groups removeObjectAtIndex:sourceIndexPath.row];
    [groups insertObject:stringToMove atIndex:destinationIndexPath.row];
    
    // 若有移動順序則更新
    [self reorderGroup];
}

- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50.0;
}

#pragma mark - Edit

// 新增類別
- (void)insertNewItem
{
    // GA
//    [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:@"editing" action:@"add_start" label:@"group_menu" value:nil] build]];
    
    isAddingGroup = TRUE;
    [self showAlertAdd];
}

- (void)startEditing
{
    [self.tableView setEditing:(!self.tableView.isEditing) animated:YES];
}

- (void)closeEditView
{
    // GA
//    [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:@"editing" action:@"press_done" label:@"group_menu" value:nil] build]];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)queryList
{
    groups = [[NSMutableArray alloc]init];
    
    FMResultSet *rsg = [db executeQuery:@"SELECT g_id, g_name, g_sequence FROM GROUP_TABLE WHERE g_id <> 0 ORDER BY g_sequence"];
    while ([rsg next])
    {
        SoundGroup *groupItem = [SoundGroup groupByID:[rsg intForColumn:@"g_id"] name:NSLocalizedString([rsg stringForColumn:@"g_name"], nil) seq:[rsg intForColumn:@"g_sequence"]];
        [groups addObject:groupItem];
    }
    
    FMResultSet *rs = [db executeQuery:@"SELECT MAX(g_id) FROM GROUP_TABLE"];
    while ([rs next])
    {
        newGroupID = [rs intForColumnIndex:0] + 1;
    }
    [self.tableView reloadData];
}

// 刪除類別
- (void)deleteGroup
{
    // 更新資料庫
    SoundGroup *deS = (SoundGroup*)groups[self.editIndex.row];
    NSString *qry = [NSString stringWithFormat:@"DELETE FROM GROUP_TABLE WHERE g_id = %d", deS.gid];
    if (![db executeUpdate:qry]){
        NSLog(@"ERROR DELETE group: %@", [db lastErrorMessage]);
        return;
    }
    
    // 將原先檔案移至未分類
    qry = [NSString stringWithFormat:@"UPDATE VOICE_LIST SET v_gid = 0 WHERE v_gid = %d", deS.gid];
    if (![db executeUpdate:qry]){
        NSLog(@"ERROR DELETE UPDATE: %@", [db lastErrorMessage]);
    }
    
    // 執行成功後，語音回饋
    UIAccessibilityPostNotification(UIAccessibilityAnnouncementNotification, NSLocalizedString(@"Delete complete", nil));
    [self performSelector:@selector(queryList) withObject:nil afterDelay:1.0];
    
    // GA
   // [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:@"editing" action:@"delete_group" label:deS.name value:nil] build]];
}

// 重新命名類別
- (void)renameGroup
{
    SoundGroup *deS = (SoundGroup*)groups[self.editIndex.row];
    if (![newGroupName isEqualToString:deS.name]) {
        // 更新資料庫
        NSString *qry = [NSString stringWithFormat:@"UPDATE GROUP_TABLE SET g_name = '%@' WHERE g_id = %d", newGroupName, deS.gid];
        if (![db executeUpdate:qry]){
            NSLog(@"ERROR UPDATE group: %@", [db lastErrorMessage]);
            return;
        }
        
        // 執行成功後，語音回饋
        UIAccessibilityPostNotification(UIAccessibilityAnnouncementNotification,NSLocalizedString(@"Rename complete", nil));
        [self performSelector:@selector(queryList) withObject:nil afterDelay:1.0];
        
        // GA
  //      [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:@"editing" action:@"rename_group" label:deS.name value:nil] build]];
    }
}

// 確認新增類別
- (void)addGroup
{
    // 更新資料庫
    NSString *qry = [NSString stringWithFormat:@"INSERT INTO GROUP_TABLE (g_id, g_name, g_sequence) VALUES (%d, '%@', %d)", newGroupID, newGroupName, newGroupID];
    if (![db executeUpdate:qry]){
        NSLog(@"ERROR INSERT group: %@", [db lastErrorMessage]);
        return;
    }
    
    // 執行成功後，語音回饋
    UIAccessibilityPostNotification(UIAccessibilityAnnouncementNotification, NSLocalizedString(@"Insert complete", nil));
    [self performSelector:@selector(queryList) withObject:nil afterDelay:1.0];
    
    // GA
  //  [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:@"editing" action:@"add_group" label:newGroupName value:nil] build]];
}

// 重新排列類別
- (void)reorderGroup
{
    // 檢查是否順序有換
    BOOL switchFlag = FALSE;
    for(int i = 0; i < groups.count; i++) {
        SoundGroup *sg = (SoundGroup*)groups[i];
        if (i+1 != sg.seq) {
            switchFlag = TRUE;
            break;
        }
    }
    
    if (switchFlag) {
        for(int i = 0; i < groups.count; i++) {
            SoundGroup *sg = (SoundGroup*)groups[i];
            NSString *qry = [NSString stringWithFormat:@"UPDATE GROUP_TABLE SET g_sequence = %d WHERE g_id = %d", i+1, sg.gid];
            if (![db executeUpdate:qry]){
                NSLog(@"ERROR UPDATE sequence: %@", [db lastErrorMessage]);
            }
        }
        
        // GA
     //   [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:@"editing" action:@"reorder_group" label:@"group_menu" value:nil] build]];
    }
}

#pragma mark - AlertView ActionSheet delegate
/*
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSString *title = [actionSheet buttonTitleAtIndex:buttonIndex];
    
    if ([title isEqualToString:NSLocalizedString(@"Delete category", nil)]) {
        [self showAlertDelete];
    }
    else if ([title isEqualToString:NSLocalizedString(@"Rename category", nil)]) {
        [self showAlertRename];
    } else if ([title isEqualToString:NSLocalizedString(@"Cancel", nil)]) {
        // GA
        [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:@"editing" action:@"edit_menu_cancel" label:@"group_menu" value:nil] build]];
    }
    
    // 取消項目的選擇狀態
    [self.tableView deselectRowAtIndexPath:self.editIndex animated:YES];
}
 */
- (void)showAlertDelete
{
      UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"DeleteGroupTitle", nil) message:NSLocalizedString(@"DeleteGroupMSG", nil) preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *cancel =[UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:nil];

    UIAlertAction *sure =[UIAlertAction actionWithTitle:NSLocalizedString(@"Confirm", nil) style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:sure];
    [alertController addAction:cancel];
    [self presentViewController:alertController animated:YES completion:nil];
    
    /*
    
    UIAlertView *alertDelete = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"DeleteGroupTitle", nil)
                                                          message:NSLocalizedString(@"DeleteGroupMSG", nil)
                                                         delegate:self
                                                cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                                                otherButtonTitles:NSLocalizedString(@"Confirm", nil), nil];
    alertDelete.tag = 10;
    [alertDelete show];
    */
    // GA
  //  [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:@"editing" action:@"delete_start" label:@"group_menu" value:nil] build]];
}

- (void)showAlertRename
{
    alertRename1 = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"RenameGroupTitle", nil) message:@"" preferredStyle:UIAlertControllerStyleAlert];
    
    __weak __typeof(self)weakSelf = self;
    SoundGroup *deS = (SoundGroup*)groups[self.editIndex.row];
       [alertRename1 addTextFieldWithConfigurationHandler:^(UITextField *textField) {
           // optionally configure the text field
           textField.keyboardType = UIKeyboardTypeDefault;
           textField.text=deS.name;
           __strong __typeof(weakSelf)strongSelf = weakSelf;
           textField.delegate = strongSelf;
           textField.tag=11;
       }];

    
    
          UIAlertAction *cancel =[UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:nil];

          UIAlertAction *sure =[UIAlertAction actionWithTitle:NSLocalizedString(@"Confirm", nil) style:UIAlertActionStyleDefault handler:nil];
          [alertRename1 addAction:sure];
          [alertRename1 addAction:cancel];
          [self presentViewController:alertRename1 animated:YES completion:nil];

    /*
    alertRename =[[UIAlertView alloc ] initWithTitle:NSLocalizedString(@"RenameGroupTitle", nil)
                                             message:@""
                                            delegate:self
                                   cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                                   otherButtonTitles:NSLocalizedString(@"Confirm", nil), nil];
    alertRename.alertViewStyle = UIAlertViewStylePlainTextInput;
    alertRename.tag = 11;
    
    SoundGroup *deS = (SoundGroup*)groups[self.editIndex.row];
    UITextField *inputField = (UITextField*)[alertRename textFieldAtIndex:0];
    inputField.delegate = self;
    inputField.tag = 11;
    inputField.returnKeyType = UIReturnKeyDone;
    inputField.text = deS.name;
    */
 //   [alertRename show];
    
    // GA
  //  [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:@"editing" action:@"rename_start" label:@"group_menu" value:nil] build]];
}

- (void)showAlertAdd
{
    alertAdd = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"NewGroupTitle", nil) message:@"" preferredStyle:UIAlertControllerStyleAlert];
       
       __weak __typeof(self)weakSelf = self;
          [alertAdd addTextFieldWithConfigurationHandler:^(UITextField *textField) {
              // optionally configure the text field
              textField.keyboardType = UIKeyboardTypeDefault;
              __strong __typeof(weakSelf)strongSelf = weakSelf;
              textField.delegate = strongSelf;
              textField.tag=12;
          }];

       
       
             UIAlertAction *cancel =[UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){
          //       [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:@"editing" action:@"add_cancel" label:@"group_menu" value:nil] build]];
                 
             }];

             UIAlertAction *sure =[UIAlertAction actionWithTitle:NSLocalizedString(@"Confirm", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){ [self addGroup];
                 self->isAddingGroup = FALSE;}];
             [alertRename1 addAction:sure];
             [alertRename1 addAction:cancel];
             [self presentViewController:alertRename1 animated:YES completion:nil];
    
    /*
    alertAdd =[[UIAlertView alloc ] initWithTitle:NSLocalizedString(@"NewGroupTitle", nil)
                                          message:@""
                                         delegate:self
                                cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                                otherButtonTitles:NSLocalizedString(@"Confirm", nil), nil];
    alertAdd.alertViewStyle = UIAlertViewStylePlainTextInput;
    alertAdd.tag = 12;
    
    UITextField *inputField = (UITextField*)[alertAdd textFieldAtIndex:0];
    inputField.delegate = self;
    inputField.tag = 12;
    inputField.returnKeyType = UIReturnKeyDone;
    */
  //  [alertAdd show];
}
/*
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 10) {
        if (buttonIndex == 1) {
            [self deleteGroup];
        } else {
            // GA
            [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:@"editing" action:@"delete_cancel" label:@"group_menu" value:nil] build]];
        }
    }
    else if (alertView.tag == 11) {
        if (buttonIndex == 1) {
            [self renameGroup];
        } else {
            // GA
            [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:@"editing" action:@"rename_cancel" label:@"group_menu" value:nil] build]];
        }
    }
    else if (alertView.tag == 12) {
        if (buttonIndex == 1) {
            [self addGroup];
            isAddingGroup = FALSE;
        } else {
            // GA
            [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:@"editing" action:@"add_cancel" label:@"group_menu" value:nil] build]];
        }
    }
    
}

- (BOOL)alertViewShouldEnableFirstOtherButton:(UIAlertView *)alertView
{
    if (alertView.tag == 11 || alertView.tag == 12) {
        UITextField *temp = (UITextField*)[alertView textFieldAtIndex:0];
        return [self validateName:alertView name:temp.text];
    }
    return YES;
}
 */

#pragma mark - TextField

- (BOOL)validateName:(UIAlertController*)alert name:(NSString*)name{

    [alert setMessage:@""];
    if (name.length == 0)
        return NO;
    
    // 沒有變
  //  if (alert.tag == 11) {
     if([alert isEqual:alertRename1]){
        SoundGroup *deS = (SoundGroup*)groups[self.editIndex.row];
        if ([name isEqualToString:deS.name]) {
            newGroupName = name;
            return YES;
        }
    }
    
    // 檢查檔案名稱是否重複
    NSString *qry = [NSString stringWithFormat:@"SELECT g_id FROM GROUP_TABLE WHERE g_name = '%@'", name];
    FMResultSet *rs = [db executeQuery:qry];
    while ([rs next])
    {
        [alert setMessage:NSLocalizedString(@"Category name already exist", nil)];
        UIAccessibilityPostNotification(UIAccessibilityAnnouncementNotification, NSLocalizedString(@"Category name already exist",nil));
        //UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message: NSLocalizedString(@"Category name already exist", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"Confirm", nil) otherButtonTitles:nil, nil];
        //[alert show];
        return NO;
    }
    
    newGroupName = name;
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    // 過濾非法的字元
    NSCharacterSet *cs = [NSCharacterSet characterSetWithCharactersInString:RESTRICT_CHARACTERS];
    NSString *filtered = [string stringByTrimmingCharactersInSet:cs];
    filtered = [filtered stringByTrimmingCharactersInSet:[NSCharacterSet newlineCharacterSet]];
    
    // 限制輸入字元長度(拼注音當中最後會被擋)
    NSUInteger oldLength = [textField.text length];
    NSUInteger replacementLength = [string length];
    NSUInteger rangeLength = range.length;
    
    NSUInteger newLength = oldLength - rangeLength + replacementLength;
    
    BOOL returnKey = [string rangeOfString: @"\n"].location != NSNotFound;
    
    return [string isEqualToString:filtered] && (newLength <= MAX_TEXT_LENGTH || returnKey);
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField.tag == 11)
        return [self validateName:alertRename1 name:textField.text];
    else
        return [self validateName:alertAdd name:textField.text];
}

#pragma mark - Accessibility

- (BOOL)accessibilityPerformMagicTap {
    
    // GA
  //  [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:@"editing" action:@"magic_tap" label:@"group_menu" value:nil] build]];
    
    return YES;
}

- (BOOL)accessibilityPerformEscape
{
    // GA
  //  [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:@"editing" action:@"escape" label:@"group_menu" value:nil] build]];
    
    [self dismissViewControllerAnimated:YES completion:nil];
    return true;
}

@end
