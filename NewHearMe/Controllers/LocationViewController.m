//
//  LocationViewController.m
//  NewHearMe
//
//  Created by IUILab Air on 2016/3/5.
//  Copyright © 2016年 IUILAB. All rights reserved.
//

#import "LocationViewController.h"
#import "FileViewController.h"
#import "FMDatabase.h"
#import "FMDatabaseAdditions.h"
#import "ShareSettings.h"
#import "SoundFile.h"
#import "UIUtility.h"

@interface LocationViewController ()
{
    FMDatabase *db;
    int refreshFlag;
    NSString *newLandmarkName;
}

@property (strong, nonatomic) ShareSettings *shareSettings;

@end

@implementation LocationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.shareSettings = [ShareSettings sharedSettings];
    self.trackTag = @"location";
    self.navigationView.lblViewTitle.text = NSLocalizedString(@"Location", nil);
    
    // 搜尋資料庫
    NSArray *pathsToDocuments = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [pathsToDocuments objectAtIndex:0];
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"HearMe.db"];
    db = [FMDatabase databaseWithPath:filePath];
    if (![db open]) {
        NSLog(@"Could not open db.");
    }
    
    refreshFlag = 0;
    [self refreshData];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if (refreshFlag)
        [self refreshData];
    refreshFlag ++;
}

- (void)viewDidAppear:(BOOL)animated {
    /*
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:SCREEN_NAME_LOCATION];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    */
    [super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Local functions

- (void)refreshData
{
    self.hasNearest = FALSE;
    NSString *query = @"";
    NSString *title = @"";
    NSString *lkey = @"";
    
    self.items = [[NSMutableArray alloc]init];
    self.itemFiles = [[NSMutableArray alloc]init];
    self.itemID = [[NSMutableArray alloc]init];
    
    FMResultSet *rsl = [db executeQuery:@"SELECT DISTINCT l_landmark FROM VOICE_LIST, LOCATION_TABLE WHERE v_lid = l_id AND v_public = 1 AND l_landmark IS NOT NULL AND l_landmark <> '' ORDER BY l_postalcode"];
    while ([rsl next])
    {
        title = NSLocalizedString([rsl stringForColumnIndex:0],nil);
        lkey = [rsl stringForColumnIndex:0];
        
        // 加入前三個檔案
        query = [NSString stringWithFormat:@"SELECT * FROM VOICE_LIST, LOCATION_TABLE WHERE v_lid = l_id AND v_public = 1 AND l_landmark = '%@' ORDER BY v_id DESC LIMIT 3", [rsl stringForColumnIndex:0]];
        FMResultSet *rsFile = [db executeQuery:query];
        
        NSMutableArray *s = [[NSMutableArray alloc]init];
        while ([rsFile next])
        {
            SoundFile *sound = [SoundFile fileOfGroup:[rsFile intForColumn:@"v_gid"]
                                                  vid:[rsFile intForColumn:@"v_id"]
                                                 name:[rsFile stringForColumn:@"v_name"]
                                               author:[rsFile stringForColumn:@"v_author"]
                                             latitude:[rsFile stringForColumn:@"v_lat"]
                                            longitude:[rsFile stringForColumn:@"v_lon"]
                                                  lid:[rsFile intForColumn:@"v_lid"]
                                             duration:[rsFile intForColumn:@"v_duration"]
                                            highlight:[rsFile intForColumn:@"v_highlight"]
                                                 date:[rsFile stringForColumn:@"v_date"]
                                                 time:[rsFile stringForColumn:@"v_time"]
                                             isPublic:[rsFile intForColumn:@"v_public"]];
            [s addObject:sound];
        }
        
        if ([[rsl stringForColumnIndex:0] isEqualToString:self.shareSettings.nearest]) {
            // 放到第一個項目 加入副標題 Nearest
            self.hasNearest = TRUE;
            [self.items insertObject:title atIndex:0];
            [self.itemFiles insertObject:s atIndex:0];
            [self.itemID insertObject:lkey atIndex:0];
        } else {
            [self.items addObject:title];
            [self.itemFiles addObject:s];
            [self.itemID addObject:lkey];
        }
    }
    
    // 沒有landmark但有地址的資料另外一筆一筆列
    FMResultSet *rsl2 = [db executeQuery:@"SELECT l_id, l_city, l_street, l_landmark FROM VOICE_LIST, LOCATION_TABLE WHERE v_lid = l_id AND v_public = 1 AND (l_landmark IS NULL OR l_landmark = '') ORDER BY l_postalcode"];
    while ([rsl2 next])
    {
        title = [NSString stringWithFormat:@"%@, %@", [rsl2 stringForColumn:@"l_city"], [rsl2 stringForColumn:@"l_street"]];
        lkey = [rsl2 stringForColumn:@"l_id"];
        
        // 加入前三個檔案
        query = [NSString stringWithFormat:@"SELECT * FROM VOICE_LIST, LOCATION_TABLE WHERE v_lid = l_id AND l_id = %@ AND v_public = 1 ORDER BY v_id DESC LIMIT 3", [rsl2 stringForColumn:@"l_id"]];
        FMResultSet *rsFile2 = [db executeQuery:query];
        
        NSMutableArray *s = [[NSMutableArray alloc]init];
        while ([rsFile2 next])
        {
            SoundFile *sound = [SoundFile fileOfGroup:[rsFile2 intForColumn:@"v_gid"]
                                                  vid:[rsFile2 intForColumn:@"v_id"]
                                                 name:[rsFile2 stringForColumn:@"v_name"]
                                               author:[rsFile2 stringForColumn:@"v_author"]
                                             latitude:[rsFile2 stringForColumn:@"v_lat"]
                                            longitude:[rsFile2 stringForColumn:@"v_lon"]
                                                  lid:[rsFile2 intForColumn:@"v_lid"]
                                             duration:[rsFile2 intForColumn:@"v_duration"]
                                            highlight:[rsFile2 intForColumn:@"v_highlight"]
                                                 date:[rsFile2 stringForColumn:@"v_date"]
                                                 time:[rsFile2 stringForColumn:@"v_time"]
                                             isPublic:[rsFile2 intForColumn:@"v_public"]];
            [s addObject:sound];
        }
        
        if ([rsl2 intForColumn:@"l_id"] == self.lid) {
            // 放到第一個項目 加入副標題 Nearest
            self.hasNearest = TRUE;
            [self.items insertObject:title atIndex:0];
            [self.itemFiles insertObject:s atIndex:0];
            [self.itemID insertObject:lkey atIndex:0];
        } else {
            [self.items addObject:title];
            [self.itemFiles addObject:s];
            [self.itemID addObject:lkey];
        }
    }
    
    [self.tableView reloadData];
}

#pragma mark - TableView Delegate

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        /*
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker set:kGAIScreenName value:SCREEN_NAME_LOCATION_EDIT];
        [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
        */
        // 記錄選擇的項目
        self.editIndex = indexPath;
        [self.tableView setEditing:NO];
        [self showAlertRename];
    }
}



- (void)showAlertRename
{
   // [self cancelPlay];
    UIAlertController *alertRename = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"RenameLoctationTitle", nil) message:NSLocalizedString(@"RenameLoctationMSG", nil) preferredStyle:UIAlertControllerStyleAlert];
    
    [alertRename addTextFieldWithConfigurationHandler:^(UITextField *textField) {
          textField.placeholder = @"name";
          textField.clearButtonMode = UITextFieldViewModeWhileEditing;
          textField.borderStyle = UITextBorderStyleRoundedRect;
     //    [textField addTarget:self action:@selector(textDidChange:) forControlEvents:UIControlEventEditingChanged];
      }];
          
    UIAlertAction *cancel =[UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
      //   [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:@"editing" action:@"rename_cancel" label:@"location_menu" value:nil] build]];
        [alertRename dismissViewControllerAnimated:YES completion:nil];
    }];

          UIAlertAction *sure =[UIAlertAction actionWithTitle:NSLocalizedString(@"Settings", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
       //          [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:@"editing" action:@"rename_start" label:@"location_menu" value:nil] build]];
          NSArray * textfields = alertRename.textFields;
          UITextField * namefield = textfields[0];
              self->newLandmarkName=namefield.text;
              if (![self->newLandmarkName isEqualToString:@""]) {
                           // 更新資料庫l_landmark
                NSString *qry = [NSString stringWithFormat:@"UPDATE LOCATION_TABLE SET l_landmark = '%@' WHERE l_landmark = '%@'", self->newLandmarkName, self.items[self.editIndex.row]];
                if (![self->db executeUpdate:qry]){
                               NSLog(@"ERROR UPDATE location: %@", [self->db lastErrorMessage]);
                               return;
                           }
                           
                           // 執行成功後，語音回饋
                           UIAccessibilityPostNotification(UIAccessibilityAnnouncementNotification, NSLocalizedString(@"Rename complete", nil));
                           [self performSelector:@selector(refreshData) withObject:nil afterDelay:1.0];
                       }
                       // GA
        //      [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:@"editing" action:@"rename_location" label:self->newLandmarkName value:nil] build]];
              
          }];
    
    [sure setEnabled:false];
          [alertRename addAction:sure];
          [alertRename addAction:cancel];
    
    
    /*
    UIAlertView  *alertRename =[[UIAlertView alloc ] initWithTitle:NSLocalizedString(@"RenameLoctationTitle", nil)
                                                           message:NSLocalizedString(@"RenameLoctationMSG", nil)
                                                          delegate:self
                                                 cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                                                 otherButtonTitles:NSLocalizedString(@"Confirm", nil), nil];
    alertRename.alertViewStyle = UIAlertViewStylePlainTextInput;
    alertRename.tag = 11;
    
    UITextField *inputField = (UITextField*)[alertRename textFieldAtIndex:0];
    inputField.delegate = self;
    inputField.returnKeyType = UIReturnKeyDone;
    inputField.text = self.items[self.editIndex.row];
    */
    [self presentViewController:alertRename animated:YES completion:nil];
    
    // GA Screen
    /*
 id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker set:kGAIScreenName value:SCREEN_NAME_LOCATION];
        [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
     */
}

- (void)textDidChange:(UITextField *)textField {
    UIAlertController *alertController = (UIAlertController *)self.presentedViewController;
    if (alertController) {
        UITextField *field = alertController.textFields.firstObject;

        UIAlertAction *okAction = alertController.actions.firstObject;
        if(field.text.length > 0)okAction.enabled=true;
        else okAction.enabled=false;
    }
}

/*
#pragma mark - AlertView ActionSheet delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    //move to UIAlertController action tag:11
    if (alertView.tag == 11) {
        if (buttonIndex == 1) {
            newLandmarkName = [[alertView textFieldAtIndex:0] text];
            if (![newLandmarkName isEqualToString:@""]) {
                // 更新資料庫l_landmark
                NSString *qry = [NSString stringWithFormat:@"UPDATE LOCATION_TABLE SET l_landmark = '%@' WHERE l_landmark = '%@'", newLandmarkName, self.items[self.editIndex.row]];
                if (![db executeUpdate:qry]){
                    NSLog(@"ERROR UPDATE location: %@", [db lastErrorMessage]);
                    return;
                }
                
                // 執行成功後，語音回饋
                UIAccessibilityPostNotification(UIAccessibilityAnnouncementNotification, NSLocalizedString(@"Rename complete", nil));
                [self performSelector:@selector(refreshData) withObject:nil afterDelay:1.0];
            }
            // GA
            [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:@"editing" action:@"rename_location" label:newLandmarkName value:nil] build]];
        } else {
            // GA
            [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:@"editing" action:@"rename_cancel" label:@"location_menu" value:nil] build]];
        }
        // GA Screen
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker set:kGAIScreenName value:SCREEN_NAME_LOCATION];
        [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    }
}

- (BOOL)alertViewShouldEnableFirstOtherButton:(UIAlertView *)alertView
{
    if (alertView.tag == 11) {
        UITextField *temp = (UITextField*)[alertView textFieldAtIndex:0];
        if (temp.text.length == 0) {
            return NO;
        }
    }
    return YES;
}
*/

#pragma mark - TextField

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    //newLandmarkName = textField.text;
    //NSLog(@"newLandmarkName %@",newLandmarkName);
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
     UIAlertController *alertController = (UIAlertController *)self.presentedViewController;
    if (alertController) {
        UITextField *field = alertController.textFields.firstObject;

        UIAlertAction *okAction = alertController.actions.firstObject;
        if(field.text.length > 0)okAction.enabled=true;
        else okAction.enabled=false;
    }
    
    // 過濾非法的字元
    NSCharacterSet *cs = [NSCharacterSet characterSetWithCharactersInString:RESTRICT_CHARACTERS];
    NSString *filtered = [string stringByTrimmingCharactersInSet:cs];
    filtered = [filtered stringByTrimmingCharactersInSet:[NSCharacterSet newlineCharacterSet]];
    
    // 限制輸入字元長度(拼注音當中最後會被擋)
    NSUInteger oldLength = [textField.text length];
    NSUInteger replacementLength = [string length];
    NSUInteger rangeLength = range.length;
    
    NSUInteger newLength = oldLength - rangeLength + replacementLength;
    
    BOOL returnKey = [string rangeOfString: @"\n"].location != NSNotFound;
    
    return [string isEqualToString:filtered] && (newLength <= MAX_TEXT_LENGTH || returnKey);
}

@end
