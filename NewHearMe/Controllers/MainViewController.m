//
//  MainViewController.m
//  NewHearMe
//
//  Created by IUILab Air on 2016/3/5.
//  Copyright © 2016年 IUILAB. All rights reserved.
//

#import "MainViewController.h"
#import "RecordViewController.h"
#import "LocationViewController.h"
#import "PortalButton.h"
#import "FMDatabase.h"
#import "FMDatabaseAdditions.h"
#import "ShareSettings.h"
#import "UIUtility.h"
#import "BottomToolBar.h"
#import "PushUpSegue.h"

#define SCREEN_NAME @"首頁"
#define USER_NAME @"TESTER"

@interface MainViewController ()

@property (weak, nonatomic) IBOutlet UIView *recordPanel;
@property (weak, nonatomic) IBOutlet UIView *listPanel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIView *portalPanel;
@property (weak, nonatomic) IBOutlet UILabel *hintLabel;
@property (weak, nonatomic) IBOutlet PortalButton *btnPortalDate;
@property (weak, nonatomic) IBOutlet PortalButton *btnPortalCategory;
@property (weak, nonatomic) IBOutlet PortalButton *btnPortalLocation;
@property (weak, nonatomic) IBOutlet UIButton *btnSearch;
@property (weak, nonatomic) IBOutlet BottomToolBar *bottomToolBar;

@property (strong, nonatomic) ShareSettings *shareSettings;
@property (strong, nonatomic) CLLocationManager *locationManager;

- (IBAction)recordPanelClicked:(id)sender;

@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.shareSettings = [ShareSettings sharedSettings];
    self.shareSettings.userLatitude = 0.0;
    self.shareSettings.userLongitude = 0.0;
    self.shareSettings.lid = 0;
    
    self.titleLabel.frame = [UIUtility newPosition:50 source:self.titleLabel.frame];
    self.recordPanel.frame = [UIUtility newHeight:self.view.bounds.size.height * 259 / 568 source:self.recordPanel.frame];
    self.listPanel.frame = [UIUtility newPosition:self.view.bounds.size.height * 259 / 568 source:self.listPanel.frame];
    
    self.recordPanel.accessibilityHint = [NSString stringWithFormat:@"%@, %@", NSLocalizedString(@"HearMe Lite", nil), NSLocalizedString(@"Double fingers double tap to record", nil)];
    
    // Set Buttons
    CGFloat buttonHeight = (self.view.bounds.size.height - self.recordPanel.frame.size.height - 44 - 40 - 2) / 3;
    [self.btnPortalDate setLabelsWithWidth:self.view.bounds.size.width height:buttonHeight];
    self.btnPortalDate.mainLabel.text = NSLocalizedString(@"Date", @"Title");
    [self.btnPortalCategory setLabelsWithWidth:self.view.bounds.size.width height:buttonHeight];
    self.btnPortalCategory.mainLabel.text = NSLocalizedString(@"Category", @"Title");
    [self.btnPortalLocation setLabelsWithWidth:self.view.bounds.size.width height:buttonHeight];
    self.btnPortalLocation.mainLabel.text = NSLocalizedString(@"Location", @"Title");
    
    UIImageView *sIcon = [[UIImageView alloc] initWithFrame:CGRectMake(self.view.bounds.size.width - 34, (self.btnSearch.frame.size.height - 15) / 2, 18, 15)];
    [sIcon setImage:[UIImage imageNamed:@"search_icon.png"]];
    [self.btnSearch addSubview:sIcon];
    [self.btnSearch setAccessibilityLabel:NSLocalizedString(@"Keyword search", @"Button Description")];
    
    [self.bottomToolBar.btnHome setTitle:NSLocalizedString(@"", nil)];
    [self.bottomToolBar.btnHome setIsAccessibilityElement:NO];
    [self.bottomToolBar.btnHelp setAction:@selector(helpAction)];
    
    [self voiceOverStatusChanged];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(voiceOverStatusChanged) name:UIAccessibilityVoiceOverStatusDidChangeNotification object:nil];
    
    // 初次使用App，通知資料搜集政策
    BOOL isFirstRun = [[NSUserDefaults standardUserDefaults] boolForKey:@"AppOpenFirstTime"];
    if (!isFirstRun) {
 
      
         
           
      [self showHelp];
      
      UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Notice", @"Alert Title") message:NSLocalizedString(@"AppLicence", @"Alert Message") preferredStyle:UIAlertControllerStyleAlert];
            

      UIAlertAction *sure =[UIAlertAction actionWithTitle:NSLocalizedString(@"Settings", @"Button Title") style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){[self showHelp];}];
            [alertController addAction:sure];
          [self presentViewController:alertController animated:YES completion:nil];
            
      
      [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"AppOpenFirstTime"];
      [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    // 取得地點
    self.locationManager = [[CLLocationManager alloc] init];
    // This is the most important property to set for the manager. It ultimately determines how the manager will
    // attempt to acquire location and thus, the amount of power that will be consumed.
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    // Once configured, the location manager must be "started".
    self.locationManager.delegate = self;
    
    // 詢問地點授權
    [self.locationManager requestWhenInUseAuthorization];
    
    [self refreshData];
    [self updateLocation];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    // 進入頁面更新地點
    [self.locationManager requestLocation];
   /*
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIAppVersion value:[[NSBundle mainBundle] objectForInfoDictionaryKey:(NSString *)kCFBundleVersionKey]];
    [tracker set:kGAIUserId value:USER_NAME];
    [tracker set:kGAIScreenName value:SCREEN_NAME];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    */
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIAccessibilityVoiceOverStatusDidChangeNotification object:nil];
}

#pragma mark - Content

- (void)refreshData
{
    // 搜尋資料庫
    NSArray *pathsToDocuments = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [pathsToDocuments objectAtIndex:0];
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"HearMe.db"];
    FMDatabase *db = [FMDatabase databaseWithPath:filePath];
    if (![db open]) {
        NSLog(@"Could not open db.");
    }
    
    NSDateFormatter *dateFormatter = [UIUtility dateFormatterYYMMdd];
    NSDateFormatter *timeDateFormatter = [UIUtility dateFormatterhhmm_a];
    NSString *dateText = NSLocalizedString(@"No Records", nil);
    NSString *cateText = NSLocalizedString(@"No Records", nil);
    NSString *dateVoice = @"";
    NSString *cateVoice = @"";
    
    NSString *query = [NSString stringWithFormat:@"SELECT v_gid, v_lid, v_date, v_time FROM VOICE_LIST ORDER BY v_id DESC LIMIT 1"];
    FMResultSet *rs = [db executeQuery:query];
    while ([rs next])
    {
        // 取得檔案最新的日期
        NSDate *nDate = [[UIUtility dateFormatterYYMMddHHmmss] dateFromString:[NSString stringWithFormat:@"%@%@", [rs stringForColumn:@"v_date"], [rs stringForColumn:@"v_time"]]];
        NSDate *yDate = [[NSDate date] dateByAddingTimeInterval:-(24*60*60)]; // Yesterday
        NSString *dateAlias = [[UIUtility dateFormatterMMMM_dd] stringFromDate:nDate];
        NSString *dateCode = [[rs stringForColumn:@"v_date"] substringToIndex:6];
        if ([[dateFormatter stringFromDate:[NSDate date]] isEqualToString:dateCode]) {
            dateAlias = NSLocalizedString(@"Today", nil);
        } else if ([[dateFormatter stringFromDate:yDate] isEqualToString:dateCode]) {
            dateAlias = NSLocalizedString(@"Yesterday", nil);
        }
        dateText = [NSString stringWithFormat:@"%@, %@", dateAlias, [timeDateFormatter stringFromDate:nDate]];
        dateVoice = [NSString stringWithFormat:@"%@, %@, %@", NSLocalizedString(@"Latest record", nil), dateAlias, [timeDateFormatter stringFromDate:nDate]];
        
        // 取得最新儲存的群組名稱
        query = [NSString stringWithFormat:@"SELECT g_name FROM GROUP_TABLE WHERE g_id = %@", [rs stringForColumn:@"v_gid"]];
        FMResultSet *rsGroup = [db executeQuery:query];
        while ([rsGroup next])
        {
            cateText = [NSString stringWithFormat:@"%@", NSLocalizedString([rsGroup stringForColumn:@"g_name"], nil)];
            cateVoice = [NSString stringWithFormat:@"%@, %@", NSLocalizedString(@"Latest saved", nil), NSLocalizedString([rsGroup stringForColumn:@"g_name"], nil)];
        }
        
    }
    
    self.btnPortalDate.sideLabel.text = dateText;
    self.btnPortalDate.voiceText = dateVoice;
    self.btnPortalCategory.sideLabel.text = cateText;
    self.btnPortalCategory.voiceText = cateVoice;
}

- (void)showHelp
{
      UIAlertController *alertHelp = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Instructions", @"Alert Title") message:NSLocalizedString(@"HelpMain", @"Alert Message") preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *sure =[UIAlertAction actionWithTitle:NSLocalizedString(@"Close", @"Button Title") style:UIAlertActionStyleDefault handler:nil];
    [alertHelp addAction:sure];
    [self presentViewController:alertHelp animated:YES completion:nil];

   /*
    UIAlertView *alertHelp = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Instructions", @"Alert Title")
                                                        message:NSLocalizedString(@"HelpMain", @"Alert Message")
                                                       delegate:self
                                              cancelButtonTitle:nil
                                              otherButtonTitles:NSLocalizedString(@"Close", @"Button Title"), nil];
    [alertHelp setTag:12];
    [alertHelp show];
    */
}

#pragma mark - Location

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(nonnull NSArray<CLLocation *> *)locations
{
    CLLocation *currentLocation = [locations lastObject];
    CLGeocoder *ceo = [[CLGeocoder alloc]init];
    CLLocation *loc = [[CLLocation alloc]initWithLatitude:currentLocation.coordinate.latitude longitude:currentLocation.coordinate.longitude];
    
    // 設定目前位置
    self.shareSettings.userLatitude = currentLocation.coordinate.latitude;
    self.shareSettings.userLongitude = currentLocation.coordinate.longitude;
    //NSLog(@"%.2f, %.2f",self.shareSettings.userLatitude,self.shareSettings.userLongitude);
    
    [ceo reverseGeocodeLocation: loc completionHandler:^(NSArray *placemarks, NSError *error) {
        if (error == nil && [placemarks count] > 0) {
            CLPlacemark *placemark = [placemarks objectAtIndex:0];
            /*
            NSLog(@"placemark %@",placemark);
            //String to hold address
            NSString *locatedAt = [[placemark.addressDictionary valueForKey:@"FormattedAddressLines"] componentsJoinedByString:@", "];
            NSLog(@"addressDictionary %@", placemark.addressDictionary);
            
            NSLog(@"country %@",placemark.country);  // Give Country Name
            NSLog(@"subAdministrativeArea %@",placemark.subAdministrativeArea); // Extract the city name
            NSLog(@"street %@",placemark.thoroughfare);
            NSLog(@"location name %@",placemark.name);
            
            NSLog(@"location %@",placemark.location);
            NSLog(@"I am currently at %@",locatedAt);
            */
            
            self.shareSettings.postalCode = placemark.postalCode;
            self.shareSettings.country = placemark.country;
            self.shareSettings.city = placemark.subAdministrativeArea;
            self.shareSettings.district = placemark.locality;
            self.shareSettings.street = placemark.thoroughfare;
            self.shareSettings.landmark = placemark.name;
            
            [self updateLocation];
        }
    }];
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"CLLocationManager didFailWithError: %@", error.description);
}

- (void)requestAlwaysAuthorization
{
    CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
    
    // If the status is denied or only granted for when in use, display an alert
    if (status == kCLAuthorizationStatusAuthorizedWhenInUse || status == kCLAuthorizationStatusDenied) {
        NSString *title;
        title = (status == kCLAuthorizationStatusDenied) ? NSLocalizedString(@"Location services are off", @"Alert Title") : NSLocalizedString(@"Background location is not enabled", @"Alert Title");
        NSString *message = NSLocalizedString(@"To use background location,\ryou must turn on 'Always'\rin the Location Services Settings", @"Alert Message");
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *cancel =[UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", @"Button Title") style:UIAlertActionStyleCancel handler:nil];

        UIAlertAction *sure =[UIAlertAction actionWithTitle:NSLocalizedString(@"Settings", @"Button Title") style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
        {
            NSURL *settingsURL = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                    [[UIApplication sharedApplication] openURL:settingsURL options:@{} completionHandler:nil];
        }];
        [alertController addAction:sure];
        [alertController addAction:cancel];
        [self presentViewController:alertController animated:YES completion:nil];
        
        /*
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title
                                                            message:message
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"Cancel", @"Button Title")
                                                  otherButtonTitles:NSLocalizedString(@"Settings", @"Button Title"), nil];
        [alertView setTag:11];
        [alertView show];
         */
    }
    // The user has not enabled any location services. Request background authorization.
    else if (status == kCLAuthorizationStatusNotDetermined) {
        [self.locationManager requestAlwaysAuthorization];
    }
}

- (void)updateLocation
{
    int count = 0;
    int distance = 0;
    NSString *loctVoice = @"";
    NSString *loctText = NSLocalizedString(@"No Records", nil);
    CLLocation *userLocation = [[CLLocation alloc] initWithLatitude:self.shareSettings.userLatitude longitude:self.shareSettings.userLongitude];
    
    NSArray *pathsToDocuments = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [pathsToDocuments objectAtIndex:0];
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"HearMe.db"];
    FMDatabase *db = [FMDatabase databaseWithPath:filePath];
    if (![db open]) {
        NSLog(@"Could not open db.");
    }
    
    // 檢查是否是新地點，若是則寫入LOCATION_TABLE
    if (self.shareSettings.city != nil) {
        // 如果已有同樣地點，取得地點id
        int count = 0;
        NSString *query = [NSString stringWithFormat:@"SELECT l_id FROM LOCATION_TABLE WHERE l_country = '%@' AND l_city = '%@' AND l_district = '%@' AND l_street = '%@'", self.shareSettings.country, self.shareSettings.city, self.shareSettings.district, self.shareSettings.street];
        FMResultSet *rs = [db executeQuery:query];
        while ([rs next])
        {
            count ++;
            self.shareSettings.lid = [rs intForColumn:@"l_id"];
        }
        
        // 如果新地點，寫入db後取得新id
        if (count == 0) {
            FMResultSet *rs2 = [db executeQuery:@"SELECT MAX (l_id)+1 FROM LOCATION_TABLE"];
            while ([rs2 next]) {
                self.shareSettings.lid = [rs2 intForColumnIndex:0];
            }
            
            NSString *insert = [NSString stringWithFormat:@"INSERT INTO LOCATION_TABLE (l_id, l_postalcode, l_country, l_city, l_district, l_street, l_landmark) VALUES (%d, %@, '%@', '%@', '%@', '%@', '%@');", self.shareSettings.lid, self.shareSettings.postalCode, self.shareSettings.country, self.shareSettings.city, self.shareSettings.district, self.shareSettings.street, self.shareSettings.landmark];
            
            // GA
         //   NSMutableDictionary *event = [[GAIDictionaryBuilder createEventWithCategory:@"tracking" action:@"location_discover" label:self.shareSettings.landmark value:nil] build];
            //[[GAI sharedInstance].defaultTracker send:event];
            
            if (![db executeUpdate:insert]){
                self.shareSettings.lid = 0;
                NSLog(@"Could not INSERT LOCATION_TABLE: %@ SQL: %@", [db lastErrorMessage], insert);
            }
        }
    }
    
    // 找到最近的地區名稱
    FMResultSet *rsl = [db executeQuery:@"SELECT AVG(v_lat), AVG(v_lon), l_landmark FROM VOICE_LIST, LOCATION_TABLE WHERE l_id = v_lid GROUP BY v_lid"];
    while ([rsl next])
    {
        CLLocation *fileLocation = [[CLLocation alloc] initWithLatitude:[rsl doubleForColumnIndex:0] longitude:[rsl doubleForColumnIndex:1]];
        //NSLog(@"Distance lid %d: %.4f", [rsl intForColumn:@"v_lid"], [userLocation distanceFromLocation:fileLocation]);
        if (count == 0 || distance > [userLocation distanceFromLocation:fileLocation]) {
            distance = [userLocation distanceFromLocation:fileLocation];
            self.shareSettings.nearest = [rsl stringForColumnIndex:2];
            
            // 讀取地點詳細資料
            loctText = [NSString stringWithFormat:@"%@", NSLocalizedString([rsl stringForColumn:@"l_landmark"],nil)];
            loctVoice = [NSString stringWithFormat:@"%@, %@", NSLocalizedString(@"Nearest location", nil), [rsl stringForColumn:@"l_landmark"]];
            
            count ++;
        }
    }
    //NSLog(@"%d",self.shareSettings.lid);
    self.btnPortalLocation.sideLabel.text = loctText;
    self.btnPortalLocation.voiceText = loctVoice;
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"showLocations"])
    {
        LocationViewController *loctView = [segue destinationViewController];
        
        loctView.lid = self.shareSettings.lid;
    }
}

- (IBAction)returnedFromSegue:(UIStoryboardSegue *)segue
{
    NSLog(@"Returned from record view");
}

/*
#pragma mark - Delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 10) {
        // 第一次使用預設提供Help
        [self showHelp];
    }
    else if (alertView.tag == 11) {
        if (buttonIndex == 1) {
            // Send the user to the Settings for this app
            NSURL *settingsURL = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
            [[UIApplication sharedApplication] openURL:settingsURL options:@{} completionHandler:nil];
        }
    }
}
*/
#pragma mark - Accessibility

- (BOOL)accessibilityPerformMagicTap {
    [self performSegueWithIdentifier:@"showRecord" sender:self];
    
    return true;
}

- (void)voiceOverStatusChanged {
    if (UIAccessibilityIsVoiceOverRunning()) {
        self.hintLabel.text = NSLocalizedString(@"Double fingers double tap to record", nil);
    } else {
        self.hintLabel.text = NSLocalizedString(@"Tap here to start record", nil);
    }
}

- (NSString*)isHeadsetPluggedIn {
    AVAudioSessionRouteDescription *route = [[AVAudioSession sharedInstance] currentRoute];
    for (AVAudioSessionPortDescription *desc in [route outputs]) {
        if ([[desc portType] isEqualToString:AVAudioSessionPortHeadphones])
            return @"Yes";
    }
    return @"No";
}

#pragma mark - Button Action

- (IBAction)recordPanelClicked:(id)sender {
    [self performSegueWithIdentifier:@"showRecord" sender:self];
}

#pragma mark - ToolBar Actions

- (void)homeAction
{
    
}

- (void)helpAction
{
    // GA
   // [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:@"tracking" action:@"get_help" label:@"main" value:nil] build]];
    
    [self showHelp];
}

@end
