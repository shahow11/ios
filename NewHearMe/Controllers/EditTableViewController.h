//
//  EditTableViewController.h
//  NewHearMe
//
//  Created by IUILab Air on 2016/3/6.
//  Copyright © 2016年 IUILAB. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import <GoogleAnalytics/GAI.h>
//#import <GoogleAnalytics/GAIDictionaryBuilder.h>
//#import <GoogleAnalytics/GAIFields.h>

@interface EditTableViewController : UITableViewController<UIAlertViewDelegate, UIActionSheetDelegate, UITextFieldDelegate>

@property (nonatomic, strong) NSIndexPath* editIndex;

@end
