//
//  PlayViewController.m
//  NewHearMe
//
//  Created by IUILab Air on 2016/3/5.
//  Copyright © 2016年 IUILAB. All rights reserved.
//

#import "PlayViewController.h"
#import "NavigationView.h"
#import "UnderscoreView.h"
#import "BottomToolBar.h"
#import "FMDatabase.h"
#import "FMDatabaseAdditions.h"
#import "SoundSection.h"
#import "HTTPServer.h"
#import "UIUtility.h"

#define TIME_FOR_SOUND  0.3
#define TIME_JUMPING    5.0

@interface PlayViewController ()
{
    BOOL isPaused;
    BOOL isAudoFocus;
    BOOL isInitPlay;
    BOOL isForward;
    BOOL pgrsAnimated;
    float playedTime;
    float timeCount;
    int pauseTime;
    
    NSTimer* timer;
    NSTimer* pauseTimer;
    NSTimeInterval playStartTime;
    NSMutableArray *sections;
    NSString *sz;
    NSString *timeText;
    NSString *lengthString;
    NSString *hostURL;
    NSURL *fileURL;
    NSURL *partURL;
    NSURL *pauseURL;
    
    AVAudioPlayer* audioPlayer;
    AVAudioPlayer* effectPlayer;
    PartTableViewCell* cellPlaying;
    HTTPServer *httpServer;
}

@property (weak, nonatomic) IBOutlet NavigationView *navigationView;
@property (weak, nonatomic) IBOutlet UnderscoreView *infoPanel;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet BottomToolBar *bottomToolBar;

@property (weak, nonatomic) IBOutlet UIButton *btnPlay;
@property (weak, nonatomic) IBOutlet UIButton *btnMask;
@property (weak, nonatomic) IBOutlet UIImageView *imgPlay;
@property (weak, nonatomic) IBOutlet UILabel *lblTime;
@property (weak, nonatomic) IBOutlet UILabel *lblLocation;
@property (weak, nonatomic) IBOutlet UILabel *lblInformation;

- (IBAction)btnPlayClicked:(id)sender;
- (IBAction)btnMaskClicked:(id)sender;

@end

@implementation PlayViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    isPaused = NO;
    isAudoFocus = NO;
    isInitPlay = YES;
    isForward = NO;
    pgrsAnimated = YES;
    playStartTime = 0;
    pauseTime = 0;

    self.navigationView.lblViewTitle.text = self.viewTitle;
    [self.navigationView setDarkBackgrounded];
    self.navigationView.btnBack.accessibilityLabel = NSLocalizedString(@"Back to file list", nil);
    [self.navigationView.btnBack addTarget:self action:@selector(popViewControllerAnimated) forControlEvents:UIControlEventTouchUpInside];
    self.navigationView.btnShare.hidden = NO;
    self.navigationView.btnShare.accessibilityLabel = NSLocalizedString(@"Share", nil);
    [self.navigationView.btnShare addTarget:self action:@selector(btnShareClicked) forControlEvents:UIControlEventTouchUpInside];
    
    [self.bottomToolBar.btnHome setAction:@selector(homeAction)];
    [self.bottomToolBar.btnHelp setAction:@selector(helpAction)];
    
    self.infoPanel.isInfoView = YES;
    self.infoPanel.fileName = self.soundFile.name;
    
    [self.btnMask setTitle:NSLocalizedString(@"PAUSED", nil) forState:UIControlStateNormal];
    self.btnMask.accessibilityViewIsModal = YES;
    self.btnMask.accessibilityLabel = NSLocalizedString(@"Paused, double tap to resume", nil);
    
    NSTimeInterval totalLength = self.soundFile.duration;
    NSInteger hours = floor(totalLength/3600);
    totalLength = totalLength - hours*3600;
    NSInteger minutes = floor(totalLength/60);
    NSInteger seconds = trunc(totalLength - minutes * 60);
    timeText = hours == 0? [NSString stringWithFormat:@"%02ld:%02ld", (long)minutes, (long)seconds]:[NSString stringWithFormat:@"%02ld:%02ld:%02ld", (long)hours, (long)minutes, (long)seconds];
    
    self.lblTime.text = timeText;
    
    NSString *hr = hours == 0? @"": [NSString stringWithFormat:@"%ld %@", (long)hours, NSLocalizedString(@"hours", nil)];
    NSString *mn = minutes == 0? @"": [NSString stringWithFormat:@"%ld %@", (long)minutes, NSLocalizedString(@"minutes", nil)];
    NSString *sc = seconds == 0? @"": [NSString stringWithFormat:@"%ld %@", (long)seconds, NSLocalizedString(@"seconds", nil)];
    
    fileURL = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/Documents/%@.mp4", NSHomeDirectory(), self.soundFile.name]];
    partURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"part_rl" ofType:@"mp3"]];
    pauseURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"pau_res_rl" ofType:@"mp3"]];
    
    // 讀取段落
    NSArray *pathsToDocuments = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [pathsToDocuments objectAtIndex:0];
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"HearMe.db"];
    
    FMDatabase *db = [FMDatabase databaseWithPath:filePath];
    if (![db open]) {
        NSLog(@"Could not open db.");
    }
    
    NSString *sqlsec = [NSString stringWithFormat:@"SELECT * FROM SECTION_TABLE WHERE s_vid=%d ORDER BY s_sid", self.soundFile.vid];
    FMResultSet *rss = [db executeQuery:sqlsec];
    
    sections = [[NSMutableArray alloc]init];
    
    while ([rss next])
    {
        if (sections.count == 0) {
            SoundSection *begin = [SoundSection sectionOfFile:0 vid:self.soundFile.vid name:NSLocalizedString(@"Beginning", nil) timestamp:0];
            [sections addObject:begin];
        }
        SoundSection *aSection = [SoundSection sectionOfFile:[rss intForColumn:@"s_sid"] vid:self.soundFile.vid name:[rss stringForColumn:@"s_name"] timestamp:[[rss stringForColumn:@"s_timestamp"] floatValue]];
        
        [sections addObject:aSection];
    }
    
    // 若沒有段落
    if (sections.count == 0) {
        SoundSection* entire = [SoundSection sectionOfFile:0 vid:self.soundFile.vid name:self.soundFile.name timestamp:0];
        [sections addObject:entire];
    }
    
    NSString *pt = sections.count == 0? NSLocalizedString(@"No Parts", nil) : [NSString stringWithFormat:@"%d %@", (int)sections.count - 1, NSLocalizedString(@"Parts", nil)];
    
    // 讀取地點
    NSString *lc = @"";
    NSString *sqlloc = [NSString stringWithFormat:@"SELECT * FROM LOCATION_TABLE WHERE l_id = %d", self.soundFile.lid];
    FMResultSet *rsl = [db executeQuery:sqlloc];
    while ([rsl next])
    {
        if ([rsl intForColumn:@"l_id"] == 0) {
            lc = NSLocalizedString(@"Not Located", nil);
        } else {
            lc = [NSString stringWithFormat:@"%@, %@", [rsl stringForColumn:@"l_city"], [rsl stringForColumn:@"l_street"]];
            // 換行改word wrapping
            //self.lblLocation.text = lc.length > 16? [NSString stringWithFormat:@"%@,\r%@", [rsl stringForColumn:@"l_city"], [rsl stringForColumn:@"l_street"]] : lc;
            self.lblLocation.text = lc;
            // 置頂，加空行
            /*
            NSDictionary *attributes = @{NSFontAttributeName: self.lblLocation.font};
            CGSize fontSize = [lc sizeWithAttributes:attributes];
            double finalHeight = fontSize.height * self.lblLocation.numberOfLines;
            double finalWidth = self.lblLocation.frame.size.width;    //expected width of label
            CGSize theStringSize = [lc sizeWithFont:self.lblLocation.font constrainedToSize:CGSizeMake(finalWidth, finalHeight) lineBreakMode:self.lblLocation.lineBreakMode];
            int newLinesToPad = (finalHeight  - theStringSize.height) / fontSize.height;
            for(int i = 0; i < newLinesToPad; i++)
                self.lblLocation.text = [self.lblLocation.text stringByAppendingString:@"\n "];*/
        }
    }
    
    // 檔案大小
    unsigned long long filesize = [[NSFileManager defaultManager] attributesOfItemAtPath:fileURL.path error:nil].fileSize;
    sz = [NSByteCountFormatter stringFromByteCount:filesize countStyle:NSByteCountFormatterCountStyleFile];
    
    self.lblInformation.text = [NSString stringWithFormat:@"%@, %@\r%@", pt, timeText, sz];
    self.infoPanel.accessibilityLabel = [NSString stringWithFormat:@"%@ %@; %@;%@%@%@;%@", NSLocalizedString(@"Recorded in", nil) , lc, pt, hr, mn, sc, sz];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.tableView reloadData];
    
    //    NSError *error;
    //    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:&error];
    //    if (error) {
    //        NSLog(@"Error setting AudioSession: %@", [error description]);
    //    }
    // 設定AudioPlayer
    audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:fileURL error:nil];
    [audioPlayer setDelegate:self];
    [audioPlayer prepareToPlay];
    
    // 無VoiceOver，直接播放
    if (!UIAccessibilityIsVoiceOverRunning()) {
        [self playFirstPart];
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:SCREEN_NAME_PLAY];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
    [super viewDidAppear:animated];
    [[UXTestingManager sharedInstance] enterViewWithName:SCREEN_NAME_PLAY andDetailName:@""];
    [[UXTestingManager sharedInstance] heatMapStartWithViewName:SCREEN_NAME_PLAY];
}

// 離開視窗，停止播放
- (void)viewWillDisappear:(BOOL)animated
{
    [audioPlayer stop];
    [timer invalidate];
    
    [super viewWillDisappear:animated];
    [[UXTestingManager sharedInstance] heatMapEnd];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TableView DataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return sections.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PartTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"partTableCell"];
    if ( cell == nil ) {
        cell = [[PartTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"partTableCell"];
    }
    
    NSUInteger rowNumber = [indexPath row];
    
    SoundSection *sect = (SoundSection *)sections[rowNumber];
    cell.row = (int)rowNumber;
    cell.startTime = sect.timestamp;
    if (rowNumber + 1 < sections.count)
    {
        SoundSection *nsect = (SoundSection *)sections[rowNumber + 1];
        cell.endTime = nsect.timestamp;
    } else {
        cell.endTime = self.soundFile.duration;
    }
    NSTimeInterval sectionLength = cell.endTime - cell.startTime;
    
    cell.delegate = self;
    //cell.accessibilityTraits = UIAccessibilityTraitAdjustable;
    cell.lblName.text = sect.name;
    cell.lblTime.text = [self durationText:sectionLength];
    cell.pgrsView.transform = CGAffineTransformMakeScale(1.0f, 5.0f);
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60.0;
}

#pragma mark - TableView Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (UIAccessibilityIsVoiceOverRunning()) {
        [self pauseOrContinue];
    } else {
        [self cancelPlay:cellPlaying reset:NO];
        PartTableViewCell *cell = (PartTableViewCell*)[self.tableView cellForRowAtIndexPath:indexPath];
        [self playPart:cell];
    }
}

#pragma mark - TableCell Delegate

- (void)playPart:(PartTableViewCell *)cell
{
    if (!isAudoFocus) {
        if (!isPaused) {
            // 不要念「可調整」！？？！
            UIAccessibilityPostNotification(UIAccessibilityLayoutChangedNotification, [self.tableView accessibilityElementAtIndex:cell.row]);
            // 改為播放顏色
            [cell.lblName setTextColor:[UIUtility BLUE]];
            [cell.lblTime setTextColor:[UIUtility BLUE]];
            
            [self.imgPlay setImage:[UIImage imageNamed:@"listen_pause.png"]];
            
            cellPlaying = cell;
            if (playStartTime == 0)
                playStartTime = cell.startTime;
            
            // 五的倍數則語音提示
            if (cell.row > 0 && cell.row % 5 == 0) {
                UIAccessibilityPostNotification(UIAccessibilityAnnouncementNotification, cell.lblName.text);
            }
            
            // 除Beginning外，播完音效再開始
            if (cell.row > 0) {
                [self playEffect:partURL];
                [self performSelector:@selector(playSection) withObject:nil afterDelay:TIME_FOR_SOUND];
            } else {
                [self playSection];
            }
            // GA
            if (isInitPlay) {
                [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:@"playing" action:@"play_start" label:self.soundFile.name value:nil] build]];
                [[UXTestingManager sharedInstance] customEvent:@"開始播放檔案"];
                isInitPlay = NO;
            }
        }
    }
    isAudoFocus = NO;
}

- (void)jumpForward
{
    if (audioPlayer.isPlaying) {
        isForward = YES;
        NSTimeInterval forwardTime = playedTime + TIME_JUMPING;
        if (forwardTime < audioPlayer.duration) {
            // 報讀跳到的時間
            UIAccessibilityPostNotification(UIAccessibilityAnnouncementNotification, [self durationVoice:forwardTime]);
            [self updateCurrentTime:forwardTime];
        } else {
            // 到檔案結尾
            [self setDefaultLayout];
            [audioPlayer prepareToPlay];
            
            UIAccessibilityPostNotification(UIAccessibilityAnnouncementNotification, NSLocalizedString(@"Finish playing", nil));
        }
        
        // GA
        [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:@"playing" action:@"play_forward" label:self.soundFile.name value:nil] build]];
        [[UXTestingManager sharedInstance] customEvent:@"快轉"];
    }
}

- (void)jumpBackward
{
    if (audioPlayer.isPlaying) {
        pgrsAnimated = NO;
        NSTimeInterval backwardTime = playedTime - TIME_JUMPING;
        if (backwardTime < 0)
            backwardTime = 0;
        // 報讀跳到的時間
        UIAccessibilityPostNotification(UIAccessibilityAnnouncementNotification, [self durationVoice:backwardTime]);
        [self updateCurrentTime:backwardTime];
        
        // GA
        [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:@"playing" action:@"play_backward" label:self.soundFile.name value:nil] build]];
        [[UXTestingManager sharedInstance] customEvent:@"倒轉"];
    }
}

- (void)cancelPlay:(PartTableViewCell*)cell reset:(BOOL)resetAll
{
    if (!isAudoFocus) {
        if (!isPaused) {
            if (audioPlayer.isPlaying) {
                [audioPlayer stop];
            }
            if (timer.valid) {
                [timer invalidate];
            }
            [NSObject cancelPreviousPerformRequestsWithTarget:self];
            
            // 回復正常顏色
            [cell.lblName setTextColor:[UIUtility BLUE_D]];
            [cell.lblTime setTextColor:[UIUtility BLUE_D]];
            [cell.pgrsView setProgress:0.0];
            
            if (resetAll) {
                // 連續播放段落時，時間文字要保持一致
                self.lblTime.text = timeText;
                [self.imgPlay setImage:[UIImage imageNamed:@"listen_play.png"]];
            }
        }
    }
}

#pragma mark - Playing

- (void)playSection
{
    if (audioPlayer.prepareToPlay) {
        if (audioPlayer.isPlaying) {
            [audioPlayer stop];
        }
        
        isPaused = NO;
        [audioPlayer setCurrentTime:playStartTime];
        [audioPlayer play];
        
        timeCount = playStartTime-cellPlaying.startTime;
        timer = [NSTimer scheduledTimerWithTimeInterval:0.01 target:self selector:@selector(updateTimer) userInfo:nil repeats:YES];
        [timer setFireDate:[NSDate date]];
        
        // 回復快轉倒轉開始值
        playStartTime = 0;
    }
}

- (void)updateTimer
{
    // 計算總播放時間
    playedTime = cellPlaying.startTime + timeCount;
    [self updateCurrentTime:playedTime];
    
    timeCount += 0.01;
}

- (void)updateCurrentTime:(NSTimeInterval)newTime
{
    self.lblTime.text = [self durationText:newTime];
    
    // 正常播放或快轉
    if (newTime >= playedTime) {
        // 更新進度條
        if (isForward) {
            timeCount += TIME_JUMPING;
        }
        
        float percentage = timeCount / (cellPlaying.endTime - cellPlaying.startTime);
        
        if (percentage < 1.0) {
            [cellPlaying.pgrsView setProgress:percentage animated:pgrsAnimated];
            pgrsAnimated = YES;
            if (isForward) {
                [audioPlayer setCurrentTime:newTime];
            }
        } else {
            [self cancelPlay:cellPlaying reset:NO];
            
            if (cellPlaying.row + 1 < sections.count) {
                // 檢查要跳到哪個Cell
                for(int i = 1; i < sections.count-cellPlaying.row; i++) {
                    NSIndexPath *nextIndex = [NSIndexPath indexPathForRow:(cellPlaying.row + i) inSection:0];
                    PartTableViewCell *cell = (PartTableViewCell *)[self.tableView cellForRowAtIndexPath:nextIndex];
                    
                    if (cell.startTime <= newTime && cell.endTime > newTime) {
                        playStartTime = newTime; //? 或要判斷是否快轉
                        [self playPart:cell];
                        
                        if (UIAccessibilityIsVoiceOverRunning()) {
                            // 會頓，要檢查是否自動轉移VO焦點(或提前一秒轉移??)
                            isAudoFocus = YES;
                            cell.isFocusDone = YES;
                            //UIAccessibilityPostNotification(UIAccessibilityLayoutChangedNotification, [self.tableView accessibilityElementAtIndex:[nextIndex row]]);
                        }
                        if (cell.row > 3) {
                            [self.tableView scrollToRowAtIndexPath:nextIndex atScrollPosition:UITableViewScrollPositionBottom animated:YES];
                        }
                        break;
                    }
                }
            } else {
                // 最後一個
                // 介面初始化
                [self setDefaultLayout];
                [audioPlayer prepareToPlay];
                UIAccessibilityPostNotification(UIAccessibilityAnnouncementNotification, NSLocalizedString(@"Finish playing", nil));
            }
        }
        isForward = NO;
    } else {
        // 倒轉
        if (cellPlaying.startTime <= newTime) {
            // 更新進度條
            if (newTime > 0)
                timeCount -= TIME_JUMPING;
            else
                timeCount = 0;
            float percentage = timeCount / (cellPlaying.endTime - cellPlaying.startTime);
            
            [cellPlaying.pgrsView setProgress:percentage animated:pgrsAnimated];
            [audioPlayer setCurrentTime:newTime];
        } else {
            [self cancelPlay:cellPlaying reset:NO];
            
            // 到上一個Cell
            for(int i = 1; i <= cellPlaying.row; i++) {
                NSIndexPath* nextIndex = [NSIndexPath indexPathForRow:(cellPlaying.row - i) inSection:0];
                PartTableViewCell *cell = (PartTableViewCell*)[self.tableView cellForRowAtIndexPath:nextIndex];
                
                if (cell.startTime <= newTime && cell.endTime > newTime) {
                    playStartTime = newTime;
                    [self playPart:cell];
                    
                    if (UIAccessibilityIsVoiceOverRunning()) {
                        // 會頓，要檢查是否自動轉移VO焦點(或提前一秒轉移??)
                        isAudoFocus = YES;
                        cell.isFocusDone = YES;
                        //UIAccessibilityPostNotification(UIAccessibilityLayoutChangedNotification, [self.tableView accessibilityElementAtIndex:[nextIndex row]]);
                    }
                    if (cell.row > 3) {
                        [self.tableView scrollToRowAtIndexPath:nextIndex atScrollPosition:UITableViewScrollPositionBottom animated:YES];
                    }
                    break;
                }
            }
        }
        
    }
    
}

- (void)playFirstPart
{
    [self.imgPlay setImage:[UIImage imageNamed:@"listen_pause.png"]];
    
    // 自動選取第一項
    NSIndexPath* indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    PartTableViewCell *cell = (PartTableViewCell*)[self.tableView cellForRowAtIndexPath:indexPath];
    [self playPart:cell];
}

- (void)playEffect:(NSURL*)url
{
    effectPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:nil];
    [effectPlayer setDelegate:self];
    [effectPlayer prepareToPlay];
    [effectPlayer play];
}

- (void)pauseOrContinue
{
    if (isPaused) {
        isPaused = NO;
        [self playEffect:pauseURL];
        [audioPlayer play];
        if (!timer.isValid) {
            timer = [NSTimer scheduledTimerWithTimeInterval:0.01 target:self selector:@selector(updateTimer) userInfo:nil repeats:YES];
        }
        [timer setFireDate:[NSDate date]];
        [self.btnMask setHidden:YES];
        [self.imgPlay setImage:[UIImage imageNamed:@"listen_pause.png"]];
        
        // 找回VO焦點
        if (UIAccessibilityIsVoiceOverRunning()) {
            isAudoFocus = YES;
            UIAccessibilityPostNotification(UIAccessibilityLayoutChangedNotification, [self.tableView accessibilityElementAtIndex:cellPlaying.row]);
        }
        
        [pauseTimer invalidate];
        // GA Timing
        [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createTimingWithCategory:@"playing" interval:[NSNumber numberWithInt:pauseTime] name:@"play_pause_length" label:@"play"] build]];
        
        // GA
        [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:@"playing" action:@"play_resume" label:self.soundFile.name value:[NSNumber numberWithInt:pauseTime]] build]];
        [[UXTestingManager sharedInstance] customEvent:@"繼續播放檔案"];
    } else {
        if (audioPlayer.isPlaying) {
            isPaused = YES;
            [audioPlayer pause];
            [timer setFireDate:[NSDate distantFuture]];
            [self playEffect:pauseURL];
            [self.btnMask setHidden:NO];
            [self.imgPlay setImage:[UIImage imageNamed:@"listen_play.png"]];
            
            UIAccessibilityPostNotification(UIAccessibilityLayoutChangedNotification, self.btnMask);
            
            pauseTime = 0;
            pauseTimer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(ticking) userInfo:nil repeats:YES];
            
            // GA
            [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:@"playing" action:@"play_pause" label:self.soundFile.name value:nil] build]];
            [[UXTestingManager sharedInstance] customEvent:@"暫停播放檔案"];
        }
    }
}

- (void)speakHint
{
    UIAccessibilityPostNotification(UIAccessibilityAnnouncementNotification,
                                    NSLocalizedString(@"Paused, double tap to resume", nil));
}

- (NSString *)durationText:(NSTimeInterval)time
{
    if (time) {
        float hours = 0.0;
        float minutes = 0.0;
        
        if (time >= 3600.0){
            hours = floor(time / 3600);
            time -= hours * 3600;
        }
        if (time >= 60.0) {
            minutes = floor(time / 60);
            time -= minutes * 60;
        }
        float seconds = time;
        
        return hours > 0? [NSString stringWithFormat:@"%02.0f:%02.0f:%02.0f", hours, minutes, seconds] :  [NSString stringWithFormat:@"%02.0f:%02.0f", minutes, seconds];
    } else {
        return @"00:00";
    }
}

- (NSString *)durationVoice:(NSTimeInterval)time
{
    if (time) {
        float hours = 0.0;
        float minutes = 0.0;
        
        if (time >= 3600.0){
            hours = floor(time / 3600);
            time -= hours * 3600;
        }
        if (time >= 60.0) {
            minutes = floor(time / 60);
            time -= minutes * 60;
        }
        float seconds = time;
        
        NSString *tempHour = hours > 0? [NSString stringWithFormat:@"%.0f%@ ", hours, NSLocalizedString(@"hours", nil)]: @"";
        NSString *tempMinutes = minutes > 0? [NSString stringWithFormat:@"%.0f%@ ", minutes, NSLocalizedString(@"minutes", nil)]: @"";
        
        return [NSString stringWithFormat:@"%@ %@ %.0f%@", tempHour, tempMinutes, seconds, NSLocalizedString(@"seconds", nil)];
    } else {
        return NSLocalizedString(@"Beginning", nil);
    }
}

- (void)ticking
{
    pauseTime ++;
}

- (void)setDefaultLayout
{
    [self.imgPlay setImage:[UIImage imageNamed:@"listen_play.png"]];
    self.lblTime.text = timeText;
}

#pragma mark - AVAudioPlayer Delegate

- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag
{
    if ([player.url isEqual:partURL])
        NSLog(@"播完分段音效");
    if ([player.url isEqual:pauseURL])
        NSLog(@"播完暫停音效");
}

- (void)audioPlayerBeginInterruption:(AVAudioPlayer *)player
{
    NSLog(@"播放被中斷");
}

- (void)audioPlayerEndInterruption:(AVAudioPlayer *)player withOptions:(NSUInteger)flags
{
    NSLog(@"從中斷恢復播放");
}

#pragma mark - IBAction

- (IBAction)btnPlayClicked:(id)sender {
    if (isPaused || audioPlayer.isPlaying)
        [self pauseOrContinue];
    else {
        // 從頭開始播放
        [self playFirstPart];
    }
}

- (IBAction)btnMaskClicked:(id)sender {
    [self pauseOrContinue];
}

- (void)btnShareClicked
{
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"ShareMenu"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
    [[UXTestingManager sharedInstance] enterViewWithName:@"分享選單" andDetailName:@""];
    
    // GA
    [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:@"sharing" action:@"share_start" label:self.soundFile.name value:nil] build]];
    [[UXTestingManager sharedInstance] customEvent:@"開啟分享選單"];
    
    // 先停止播放
    if (!isPaused) {
        [self pauseOrContinue];
    }
    
    NSString *menuTitle = [NSString stringWithFormat:@"%@ %@, %@", NSLocalizedString(@"Sharing file", nil), sz, NSLocalizedString(@"Sharing tip", nil)];
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:menuTitle
                                                             delegate:self
                                                    cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:NSLocalizedString(@"ShareByEmail", nil), NSLocalizedString(@"ShareByWifi", nil), nil];
    [actionSheet showInView:self.view];
}

- (void)homeAction {
    // GA
    [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:@"playing" action:@"back_home" label:@"play" value:nil] build]];
    [[UXTestingManager sharedInstance] customEvent:@"點擊回首頁按鈕"];
    
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)helpAction {
    // GA
    [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:@"playing" action:@"get_help" label:@"play" value:nil] build]];
    [[UXTestingManager sharedInstance] customEvent:@"點擊說明按鈕"];
    
    // 先停止播放
    if (!isPaused) {
        [self pauseOrContinue];
    }
    
    UIAlertView* alertHelp = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Instructions", nil)
                                                        message:NSLocalizedString(@"HelpPlay", nil)
                                                       delegate:self
                                              cancelButtonTitle:nil
                                              otherButtonTitles:NSLocalizedString(@"Close", nil), nil];
    [alertHelp setTag:10];
    [alertHelp show];
}

- (void)popViewControllerAnimated
{
    // GA
    [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:@"playing" action:@"press_back" label:@"play" value:nil] build]];
    [[UXTestingManager sharedInstance] customEvent:@"點擊返回按鈕"];
    
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - ActionSheet Delegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSString *title = [actionSheet buttonTitleAtIndex:buttonIndex];
    
    if ([title isEqualToString:NSLocalizedString(@"ShareByEmail", nil)]) {
        [self showEmail];
    }
    else if ([title isEqualToString:NSLocalizedString(@"ShareByWifi", nil)]) {
        [self showWifi];
    }
    else if ([title isEqualToString:NSLocalizedString(@"ShareByDropBox", nil)]) {
        // 加入Dropbox SDK
    }
    else if ([title isEqualToString:NSLocalizedString(@"Cancel", nil)])  {
        // GA
        [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:@"sharing" action:@"share_cancel" label:self.soundFile.name value:nil] build]];
        [[UXTestingManager sharedInstance] customEvent:@"取消分享選單"];
        
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker set:kGAIScreenName value:@"PlayView"];
        [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
        
        [[UXTestingManager sharedInstance] enterViewWithName:@"播放頁面" andDetailName:@""];
    }
}

#pragma mark - Email

- (void)showEmail
{
    // 偵測網路狀態？沒網路還是可以sent，等有網路再送達
    
    NSString *emailTitle = NSLocalizedString(@"Email title", nil);
    NSString *messageBody = NSLocalizedString(@"Email body", nil);
    NSArray *toRecipents = nil;
    
    MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
    if ([MFMailComposeViewController canSendMail]) {
        mc.mailComposeDelegate = self;
        [mc setSubject:emailTitle];
        [mc setMessageBody:messageBody isHTML:NO];
        [mc setToRecipients:toRecipents];
        
        // Get the resource path and read the file using NSData
        NSString *fileName = [NSString stringWithFormat:@"%@.mp4", self.soundFile.name];
        NSData *fileData = [NSData dataWithContentsOfFile:fileURL.path];
        
        // Add attachment
        [mc addAttachmentData:fileData mimeType:@"application/mp4" fileName:fileName];
        
        // Present mail view controller on screen
        [self presentViewController:mc animated:YES completion:NULL];
        // GA
        [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:@"sharing" action:@"email_start" label:self.soundFile.name value:nil] build]];
        [[UXTestingManager sharedInstance] customEvent:@"開始編輯郵件"];
    } else {
        // 系統自動彈出提示視窗
        NSLog(@"Can't send email");
        // GA
        [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:@"sharing" action:@"email_error" label:self.soundFile.name value:nil] build]];
        [[UXTestingManager sharedInstance] customEvent:@"開啟郵件錯誤"];
    }
}

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            UIAccessibilityPostNotification(UIAccessibilityAnnouncementNotification, NSLocalizedString(@"Mail cancelled", nil));
            // GA
            [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:@"sharing" action:@"email_cancel" label:self.soundFile.name value:nil] build]];
            [[UXTestingManager sharedInstance] customEvent:@"取消編輯郵件"];
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            UIAccessibilityPostNotification(UIAccessibilityAnnouncementNotification, NSLocalizedString(@"Mail saved", nil));
            // GA
            [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:@"sharing" action:@"email_saved" label:self.soundFile.name value:nil] build]];
            [[UXTestingManager sharedInstance] customEvent:@"儲存郵件草稿"];
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            UIAccessibilityPostNotification(UIAccessibilityAnnouncementNotification, NSLocalizedString(@"Mail sent", nil));
            // GA
            [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:@"sharing" action:@"email_send" label:self.soundFile.name value:nil] build]];
            [[UXTestingManager sharedInstance] customEvent:@"傳送郵件"];
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            UIAccessibilityPostNotification(UIAccessibilityAnnouncementNotification,
                                            [NSString stringWithFormat:@"%@, %@", NSLocalizedString(@"Mail sent failure", nil), [error localizedDescription]]);
            // GA
            [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:@"sharing" action:@"email_error" label:self.soundFile.name value:nil] build]];
            [[UXTestingManager sharedInstance] customEvent:@"編輯郵件錯誤"];
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
    
    // GA Screen
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"PlayView"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
    [[UXTestingManager sharedInstance] enterViewWithName:@"播放頁面" andDetailName:@""];
}

#pragma mark - Wi-Fi

- (void)showWifi
{
    // GA
    [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:@"sharing" action:@"wifi_start" label:@"share" value:nil] build]];
    [[UXTestingManager sharedInstance] customEvent:@"開啟wifi選單"];
    
    // Create server using our custom MyHTTPServer class
    httpServer = [[HTTPServer alloc] init];
    
    // Tell the server to broadcast its presence via Bonjour.
    // This allows browsers such as Safari to automatically discover our service.
    [httpServer setType:@"_http._tcp."];
    
    // Normally there's no need to run our server on any specific port.
    // Technologies like Bonjour allow clients to dynamically discover the server's port at runtime.
    // However, for easy testing you may want force a certain port so you can just hit the refresh button.
    [httpServer setPort:80];
    
    // Serve files from our embedded Web folder
    //NSString *webPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"Web"];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    // 自動生成網頁內容，代入檔案資訊與下載位置
    [self preparePage];
    
    [httpServer setDocumentRoot:documentsDirectory];
    
    [self startServer];
    
    hostURL = [NSString stringWithFormat:@"http://%@", [httpServer hostName]];
    NSString *alertMessage = [NSString stringWithFormat:@"%@:\n %@\n%@", NSLocalizedString(@"DownloadAddress", nil), hostURL, NSLocalizedString(@"Wifi tip", nil)];
    
    UIAlertView* alertHost = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"ShareByWifi", nil)
                                                        message:alertMessage
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"Terminate", nil)
                                              otherButtonTitles:nil];
    [alertHost setTag:11];
    [alertHost show];
}

- (void)startServer
{
    NSError *error;
    if ([httpServer start:&error])
    {
        // GA
        [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:@"sharing" action:@"wifi_connect" label:self.soundFile.name value:nil] build]];
        [[UXTestingManager sharedInstance] customEvent:@"開啟wifi連線"];
        NSLog(@"Started HTTP Server on port %hu", [httpServer listeningPort]);
    }
    else
    {
        // GA
        [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:@"sharing" action:@"wifi_error" label:self.soundFile.name value:nil] build]];
        [[UXTestingManager sharedInstance] customEvent:@"wifi連線錯誤"];
        NSLog(@"Error starting HTTP Server: %@", error);
    }
}

- (void)stopServer
{
    if (httpServer)
        [httpServer stop];
    // GA
    [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:@"sharing" action:@"wifi_stop" label:self.soundFile.name value:nil] build]];
    [[UXTestingManager sharedInstance] customEvent:@"結束wifi連線"];
    NSLog(@"Stop HTTP Server");
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 11) {
        [self stopServer];
        
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker set:kGAIScreenName value:@"PlayView"];
        [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
        
        [[UXTestingManager sharedInstance] enterViewWithName:@"播放頁面" andDetailName:@""];
    }
}

// 準備下載網頁
- (void)preparePage
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"index.html"];
    NSString *sourcePath = [[NSBundle mainBundle] pathForResource:@"index" ofType:@"html"];
    
    NSString *fileName = [NSString stringWithFormat:@"%@.mp4", self.soundFile.name];
    
    NSDate *vDate = [[UIUtility dateFormatterYYMMddHHmmss] dateFromString:[NSString stringWithFormat:@"%@%@", self.soundFile.date, self.soundFile.time]];
    NSString *recordDate = [NSString stringWithFormat:@"%@, %@", [[UIUtility dateFormatterMMMM_dd] stringFromDate:vDate], [[UIUtility dateFormatterhhmm_a] stringFromDate:vDate]];
    NSString *recordDetail = sz;
    //直接下載 <script type='text/javascript'>\n<!--\nwindow.location='%@'\n//-->\n</script>
    NSString *content = [NSString stringWithFormat:@"<html>\n<head>\n<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />\n<title>HearMe App</title>\n</head>\n<body bgcolor='#001A50' style='text-align:center;font-family:Helvetica;padding-top:300px'>\n<a style='font-size:24px;color: #F2F3F6;' href='%@'>%@ %@</a><br/>\n<p style='font-size:18px;color: #808080;line-height:25px;'>%@<br/>%@</p>\n<div style='font-size:14px;color: #F2F3F6;margin-top:100px;'>HearMe © Copyright 2015</div>\n</body>\n</html>", fileName, NSLocalizedString(@"Click to download", nil), fileName, recordDate, recordDetail];
    
    NSError *error = nil;
    
    // 先檢查刪除
    if ([[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
        if (![[NSFileManager defaultManager] removeItemAtPath:filePath error:&error]) {NSLog(@"Remove page Error: %@", error);}
    }
    
    // 複製預設網頁(error page)
    if (![[NSFileManager defaultManager] copyItemAtPath:sourcePath toPath:filePath error:&error]) {
        NSLog(@"Copy page file Error: %@", error);
    } else {
        // 覆寫下載網頁
        [content writeToFile:filePath atomically:YES encoding:NSUTF8StringEncoding error:&error];
        
        if (error) {
            NSLog(@"Rewrite page Error: %@", error);
        }
    }
}

#pragma mark - Accessibility

- (BOOL)accessibilityPerformMagicTap
{
    // GA
    [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:@"playing" action:@"magic_tap" label:@"play" value:nil] build]];
    [[UXTestingManager sharedInstance] customEvent:@"MagicTap手勢"];
    
    // 移除快速錄音手勢
    //self.shareSettings.doubleTapped = YES;
    //[self.navigationController popToRootViewControllerAnimated:YES];
    return YES;
}

- (BOOL)accessibilityPerformEscape
{
    // GA
    [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:@"playing" action:@"escape" label:@"play" value:nil] build]];
    [[UXTestingManager sharedInstance] customEvent:@"Escape手勢"];
    
    [self.navigationController popViewControllerAnimated:YES];
    return YES;
}

@end
