//
//  AppDelegate.h
//  NewHearMe
//
//  Created by AfraTsai on 2016/2/15.
//  Copyright © 2016年 IUILAB. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import <GoogleAnalytics/GAI.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
//@property (strong, nonatomic) id<GAITracker> tracker;

@end

