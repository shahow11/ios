//
//  ShareSettings.h
//  NewHearMe
//
//  Created by IUILab Air on 2016/3/5.
//  Copyright © 2016年 IUILAB. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ShareSettings : NSObject

@property (nonatomic, strong) NSString* iosVersion;
@property (nonatomic, strong) NSString* device;

@property (nonatomic) BOOL doubleTapped;
@property (nonatomic) BOOL checkedList;
@property (nonatomic) CGFloat userLatitude;
@property (nonatomic) CGFloat userLongitude;
@property (nonatomic) int lid;
@property (nonatomic, strong) NSString *postalCode;
@property (nonatomic, strong) NSString *country;
@property (nonatomic, strong) NSString *city;
@property (nonatomic, strong) NSString *district;
@property (nonatomic, strong) NSString *street;
@property (nonatomic, strong) NSString *landmark;
@property (nonatomic, strong) NSString *nearest;

@property (nonatomic, strong) UIFont *TITLE;

+ (id)sharedSettings;

@end
