//
//  SoundFile.m
//  NewHearMe
//
//  Created by IUILab Air on 2016/3/5.
//  Copyright © 2016年 IUILAB. All rights reserved.
//

#import "SoundFile.h"

@implementation SoundFile

+ (id)fileOfGroup:(int)gid vid:(int)vid name:(NSString*)name author:(NSString*)author latitude:(NSString*)lat longitude:(NSString*)lon lid:(int)lid duration:(NSTimeInterval)duration highlight:(NSTimeInterval)highlight date:(NSString*)date time:(NSString*)time isPublic:(int)isPublic
{
    SoundFile *newfile = [[self alloc] init];
    newfile.gid = gid;
    newfile.vid = vid;
    newfile.name = name;
    newfile.author = author;
    newfile.latitude = lat;
    newfile.longitude = lon;
    newfile.lid = lid;
    newfile.duration = duration;
    newfile.highlight = highlight;
    newfile.date = date;
    newfile.time = time;
    newfile.isPublic = isPublic;
    
    return newfile;
}

- (NSString *)durationText
{
    if (_duration) {
        float temp = _duration;
        float hours = 0.0;
        float minutes = 0.0;
        
        if (temp >= 3600.0){
            hours = floor(temp / 3600);
            temp -= hours * 3600;
        }
        if (temp >= 60.0) {
            minutes = floor(temp / 60);
            temp -= minutes * 60;
        }
        float seconds = temp;
        
        return hours > 0 ? [NSString stringWithFormat:@"%02.0f:%02.0f:%02.0f", hours, minutes, seconds] : [NSString stringWithFormat:@"%02.0f:%02.0f", minutes, seconds];
    } else {
        return @"00:00";
    }
}

@end
