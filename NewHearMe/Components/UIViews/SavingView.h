//
//  SavingView.h
//  NewHearMe
//
//  Created by AfraTsai on 2016/3/24.
//  Copyright © 2016年 IUILAB. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <AudioToolbox/AudioToolbox.h>
//#import <GoogleAnalytics/GAI.h>
//#import <GoogleAnalytics/GAIDictionaryBuilder.h>
//#import <GoogleAnalytics/GAIFields.h>
#import "RecordButton.h"

@protocol SavingViewDelegate;

@interface SavingView : UIView<AVAudioRecorderDelegate, AVAudioPlayerDelegate, UITextFieldDelegate, UIScrollViewDelegate>

@property (nonatomic, weak) id<SavingViewDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIView *inputView;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UITextField *tfdName;
@property (weak, nonatomic) IBOutlet UILabel *lblHint;
@property (weak, nonatomic) IBOutlet RecordButton *btnAudioIntro;
@property (weak, nonatomic) IBOutlet UIButton *btnCategory;
@property (weak, nonatomic) IBOutlet UILabel *lblCategory;
@property (weak, nonatomic) IBOutlet UIScrollView *categoryView;
@property (weak, nonatomic) IBOutlet UIView *maskView;
@property (weak, nonatomic) IBOutlet UIView *buttonView;
@property (weak, nonatomic) IBOutlet UIButton *btnSave;
@property (weak, nonatomic) IBOutlet UIButton *btnCancel;
@property (weak, nonatomic) IBOutlet UIButton *btnDelete;

@property int newCategoryID;
@property int newCategorySeq;
@property (strong, nonatomic) NSString *defaultFileName;
@property (strong, nonatomic) NSString *fileNameString;
@property (strong, nonatomic) NSString *fileIntroNameString;
@property (strong, nonatomic) NSMutableArray *categories;

- (IBAction)btnSaveClicked:(id)sender;
- (IBAction)btnCancelClicked:(id)sender;
- (IBAction)btnDeleteClicked:(id)sender;

- (void)show;
- (void)close;
- (void)recordTitle;

@end

@protocol SavingViewDelegate<NSObject>

- (void)saveAction:(id)savingView;
- (void)cancelAction:(id)savingView;
- (void)deleteAction:(id)savingView;

- (void)playTitle:(NSString *)fileString;
- (void)playSound:(NSString *)fileName;

@end
