//
//  NavigationView.m
//  NewHearMe
//
//  Created by IUILab Air on 2016/3/6.
//  Copyright © 2016年 IUILAB. All rights reserved.
//

#import "NavigationView.h"
#import "UIUtility.h"

@implementation NavigationView

- (id)initWithCoder:(NSCoder *)aDecoder {
    if ((self = [super initWithCoder:aDecoder])){
        UIView *nibView = [[[NSBundle mainBundle] loadNibNamed:@"NavigationView" owner:self options:nil] objectAtIndex:0];
        nibView.frame = CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, nibView.frame.size.height);
        [self addSubview:nibView];
    }
    return self;
}

- (void)setDarkBackgrounded {
    self.subviews[0].backgroundColor = [UIUtility BLUE_XD];
    self.lblViewTitle.textColor = [UIUtility BLUE];
    [self.btnBack setImage:[UIImage imageNamed:@"back_listen.png"] forState:UIControlStateNormal];
    [self.btnBack setImage:[UIImage imageNamed:@"back_listen_p.png"] forState:UIControlStateHighlighted];
}

@end
