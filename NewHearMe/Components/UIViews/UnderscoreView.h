//
//  UnderscoreView.h
//  NewHearMe
//
//  Created by IUILAB on 2014/11/24.
//  Copyright (c) 2014年 iuilab. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UnderscoreView : UIView

@property (nonatomic) BOOL isInfoView;
@property (nonatomic) int stayTime;
@property (nonatomic, strong) NSTimer* timer;
@property (nonatomic, strong) NSString* fileName;

@end
