//
//  UnderscoreView.m
//  NewHearMe
//
//  Created by IUILAB on 2014/11/24.
//  Copyright (c) 2014年 iuilab. All rights reserved.
//

#import "UnderscoreView.h"
//#import "GAI.h"
//#import "GAIDictionaryBuilder.h"

@implementation UnderscoreView

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    [[UIColor colorWithRed:0.0 green:175.0/255.0 blue:1.0 alpha:1.0] set];
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetLineWidth(context, 0.5);
    CGContextSetLineJoin(context, kCGLineJoinRound);
    CGContextMoveToPoint(context, 0, self.frame.size.height-0.5);
    CGContextAddLineToPoint(context, self.frame.size.width, self.frame.size.height-0.5);
    CGContextStrokePath(context);

}

- (void)accessibilityElementDidBecomeFocused
{
    if (self.isInfoView) {
        self.stayTime = 0;
        self.timer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(ticking) userInfo:nil repeats:YES];
    }
}

- (void)accessibilityElementDidLoseFocus
{
    if (self.isInfoView) {
        [self.timer invalidate];
        // GA Event
      //  [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:@"playing" action:@"read_file_info" label:self.fileName value:[NSNumber numberWithInt:self.stayTime]] build]];
        // GA Timing
        //[[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createTimingWithCategory:@"playing" interval:[NSNumber numberWithInt:self.stayTime] name:@"play_info_length" label:nil] build]];
    }
}

- (void)ticking
{
    self.stayTime ++;
}

@end
