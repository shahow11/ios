//
//  EditCategoryView.h
//  NewHearMe
//
//  Created by Afra Tsai on 6/1/16.
//  Copyright © 2016 IUILAB. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol EditCategoryViewDelegate;

@interface EditCategoryView : UIView<UIScrollViewDelegate>

@property (nonatomic, weak) id<EditCategoryViewDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIView *inputView;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnCategory;
@property (weak, nonatomic) IBOutlet UIScrollView *categoryView;
@property (weak, nonatomic) IBOutlet UIView *buttonView;
@property (weak, nonatomic) IBOutlet UIButton *btnCancel;
@property (weak, nonatomic) IBOutlet UIButton *btnConfirm;
@property (weak, nonatomic) IBOutlet UIView *maskView;

@property int newCategoryID;
@property int newCategorySeq;
@property int currentCategoryID;
@property (strong, nonatomic) NSMutableArray *categories;

- (IBAction)btnCancelClicked:(id)sender;
- (IBAction)btnConfirmClicked:(id)sender;

- (void)show;
- (void)close;

@end

@protocol EditCategoryViewDelegate<NSObject>

- (void)cancelAction:(id)editCategoryView;
- (void)confirmAction:(id)editCategoryView;

@end