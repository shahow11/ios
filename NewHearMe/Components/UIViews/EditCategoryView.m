//
//  EditCategoryView.m
//  NewHearMe
//
//  Created by Afra Tsai on 6/1/16.
//  Copyright © 2016 IUILAB. All rights reserved.
//

#import "EditCategoryView.h"
#import "SoundGroup.h"
#import "UIUtility.h"

@implementation EditCategoryView

- (id)init
{
    self = [[NSBundle mainBundle] loadNibNamed:@"EditCategoryView" owner:self options:nil][0];
    if (self)
        [self prepareMenu];
    
    return self;
}

- (void)initProperties
{
    SoundGroup *currentGroup;
    for (int i = 0; i<self.categories.count; i++) {
        SoundGroup *sg = (SoundGroup*)[self.categories objectAtIndex:i];
        if (sg.gid == self.currentCategoryID) {
            currentGroup = sg;
        }
    }
    
    self.newCategoryID = currentGroup.gid == 0? [(SoundGroup*)[self.categories objectAtIndex:1] gid]: currentGroup.gid;
    self.newCategorySeq = currentGroup.gid == 0? 1: currentGroup.seq;
    
    // Set Title
    [self.lblTitle setText:[NSString stringWithFormat:@"%@ - %@", NSLocalizedString(@"Current category", nil), currentGroup.name]];
    [self.lblTitle setAccessibilityLabel:[NSString stringWithFormat:@"%@, %@", NSLocalizedString(@"Current category", nil), currentGroup.name]];
    
    // 加入Groupe清單的按鈕
    [self.categoryView setContentSize:CGSizeMake((self.categories.count + 1) * GROUPITEM_WIDTH, 44)];
    for(int i = 1; i < self.categories.count; i++){
        UIButton *btnGroupItem = [UIButton buttonWithType:UIButtonTypeCustom];
        btnGroupItem.frame = CGRectMake(i*GROUPITEM_WIDTH, 0, GROUPITEM_WIDTH, 44);
        if (i == self.newCategorySeq) {
            [btnGroupItem setTitleColor:[UIUtility BLUE_D] forState:UIControlStateNormal];
        } else {
            [btnGroupItem setTitleColor:[UIUtility GRAY_M] forState:UIControlStateNormal];
        }
        [btnGroupItem.titleLabel setFont:[UIFont systemFontOfSize:17]];
        [btnGroupItem setTitle:NSLocalizedString([[self.categories objectAtIndex:i] name], nil) forState:UIControlStateNormal];
        btnGroupItem.isAccessibilityElement = NO;
        btnGroupItem.titleLabel.isAccessibilityElement = NO;
        [self.categoryView addSubview:btnGroupItem];
    }
    [self.categoryView setContentOffset:CGPointMake((self.newCategorySeq - 1) * GROUPITEM_WIDTH, 0)];
}

- (void)prepareMenu
{
    self.accessibilityViewIsModal = YES;
    
    // Set White Card
    self.inputView.backgroundColor = [UIUtility WHITE];
    self.inputView.layer.cornerRadius = 4;
    self.inputView.layer.shadowOffset = CGSizeZero;
    self.inputView.layer.shadowRadius = 6;
    self.inputView.layer.shadowOpacity = 0.5;
    self.inputView.layer.opacity = 0.9;
    
    // Set Category
    [self.btnCategory addTarget:self action:@selector(btnCategoryClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.btnCategory setTitle:NSLocalizedString(@"Select new category", nil) forState:UIControlStateNormal];
    self.btnCategory.accessibilityHint = NSLocalizedString(@"Double tap to select category", nil);
    
    // Category list
    [self.categoryView setDelegate:self];
    
    self.maskView.accessibilityViewIsModal = YES;
    self.maskView.accessibilityTraits = UIAccessibilityTraitAllowsDirectInteraction;
    self.maskView.accessibilityHint = NSLocalizedString(@"Swipe left and right to browse, double tap to select", nil);
    
    // 加入觸控手勢：左右滑來移動
    UISwipeGestureRecognizer *gstSwipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(categorySwipeRight:)];
    gstSwipeRight.direction = UISwipeGestureRecognizerDirectionRight;
    [self.maskView addGestureRecognizer:gstSwipeRight];
    
    UISwipeGestureRecognizer *gstSwipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(categorySwipeLeft:)];
    gstSwipeLeft.direction = UISwipeGestureRecognizerDirectionLeft;
    [self.maskView addGestureRecognizer:gstSwipeLeft];
    
    UITapGestureRecognizer *gstTapSelect = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(categoryTapSelect:)];
    gstTapSelect.numberOfTapsRequired = 2;
    [self.maskView addGestureRecognizer:gstTapSelect];
    
    // Set Action buttons
    self.buttonView.backgroundColor = [UIUtility WHITE];
    self.buttonView.layer.cornerRadius = 4;
    self.buttonView.layer.shadowOffset = CGSizeZero;
    self.buttonView.layer.shadowRadius = 6;
    self.buttonView.layer.shadowOpacity = 0.5;
    self.buttonView.layer.opacity = 0.9;
    
    [self.btnConfirm setTitle:NSLocalizedString(@"Confirm", nil) forState:UIControlStateNormal];
    [self.btnCancel setTitle:NSLocalizedString(@"Cancel", nil) forState:UIControlStateNormal];
}

#pragma mark - Category

- (void)categorySwipeRight:(UISwipeGestureRecognizer*)gs {
    CGFloat x = self.categoryView.contentOffset.x + GROUPITEM_WIDTH;
    if (x > (self.categories.count-1) * GROUPITEM_WIDTH) { x = 0; }
    [self.categoryView setContentOffset:CGPointMake(x, 0) animated:YES];
}

- (void)categorySwipeLeft:(UISwipeGestureRecognizer*)gs {
    // 向左選擇
    CGFloat x = self.categoryView.contentOffset.x - GROUPITEM_WIDTH;
    if (x < 0) { x = (self.categories.count-1) * GROUPITEM_WIDTH; }
    [self.categoryView setContentOffset:CGPointMake(x, 0) animated:YES];
}

- (void)categoryTapSelect:(UISwipeGestureRecognizer*)gs {
    // 設定選擇目前的項目 關閉mask 焦點回到btnCategory
    [self.maskView setHidden:YES];
    UIAccessibilityPostNotification(UIAccessibilityScreenChangedNotification, self.btnCategory);
}

#pragma mark - ScrollView Delegate

- (void)scrollViewDidScroll:(UIScrollView *)s {
    NSInteger middleBtn = s.contentOffset.x / GROUPITEM_WIDTH + 0.5;
    for(int i = 0; i < [s.subviews count]; i++) {
        if ([[s.subviews objectAtIndex:i] isKindOfClass:[UIButton class]]) {
            UIButton *thisBtn = (UIButton*)[s.subviews objectAtIndex:i];
            if (i == middleBtn) {
                if (self.newCategorySeq != i+1) {
                    self.newCategorySeq = i+1;
                    [thisBtn setTitleColor:[UIUtility BLUE_D] forState:UIControlStateNormal];
                    UIAccessibilityPostNotification(UIAccessibilityAnnouncementNotification, NSLocalizedString([[self.categories objectAtIndex:self.newCategorySeq] name], nil));
                    [self.btnCategory setAccessibilityLabel:[NSString stringWithFormat:@"%@, %@", NSLocalizedString(@"Selected new category", nil), NSLocalizedString([[self.categories objectAtIndex:self.newCategorySeq] name], nil)]];
                    self.newCategoryID = [[self.categories objectAtIndex:self.newCategorySeq] gid];
                }
            }
            else {
                [thisBtn setTitleColor:[UIUtility GRAY_M] forState:UIControlStateNormal];
            }
        }
    }
}

#pragma mark - Button Action

- (void)btnCategoryClicked
{
    // 加入遮罩 在遮罩中可左右滑選擇scrollview的項目
    if (UIAccessibilityIsVoiceOverRunning()) {
        [self.maskView setHidden:NO];
        
        UIAccessibilityPostNotification(UIAccessibilityScreenChangedNotification, self.maskView);        
    }
}

- (IBAction)btnCancelClicked:(id)sender {
    if (self.delegate)
        [self.delegate cancelAction:self];
}

- (IBAction)btnConfirmClicked:(id)sender {
    if (self.delegate)
        [self.delegate confirmAction:self];
}

- (void)show
{
    [self initProperties];
    
    [[[[UIApplication sharedApplication] windows] firstObject] addSubview:self];
    self.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
    
    UIAccessibilityPostNotification(UIAccessibilityScreenChangedNotification, self.lblTitle);
}

- (void)close
{
    [self removeFromSuperview];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
