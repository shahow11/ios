//
//  SavingView.m
//  NewHearMe
//
//  Created by AfraTsai on 2016/3/24.
//  Copyright © 2016年 IUILAB. All rights reserved.
//

#import "SavingView.h"
#import "SoundGroup.h"
#import "UIUtility.h"

#define MAX_TITLE_RECORD_TIME   5
#define GROUPITEM_WIDTH         94
#define MAX_TEXT_LENGTH         50
#define WAVE_UPDATE_FREQUENCY   0.05

@interface SavingView ()
{
    int namingTime;
    int waitingTime;
    float recordTitleTime;
    
    BOOL isTextEditting;
    BOOL isHoldEnough;
    BOOL isRecordingTitle;
    
    NSTimer *timerWait;
    NSTimer *timerCount;
    AVAudioRecorder *recorderT;
    AVAudioSession *audioSessionT;
}

@end

@implementation SavingView

- (id)init
{
    self = [[NSBundle mainBundle] loadNibNamed:@"SavingView" owner:self options:nil][0];
    if (self)
        [self prepareMenu];
    
    return self;
}

- (void)initProperties
{
    namingTime = 0;
    waitingTime = 0;
    recordTitleTime = 0.0;
    isTextEditting = NO;
    isHoldEnough = NO;
    isRecordingTitle = NO;
    self.newCategoryID = 0;
    self.newCategorySeq = 0;
    self.lblHint.text = @"";
    self.tfdName.text = self.defaultFileName;
    self.fileNameString = self.defaultFileName;
    self.fileIntroNameString = [NSString stringWithFormat:@"%@_Intro", self.defaultFileName];
    
    [self.btnCategory setTitle:NSLocalizedString([[self.categories objectAtIndex:self.newCategorySeq] name], nil) forState:UIControlStateNormal];
    self.btnCategory.accessibilityLabel = [NSString stringWithFormat:@"%@ %@", NSLocalizedString(@"Category of", nil), NSLocalizedString([[self.categories objectAtIndex:self.newCategorySeq] name], nil)];
    
    [self.categoryView setContentSize:CGSizeMake((self.categories.count + 2) * GROUPITEM_WIDTH, 44)];
    
    // 加入Groupe清單的按鈕
    for(int i = 0; i < self.categories.count; i++){
        UIButton *btnCategoryItem = [UIButton buttonWithType:UIButtonTypeCustom];
        btnCategoryItem.frame = CGRectMake((i + 1) * GROUPITEM_WIDTH, 0, GROUPITEM_WIDTH, 44);
        if (i == self.newCategorySeq) {
            [btnCategoryItem setTitleColor:[UIUtility BLUE_D] forState:UIControlStateNormal];
        } else {
            [btnCategoryItem setTitleColor:[UIUtility GRAY_M] forState:UIControlStateNormal];
        }
        [btnCategoryItem.titleLabel setFont:[UIFont systemFontOfSize:17]];
        [btnCategoryItem setTitle:NSLocalizedString([[self.categories objectAtIndex:i] name], nil) forState:UIControlStateNormal];
        btnCategoryItem.isAccessibilityElement = NO;
        btnCategoryItem.titleLabel.isAccessibilityElement = NO;
        [self.categoryView addSubview:btnCategoryItem];
    }
    [self.categoryView setContentOffset:CGPointMake(self.newCategorySeq * GROUPITEM_WIDTH, 0)];
}

- (void)prepareMenu
{
    self.accessibilityViewIsModal = YES;
    
    // Set White Card
    self.inputView.backgroundColor = [UIUtility WHITE];
    self.inputView.layer.cornerRadius = 4;
    self.inputView.layer.shadowOffset = CGSizeZero;
    self.inputView.layer.shadowRadius = 6;
    self.inputView.layer.shadowOpacity = 0.5;
    self.inputView.layer.opacity = 0.9;
    
    // Set Title
    self.lblTitle.textColor = [UIUtility GRAY_D];
    [self.lblTitle setText:NSLocalizedString(@"Finish and Save", nil)];
    
    // Set Text Input
    [self.lblName setTextColor:[UIUtility GRAY_M]];
    [self.lblName setText:NSLocalizedString(@"Name", nil)];
    
    self.tfdName.delegate = self;
    self.tfdName.returnKeyType = UIReturnKeyDone;
    self.tfdName.accessibilityLabel = NSLocalizedString(@"Input file name", nil);
    [self.tfdName addTarget:self action:@selector(hidetxtFieldKeyboard) forControlEvents:UIControlEventEditingDidEndOnExit];
    
    UITapGestureRecognizer* tapBackground = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hidetxtFieldKeyboard)];
    [self addGestureRecognizer:tapBackground];
    
    // Set Audio Info
    [self.btnAudioIntro setupButton];
    [self.btnAudioIntro setTitle:NSLocalizedString(@"Record audio intro", nil) forState:UIControlStateNormal];
    [self.btnAudioIntro addTarget:self action:@selector(btnNamePressDown) forControlEvents:UIControlEventTouchDown];
    [self.btnAudioIntro addTarget:self action:@selector(btnNameReleased) forControlEvents:UIControlEventTouchUpInside];
    [self.btnAudioIntro addTarget:self action:@selector(btnNameReleased) forControlEvents:UIControlEventTouchUpOutside];
    self.btnAudioIntro.accessibilityLabel = NSLocalizedString(@"Record audio intro", nil);
    self.btnAudioIntro.accessibilityHint = NSLocalizedString(@"Double tap and hold to start, release to finish", nil);
    
    // Set Category
    [self.lblCategory setText:NSLocalizedString(@"Category", nil)];
    
    [self.btnCategory addTarget:self action:@selector(btnCategoryClicked) forControlEvents:UIControlEventTouchUpInside];
    self.btnCategory.accessibilityHint = NSLocalizedString(@"Double tap to select category", nil);
    
    // Category list
    [self.categoryView setDelegate:self];
    
    self.maskView.accessibilityViewIsModal = TRUE;
    self.maskView.accessibilityTraits = UIAccessibilityTraitAllowsDirectInteraction;
    self.maskView.accessibilityHint = NSLocalizedString(@"Swipe left and right to browse, double tap to select", nil);
    
    // 加入觸控手勢：左右滑來移動
    UISwipeGestureRecognizer *gstSwipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(categorySwipeRight:)];
    gstSwipeRight.direction = UISwipeGestureRecognizerDirectionRight;
    [self.maskView addGestureRecognizer:gstSwipeRight];
    
    UISwipeGestureRecognizer *gstSwipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(categorySwipeLeft:)];
    gstSwipeLeft.direction = UISwipeGestureRecognizerDirectionLeft;
    [self.maskView addGestureRecognizer:gstSwipeLeft];
    
    UITapGestureRecognizer *gstTapSelect = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(categoryTapSelect:)];
    gstTapSelect.numberOfTapsRequired = 2;
    [self.maskView addGestureRecognizer:gstTapSelect];
    
    // Set Action buttons
    self.buttonView.backgroundColor = [UIUtility WHITE];
    self.buttonView.layer.cornerRadius = 4;
    self.buttonView.layer.shadowOffset = CGSizeZero;
    self.buttonView.layer.shadowRadius = 6;
    self.buttonView.layer.shadowOpacity = 0.5;
    self.buttonView.layer.opacity = 0.9;
    
    [self.btnSave setTitle:NSLocalizedString(@"Save", nil) forState:UIControlStateNormal];
    [self.btnCancel setTitle:NSLocalizedString(@"CancelSave", nil) forState:UIControlStateNormal];
    [self.btnDelete setTitle:NSLocalizedString(@"Delete", nil) forState:UIControlStateNormal];
}

#pragma mark - TextField Delegate

// 隱藏輸入鍵盤
- (void)hidetxtFieldKeyboard
{
    if (self.tfdName != nil)
        [self.tfdName resignFirstResponder];
    
    isTextEditting = FALSE;
}

// 語音辨識似乎有error：_NSLayoutTreeGetBaselineOffsetForGlyphAtIndex invalid glyph index 0
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    self.lblHint.text = @"";
    textField.accessibilityLabel = @"";
    
    // 過濾非法的字元
    NSCharacterSet *cs = [NSCharacterSet characterSetWithCharactersInString:RESTRICT_CHARACTERS];
    NSString *filtered = [string stringByTrimmingCharactersInSet:cs];
    filtered = [filtered stringByTrimmingCharactersInSet:[NSCharacterSet newlineCharacterSet]];
    
    // 限制輸入字元長度(拼注音當中最後會被擋)
    NSUInteger oldLength = [textField.text length];
    NSUInteger replacementLength = [string length];
    NSUInteger rangeLength = range.length;
    
    NSUInteger newLength = oldLength - rangeLength + replacementLength;
    
    BOOL returnKey = [string rangeOfString: @"\n"].location != NSNotFound;
    
    return [string isEqualToString:filtered] && (newLength <= MAX_TEXT_LENGTH || returnKey);
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    isTextEditting = TRUE;
    //[textField selectAll:self]; //or after keyboard didshow
    
    // 把其他按鈕都先關掉
    for(UIView *v in self.subviews) {
        for(UIView *subv in v.subviews) {
            if ([subv isKindOfClass:[UIButton class]]) {
                [subv setIsAccessibilityElement:NO];
            }
        }
    }
    [self.lblTitle setIsAccessibilityElement:NO];
    [self.btnAudioIntro setIsAccessibilityElement:NO];
    [self.btnCategory setIsAccessibilityElement:NO];
    
    // 開始計時
    namingTime = 0;
    timerWait = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(updateNaming) userInfo:nil repeats:YES];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if ([textField.text isEqualToString:@""]) {
        textField.text = self.defaultFileName;
    }
    
    self.fileNameString = textField.text;
    self.fileIntroNameString = [NSString stringWithFormat:@"%@_Intro", self.fileNameString];
    
    isTextEditting = FALSE;
    
    // 結束計時
    [timerWait invalidate];
   // [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createTimingWithCategory:@"recording" interval:[NSNumber numberWithInt:namingTime] name:@"record_naming_length" label:@"record"] build]];
}

// 按下Return鍵, 焦點無法設定, 會跑到"儲存"->用暴力先關掉其他按鈕，等會再開起來
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    // 檢查檔案名稱是否重複
    if (![textField.text isEqualToString:self.defaultFileName]) {
        NSString *docPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
        NSString *filePath = [docPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.mp4", textField.text]];
        if ([[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
            // AlertView 無法放到最前面, 用標簽當提示
            self.lblHint.text = NSLocalizedString(@"NameExistLabel", nil);
            textField.accessibilityLabel = [NSString stringWithFormat:@"%@, %@, %@", NSLocalizedString(@"NameExistVoice1", nil), textField.text, NSLocalizedString(@"NameExistVoice2", nil)];
            textField.text = self.defaultFileName;
        }
    }
    [self performSelector:@selector(setAccessible) withObject:nil afterDelay:2.0];
    
    [self hidetxtFieldKeyboard];
    return YES;
}

- (void)setAccessible
{
    // 把其他按鈕開回來
    for(UIView *v in self.subviews) {
        for(UIView *subv in v.subviews) {
            if ([subv isKindOfClass:[UIButton class]]) {
                [subv setIsAccessibilityElement:YES];
            }
        }
    }
    [self.lblTitle setIsAccessibilityElement:YES];
    [self.btnAudioIntro setIsAccessibilityElement:YES];
    [self.btnCategory setIsAccessibilityElement:YES];
}

- (BOOL)isNowTextEditting {
    return isTextEditting;
}

#pragma mark - Audio Intro

// 錄製標題
- (void)btnNamePressDown
{
    isHoldEnough = NO;
    if (!isTextEditting) {
        recordTitleTime = 0;
        UIAccessibilityPostNotification(UIAccessibilityAnnouncementNotification, @"");
        
        [self performSelector:@selector(holdTitle) withObject:nil afterDelay:0.3];
        
    } else {
        // 若打字中則收回鍵盤
        [self hidetxtFieldKeyboard];
    }
}

- (void)btnNameReleased
{
    if (!isHoldEnough) {
        // 很快按到，還沒開始錄製就離開，取消之後的排程
        [NSObject cancelPreviousPerformRequestsWithTarget:self];
        return;
    }
    
    if (isRecordingTitle) {
        // 只要有init Recorder就要關掉
        if (recorderT.isRecording) {
            [recorderT stop];
            [timerCount invalidate];
            [audioSessionT setCategory :AVAudioSessionCategoryMultiRoute error:nil];
            [audioSessionT setActive:NO error:nil];
            
            [self.btnAudioIntro setTitle:NSLocalizedString(@"Rename", nil) forState:UIControlStateNormal];
            [self.btnAudioIntro setSideLabelText:@""];
            [self.btnAudioIntro setProgressHidden:YES];
        }
        
        // 大於0.5秒才算有錄到
        if (recordTitleTime > 0.5) {
            // GA
     //       int recordLength = floor(recordTitleTime * 10);
         //   [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createTimingWithCategory:@"recording" interval:[NSNumber numberWithInt:recordLength] name:@"record_introducing_length" label:@"record"] build]];
            
           // [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:@"saving" action:@"record_introducing" label:self.defaultFileName value:[NSNumber numberWithInt:recordLength]] build]];
            
            [self performSelector:@selector(playTitle) withObject:nil afterDelay:0.7];
        }
        
        // 有開isRecordingTitle就有音效，要呼應
        [self.delegate playSound:@"finish_r"];
        AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
        isRecordingTitle = FALSE;
    }
}

- (void)btnCategoryClicked
{
    // 加入遮罩 在遮罩中可左右滑選擇scrollview的項目
    if (UIAccessibilityIsVoiceOverRunning()) {
        [self.maskView setHidden:NO];
        
        UIAccessibilityPostNotification(UIAccessibilityScreenChangedNotification, self.maskView);
        
        // 開始計時
        waitingTime = 0;
        timerWait = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(updateWaiting) userInfo:nil repeats:YES];
    }
}

#pragma mark - AVAudioRecorder Delegate

- (void)audioRecorderDidFinishRecording:(AVAudioRecorder *)recorder successfully:(BOOL)flag
{
    if (flag) {
        if (recordTitleTime >= MAX_TITLE_RECORD_TIME) {
            [self btnNameReleased];
        }
    }
}

/*
// AudioSession（待測試）
- (void)audioRecorderBeginInterruption:(AVAudioRecorder *)recorder
{
    NSLog(@"Recorder beginInterruption");
}

- (void)audioRecorderEndInterruption:(AVAudioRecorder *)recorder withOptions:(NSUInteger)flags
{
    NSLog(@"Recorder EndInterruption");
}
*/

// 錄製標題
- (void)recordTitle
{
    if (isRecordingTitle) {
        NSLog(@"recordTitle START");
        NSError *err = nil;
        audioSessionT = [AVAudioSession sharedInstance];
        [audioSessionT setCategory :AVAudioSessionCategoryPlayAndRecord error:&err];
        if (err){
            NSLog(@"audioSessionT: %@ %ld %@", [err domain], (long)[err code], [[err userInfo] description]);
            return;
        }
        err = nil;
        [audioSessionT setActive:YES error:&err];
        if (err){
            NSLog(@"audioSessionT: %@ %ld %@", [err domain], (long)[err code], [[err userInfo] description]);
            return;
        }
        NSString *recorderFilePath = [NSString stringWithFormat:@"%@/Documents/%@.mp4", NSHomeDirectory(), self.fileIntroNameString];
        //NSLog(@"%@", recorderFilePath);
        
        NSURL *url = [NSURL fileURLWithPath:recorderFilePath];
        
        err = nil;
        
        NSData *audioData = [NSData dataWithContentsOfFile:[url path] options: 0 error:&err];
        if (audioData)
        {
            NSFileManager *fm = [NSFileManager defaultManager];
            [fm removeItemAtPath:[url path] error:&err];
        }
        
        err = nil;
        NSMutableDictionary *recordSetting = [[NSMutableDictionary alloc] init];
        
        // You can change the settings for the voice quality
        [recordSetting setValue :[NSNumber numberWithInt:kAudioFormatMPEG4AAC] forKey:AVFormatIDKey];
        [recordSetting setValue:[NSNumber numberWithFloat:44100.0] forKey:AVSampleRateKey];
        [recordSetting setValue:[NSNumber numberWithInt: 2] forKey:AVNumberOfChannelsKey];
        recorderT = [[ AVAudioRecorder alloc] initWithURL:url settings:recordSetting error:&err];
        if (!recorderT){
            NSLog(@"recorderT: %@ %ld %@", [err domain], (long)[err code], [[err userInfo] description]);
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Warning" message:[err localizedDescription] preferredStyle:UIAlertControllerStyleAlert];
                  
                  UIAlertAction *cancel =[UIAlertAction actionWithTitle:NSLocalizedString(@"Ok",nil) style:UIAlertActionStyleCancel handler:nil];

                  
                  [alertController addAction:cancel];
            UIViewController *vc = self.window.rootViewController;
            [vc presentViewController:alertController animated:YES completion:nil];
/*
            UIAlertView *alert =
            [[UIAlertView alloc] initWithTitle: @"Warning"
                                       message: [err localizedDescription]
                                      delegate: nil
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
            [alert show];
            */
            return;
        }
        
        [recorderT setDelegate:self];
        [recorderT prepareToRecord];
        recorderT.meteringEnabled = YES;
        
        BOOL audioHWAvailable = audioSessionT.inputAvailable;
        if (! audioHWAvailable) {
         UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Warning" message:@"Audio input hardware not available" preferredStyle:UIAlertControllerStyleAlert];
                        
                        UIAlertAction *cancel =[UIAlertAction actionWithTitle:NSLocalizedString(@"Ok",nil) style:UIAlertActionStyleCancel handler:nil];

                        
                        [alertController addAction:cancel];
                        UIViewController *vc = self.window.rootViewController;
                                  [vc presentViewController:alertController animated:YES completion:nil];
/*
            UIAlertView *cantRecordAlert =
            [[UIAlertView alloc] initWithTitle: @"Warning"
                                       message: @"Audio input hardware not available"
                                      delegate: nil
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
            [cantRecordAlert show];*/
            return;
        }
        
        // 設定錄音時間上限
        [recorderT recordForDuration:MAX_TITLE_RECORD_TIME];
        [self.btnAudioIntro setProgressHidden:NO];
        timerCount = [NSTimer scheduledTimerWithTimeInterval:WAVE_UPDATE_FREQUENCY target:self selector:@selector(updateCounter) userInfo:nil repeats:YES];
    }
}

- (void)holdTitle
{
    isHoldEnough = YES;
    isRecordingTitle = TRUE;
    [self.delegate playSound:@"start_r"];
    AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
    [self performSelector:@selector(recordTitle) withObject:nil afterDelay:0.6];
}

// 輸入名稱計時
- (void)updateNaming
{
    namingTime ++;
}

// 暫停/中斷計時
- (void)updateWaiting
{
    waitingTime ++;
}

// 顯示錄音狀態
- (void)updateCounter
{
    [recorderT updateMeters];
    
    recordTitleTime += WAVE_UPDATE_FREQUENCY;
    
    [self.btnAudioIntro setTitle:NSLocalizedString(@"Hold to Record", nil) forState:UIControlStateHighlighted];
    [self.btnAudioIntro setSideLabelText:[NSString stringWithFormat:@"%.1f %@", recorderT.currentTime, NSLocalizedString(@"sec", nil)]];
    [self.btnAudioIntro setProgressValue:recorderT.currentTime / 5];
}

- (void)playTitle
{
    [self.delegate playTitle:self.fileIntroNameString];
}

#pragma mark - Category

- (void)categorySwipeRight:(UISwipeGestureRecognizer*)gs {
    CGFloat x = self.categoryView.contentOffset.x + GROUPITEM_WIDTH;
    if (x > (self.categories.count-1) * GROUPITEM_WIDTH) { x = 0; }
    [self.categoryView setContentOffset:CGPointMake(x, 0) animated:YES];
}

- (void)categorySwipeLeft:(UISwipeGestureRecognizer*)gs {
    // 向左選擇
    CGFloat x = self.categoryView.contentOffset.x - GROUPITEM_WIDTH;
    if (x < 0) { x = (self.categories.count-1) * GROUPITEM_WIDTH; }
    [self.categoryView setContentOffset:CGPointMake(x, 0) animated:YES];
}

- (void)categoryTapSelect:(UISwipeGestureRecognizer*)gs {
    // 設定選擇目前的項目 關閉mask 焦點回到btnCategory
    [self.maskView setHidden:YES];
    UIAccessibilityPostNotification(UIAccessibilityScreenChangedNotification, self.btnCategory);
    
    // 結束計時
    [timerWait invalidate];
    /*
    [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createTimingWithCategory:@"recording" interval:[NSNumber numberWithInt:waitingTime] name:@"record_grouping_length" label:@"record"] build]];
    
    // GA
    [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:@"saving" action:@"record_grouping" label:[(SoundGroup*)self.categories[self.newCategorySeq] name] value:[NSNumber numberWithInt:waitingTime]] build]];
     */
}

#pragma mark - ScrollView Delegate

- (void)scrollViewDidScroll:(UIScrollView *)s {
    // 取得目前在中間的btn index
    NSInteger middleBtn = s.contentOffset.x / GROUPITEM_WIDTH + 0.5;
    for(int i = 0; i < [s.subviews count]; i++) {
        if ([[s.subviews objectAtIndex:i] isKindOfClass:[UIButton class]]) {
            UIButton *thisBtn = (UIButton*)[s.subviews objectAtIndex:i];
            if (i == middleBtn) {
                if (self.newCategorySeq != i) {
                    [thisBtn setTitleColor:[UIUtility BLUE_D] forState:UIControlStateNormal];
                    UIAccessibilityPostNotification(UIAccessibilityAnnouncementNotification, NSLocalizedString([[self.categories objectAtIndex:i] name], nil));
                    [self.btnCategory setTitle:NSLocalizedString([[self.categories objectAtIndex:i] name], nil) forState:UIControlStateNormal];
                    self.btnCategory.accessibilityLabel = [NSString stringWithFormat:@"%@ %@", NSLocalizedString(@"Category of", nil), NSLocalizedString([[self.categories objectAtIndex:i] name], nil)];
                    self.newCategorySeq = i;
                    self.newCategoryID = [[self.categories objectAtIndex:i] gid];
                }
            }
            else {
                [thisBtn setTitleColor:[UIUtility GRAY_M] forState:UIControlStateNormal];
            }
        }
    }
}

#pragma mark - AlertView delegate

- (void)alertView:(UIAlertController *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 1) {
        [self.delegate deleteAction:self];
    } else {
        // GA
      //  [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:@"saving" action:@"record_deletecancel" label:@"record" value:nil] build]];
    }
}

#pragma mark - Button Action

- (IBAction)btnSaveClicked:(id)sender {
    if (self.delegate)
        [self.delegate saveAction:self];
}

- (IBAction)btnCancelClicked:(id)sender {
    if (self.delegate)
        [self.delegate cancelAction:self];
}

- (IBAction)btnDeleteClicked:(id)sender {
    if (![self isNowTextEditting]) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Delete this recording?", nil) message:@"" preferredStyle:UIAlertControllerStyleAlert];
              
              UIAlertAction *cancel =[UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:nil];

              UIAlertAction *sure =[UIAlertAction actionWithTitle:NSLocalizedString(@"Confirm", nil) style:UIAlertActionStyleDefault handler:nil];
              [alertController addAction:sure];
              [alertController addAction:cancel];
         //   UIViewController *controller = [UIApplication sharedApplication].keyWindow.rootViewController;

              UIViewController *vc = self.window.rootViewController;
                        [vc presentViewController:alertController animated:YES completion:nil];
/*
        UIAlertView *alertDelete = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Delete this recording?", nil)
                                                              message:@""
                                                             delegate:self
                                                    cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                                                    otherButtonTitles:NSLocalizedString(@"Confirm", nil), nil];
        [alertDelete show];
 */
    }
}

- (void)show
{
    [self initProperties];
    
    [[[[UIApplication sharedApplication] windows] firstObject] addSubview:self];
    self.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
    
    UIAccessibilityPostNotification(UIAccessibilityScreenChangedNotification, self.lblTitle);
    
    // GA
    //id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    //[tracker set:kGAIScreenName value:SCREEN_NAME_SAVE];
    //[tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

- (void)close
{
    [self removeFromSuperview];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
