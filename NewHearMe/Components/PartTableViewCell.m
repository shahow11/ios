//
//  PartTableViewCell.m
//  NewHearMe
//
//  Created by AfraTsai on 2016/3/7.
//  Copyright © 2016年 IUILAB. All rights reserved.
//

#import "PartTableViewCell.h"

@implementation PartTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - Accessibility

- (UIAccessibilityTraits)accessibilityTraits
{
    //if (self.isFocusDone)
        return UIAccessibilityTraitAdjustable;
    //else
    //    return UIAccessibilityTraitNone;
}

- (NSString *)accessibilityHint {
    return @"";
}

- (NSString*)accessibilityValue {
    return @"";
}

- (void)accessibilityElementDidBecomeFocused
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(playPart:)])
        [self.delegate playPart:self];
    
    //[self performSelector:@selector(temp) withObject:nil afterDelay:0.2];
    
    self.focusTime = 0;
    self.timer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(ticking) userInfo:nil repeats:YES];
}

- (void)accessibilityElementDidLoseFocus
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(cancelPlay:reset:)])
        [self.delegate cancelPlay:self reset:YES];
    
    if (self.timer) {
        [self.timer invalidate];
        self.timer = nil;
    }
    
    // GA
   // [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:@"playing" action:@"focus_on_cell" label:@"play" value:[NSNumber numberWithInt:self.focusTime]] build]];
}

- (void)accessibilityIncrement
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(jumpForward)])
        [self.delegate jumpForward];
}

- (void)accessibilityDecrement
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(jumpBackward)])
        [self.delegate jumpBackward];
}

- (void)temp {
    
    self.isFocusDone = YES;
    //self.accessibilityTraits = UIAccessibilityTraitAdjustable;
}

- (void)ticking
{
    self.focusTime ++;
}

@end
