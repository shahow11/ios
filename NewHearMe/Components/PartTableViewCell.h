//
//  PartTableViewCell.h
//  NewHearMe
//
//  Created by AfraTsai on 2016/3/7.
//  Copyright © 2016年 IUILAB. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import <GoogleAnalytics/GAI.h>
//#import <GoogleAnalytics/GAIDictionaryBuilder.h>

@protocol PartTableViewCellDelegate;

@interface PartTableViewCell : UITableViewCell

@property (nonatomic, assign) id<PartTableViewCellDelegate> delegate;

@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblTime;
@property (weak, nonatomic) IBOutlet UIProgressView *pgrsView;

@property BOOL isFocusDone;
@property int row;
@property float startTime;
@property float endTime;
@property int focusTime;
@property (nonatomic, strong) NSTimer *timer;

@end

@protocol PartTableViewCellDelegate <NSObject>

- (void)playPart:(PartTableViewCell*)cell;
- (void)jumpForward;
- (void)jumpBackward;
- (void)cancelPlay:(PartTableViewCell*)cell reset:(BOOL)resetAll;

@end
