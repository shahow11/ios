//
//  SegueNavigationController.h
//  NewHearMe
//
//  Created by Afra Tsai on 6/4/16.
//  Copyright © 2016 IUILAB. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SegueNavigationController : UINavigationController

@end
