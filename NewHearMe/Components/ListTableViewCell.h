//
//  ListTableViewCell.h
//  NewHearMe
//
//  Created by IUILab Air on 2016/3/6.
//  Copyright © 2016年 IUILAB. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import <GoogleAnalytics/GAI.h>
//#import <GoogleAnalytics/GAIDictionaryBuilder.h>
#import "SoundFile.h"

@protocol ListTableViewCellDelegate;

@interface ListTableViewCell : UITableViewCell

@property (nonatomic, assign) id<ListTableViewCellDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIButton *btnPreview;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblSubTitle;

@property (nonatomic, strong) NSString *trackTag;
@property (nonatomic, strong) NSString *dateTag;
@property (nonatomic, strong) NSString *lkey;
@property (nonatomic, strong) NSString *day;
@property (strong, nonatomic) NSMutableArray *playList;

@property (strong, nonatomic) SoundFile *soundFile;
@property (strong, nonatomic) NSString *voiceTitle;

@property int gid;
@property int focusTime;
@property BOOL isAccessibilityFocused;
@property (nonatomic, strong) NSTimer *timer;

- (IBAction)btnPreviewClicked:(id)sender;

@end

@protocol ListTableViewCellDelegate <NSObject>

- (void)playCellList:(ListTableViewCell*)cell;
- (void)cancelPlay;

@end
