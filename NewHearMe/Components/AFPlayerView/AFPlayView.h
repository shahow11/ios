//
//  AFPlayView.h
//  HearMe
//
//  Created by iuilab on 13/8/17.
//  Copyright (c) 2013年 iuilab. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <AudioToolbox/AudioToolbox.h>

@interface AFPlayView : UIView<AVAudioPlayerDelegate>
{
    int soundMeters[181];
    AVAudioPlayer* audioPlayer;
}

@property (strong, nonatomic) NSTimer* timer;
@property BOOL isReStart;

- (NSTimeInterval)duration;
- (NSTimeInterval)currentTime;

- (BOOL)isPlaying;

- (void)playFile;
- (void)stopPlay;
- (void)pausePlay;
- (void)updateMeters;

- (void)setCurrentTime:(NSTimeInterval)currentTime;
- (void)setPlayRate:(float)playRate;
- (void)setupPlayer:(NSString*)filename;

- (void)addSoundMeterItem;
- (void)clearMeterItem;

@end
