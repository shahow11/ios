//
//  AFPlayView.m
//  HearMe
//
//  Created by iuilab on 13/8/17.
//  Copyright (c) 2013年 iuilab. All rights reserved.
//

#import "AFPlayView.h"

#define SOUND_METER_COUNT   (([[UIScreen mainScreen] bounds].size.height==480)? 147:181)
#define START_RED           80
#define START_GREEN         200
#define START_BLUE          255
#define END_RED           0
#define END_GREEN         26
#define END_BLUE          80

@implementation AFPlayView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.contentMode = UIViewContentModeRedraw;
        
        // 清空sound meters
        for(int i = 0; i < SOUND_METER_COUNT; i++) {
            soundMeters[i] = 0;
        }
    }
    return self;
}

- (void)drawRect:(CGRect)rect {
    // Draw seperate line
    [[UIColor colorWithRed:START_RED/255.0 green:START_GREEN/255.0 blue:START_BLUE/255.0 alpha:1.0] set];
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetLineWidth(context, 0.5);
    CGContextSetLineJoin(context, kCGLineJoinRound);
    CGContextMoveToPoint(context, 0, 0);
    CGContextAddLineToPoint(context, self.frame.size.width, 0);
    CGContextStrokePath(context);
    
    // Draw sound meter wave
    int baseLine = 160;
    float maxLengthOfWave = 150;
    float maxValueOfMeter = 50;
    CGFloat newY = self.frame.size.height - 2;
    for(CGFloat x = SOUND_METER_COUNT - 1; x >= 0; x--)
    {
        if(abs(soundMeters[(int)x]) > 0) {
            // Get line's percentage of position
            double percent = newY / self.frame.size.height;
            [[self colorAtPercent:percent] set];
            
            CGContextRef context = UIGraphicsGetCurrentContext();
            CGContextSetLineWidth(context, 1.0);
            CGContextSetLineJoin(context, kCGLineJoinRound);
            
            int lineWidth = (int)roundf(((maxValueOfMeter - abs(soundMeters[(int)x])) / maxValueOfMeter) * maxLengthOfWave);
            
            CGContextMoveToPoint(context, baseLine - lineWidth, newY);
            CGContextAddLineToPoint(context, baseLine + lineWidth, newY);
            CGContextStrokePath(context);
            newY -= 2;
        }
    }
}

- (void)shiftSoundMeterLeft {
    for(int i=0; i<SOUND_METER_COUNT - 1; i++) {
        soundMeters[i] = soundMeters[i+1];
    }
}

- (void)addSoundMeterItem {
    if (audioPlayer.playing)
    {        
        //int lastValue = [audioPlayer averagePowerForChannel:0];
        int lastValue = (((int)[audioPlayer averagePowerForChannel:0]) + 120) * (5.0 / 6.0);
        //NSLog(@"%.2f",[audioPlayer averagePowerForChannel:0]);
        
        [self shiftSoundMeterLeft];
        soundMeters[SOUND_METER_COUNT - 1] = lastValue;
        
        [self setNeedsDisplay];
        audioPlayer.meteringEnabled = YES;
    }
}

- (void)clearMeterItem
{
    for(int i = 0; i < SOUND_METER_COUNT; i++) {
        soundMeters[i] = 0;
    }
    
    [self setNeedsDisplay];
}

- (void)setupPlayer:(NSString*)filename
{
    NSURL *url = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/Documents/%@.mp4", NSHomeDirectory(), filename]];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:[url path]]) {
        NSLog(@"files not found!!!");
    }
    
    NSError *error;
    audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&error];
    if (error)
    {
        NSLog(@"Error in audioPlayer: %@", [error localizedDescription]);
    } else {
        audioPlayer.delegate = self;
        audioPlayer.enableRate = YES;
        [audioPlayer setMeteringEnabled: YES];
        [audioPlayer prepareToPlay];
    }
}

// 播放音訊檔案
- (void)playFile
{
    if(self.isReStart) {
        //NSLog(@"self.isReStart");
        //[self clearMeterItem];
        [self setNeedsDisplay];
        self.isReStart = FALSE;
    }
    [audioPlayer play];
}

- (void)stopPlay
{
    [audioPlayer stop];
}

- (void)pausePlay
{
    [audioPlayer pause];
}

- (void)updateMeters
{
    [audioPlayer updateMeters];
}

- (void)setCurrentTime:(NSTimeInterval)currentTime
{
    audioPlayer.currentTime = currentTime;
}

- (void)setPlayRate:(float)playRate
{
    audioPlayer.rate = playRate;
}

- (BOOL)isPlaying
{
    return audioPlayer.isPlaying;
}

- (NSTimeInterval)duration
{
    return audioPlayer.duration;
}

- (NSTimeInterval)currentTime
{
    return audioPlayer.currentTime;
}

- (UIColor*)colorAtPercent:(double)percent
{
    double resultRed = START_RED + percent * (END_RED - START_RED);
    double resultGreen = START_GREEN + percent * (END_GREEN - START_GREEN);
    double resultBlue = START_BLUE + percent * (END_BLUE - START_BLUE);
    return [UIColor colorWithRed:resultRed/255 green:resultGreen/255 blue:resultBlue/255 alpha:1.0];
}

#pragma mark - Accessibility

- (UIAccessibilityTraits)accessibilityTraits {
    return UIAccessibilityTraitAllowsDirectInteraction;
}

- (void)accessibilityElementDidBecomeFocused
{
    [self playFile];
    [self.timer setFireDate:[NSDate date]];
}

- (void)accessibilityElementDidLoseFocus
{
    [self stopPlay];
}

- (void)accessibilityIncrement
{
    NSLog(@"accessibilityIncrement");
}

- (BOOL) accessibilityScroll: (UIAccessibilityScrollDirection) direction
{
    NSString *announcement;
    switch (direction) {
        case UIAccessibilityScrollDirectionLeft:
        {
            NSLog(@"Left");
            announcement = @"right-handed";
        }
            break;
            
        case UIAccessibilityScrollDirectionRight:
        {
            NSLog(@"Right");
            announcement = @"left-handed";
        }
            break;
            
        case UIAccessibilityScrollDirectionNext:
        {
            //NSLog(@"Next");
        }
        case UIAccessibilityScrollDirectionDown:
        {
            //NSLog(@"Down");
        }
        case UIAccessibilityScrollDirectionUp:
        {
            //NSLog(@"Up");
        }
            break;
        default:
            return NO;;
    }
    
    UIAccessibilityPostNotification(UIAccessibilityPageScrolledNotification,
                                    announcement);
    return YES;
}

#pragma mark - AVAudioPlayer Delegate Method

- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag {
    self.isReStart = TRUE;
    UIAccessibilityPostNotification(UIAccessibilityAnnouncementNotification, NSLocalizedString(@"Finish playing", nil));
}

@end
