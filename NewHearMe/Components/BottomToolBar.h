//
//  BottomToolBar.h
//  NewHearMe
//
//  Created by IUILab Air on 2016/3/6.
//  Copyright © 2016年 IUILAB. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import <GoogleAnalytics/GAI.h>
//#import <GoogleAnalytics/GAIDictionaryBuilder.h>

@interface BottomToolBar : UIToolbar

@property (weak, nonatomic) IBOutlet UIBarButtonItem *btnHome;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *btnHelp;

@end
