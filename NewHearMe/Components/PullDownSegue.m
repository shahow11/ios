//
//  PullDownSegue.m
//  NewHearMe
//
//  Created by Afra Tsai on 6/2/16.
//  Copyright © 2016 IUILAB. All rights reserved.
//

#import "PullDownSegue.h"

@implementation PullDownSegue

- (void)perform
{
    UIView *sourceView = ((UIViewController *)self.sourceViewController).view;
    UIView *destinationView = ((UIViewController *)self.destinationViewController).view;
    
    UIWindow *window = [[[UIApplication sharedApplication] delegate] window];
    CGFloat startY = (25.0/568.0) * [UIScreen mainScreen].bounds.size.height;
    CGFloat centerY = [UIScreen mainScreen].bounds.size.height / 2;
    destinationView.center = CGPointMake(sourceView.center.x,  -startY);
    [window insertSubview:destinationView aboveSubview:sourceView];
    
    [UIView animateWithDuration:0.3
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         destinationView.center = CGPointMake(sourceView.center.x, centerY);
                         sourceView.center = CGPointMake(sourceView.center.x, centerY+startY);
                     }
                     completion:^(BOOL finished) {
                         [[self sourceViewController] presentViewController:[self destinationViewController] animated:NO completion:nil];
                     }];
}

@end
