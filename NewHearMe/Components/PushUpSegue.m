//
//  PushUpSegue.m
//  NewHearMe
//
//  Created by Afra Tsai on 6/2/16.
//  Copyright © 2016 IUILAB. All rights reserved.
//

#import "PushUpSegue.h"

@implementation PushUpSegue

- (void)perform
{
    UIView *sourceView = ((UIViewController *)self.sourceViewController).view;
    UIView *destinationView = ((UIViewController *)self.destinationViewController).view;
    
    UIWindow *window = [[[UIApplication sharedApplication] delegate] window];
    CGFloat startY = (25.0/568.0) * [UIScreen mainScreen].bounds.size.height;
    CGFloat endY = (309.0/568.0) * [UIScreen mainScreen].bounds.size.height;
    CGFloat centerY = [UIScreen mainScreen].bounds.size.height / 2;
    destinationView.center = CGPointMake(sourceView.center.x,  centerY+endY);
    destinationView.alpha = 0.0;
    [window insertSubview:destinationView belowSubview:sourceView];
    
    [UIView animateWithDuration:0.3
                          delay:0.2
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         destinationView.alpha = 1.0;
                         destinationView.center = CGPointMake(sourceView.center.x, centerY);
                         sourceView.center = CGPointMake(sourceView.center.x, -startY);
                     }
                     completion:^(BOOL finished) {
                         [[self destinationViewController] dismissViewControllerAnimated:NO completion:nil];
                     }];
}

@end
