//
//  SoundFile.h
//  NewHearMe
//
//  Created by IUILab Air on 2016/3/5.
//  Copyright © 2016年 IUILAB. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SoundFile : NSObject

@property int vid;
@property int gid;
@property int lid;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *author;
@property (nonatomic, copy) NSString *latitude;
@property (nonatomic, copy) NSString *longitude;
@property (nonatomic, copy) NSString *durationText;
@property NSTimeInterval duration;
@property NSTimeInterval highlight;
@property (nonatomic, copy) NSString * date;
@property (nonatomic, copy) NSString * time;
@property int isPublic;

+ (id)fileOfGroup:(int)gid vid:(int)vid name:(NSString*)name author:(NSString*)author latitude:(NSString*)lat longitude:(NSString*)lon lid:(int)lid duration:(NSTimeInterval)duration highlight:(NSTimeInterval)highlight date:(NSString*)date time:(NSString*)time isPublic:(int)isPublic;

@end
