//
//  SoundGroup.h
//  NewHearMe
//
//  Created by IUILab Air on 2016/3/5.
//  Copyright © 2016年 IUILAB. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SoundGroup : NSObject

@property int gid;
@property (nonatomic, copy) NSString *name;
@property int seq;

+ (id)groupByID:(int)gid name:(NSString*)name seq:(int)sequence;

@end
