//
//  SoundSection.h
//  NewHearMe
//
//  Created by IUILab Air on 2016/3/5.
//  Copyright © 2016年 IUILAB. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SoundSection : NSObject

@property int vid;
@property int sid;
@property (nonatomic, copy) NSString *name;
@property NSTimeInterval timestamp;

+ (id)sectionOfFile:(int)sid vid:(int)vid name:(NSString*)name timestamp:(NSTimeInterval)timestamp;

@end
