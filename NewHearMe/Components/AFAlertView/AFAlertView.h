//
//  AFAlertView.h
//  HearMe
//
//  Created by iuilab on 13/8/15.
//  Copyright (c) 2013年 iuilab. All rights reserved.
//

#import <UIKit/UIKit.h>

extern NSString *const AFAlertViewWillShowNotification;
extern NSString *const AFAlertViewDidShowNotification;
extern NSString *const AFAlertViewWillDismissNotification;
extern NSString *const AFAlertViewDidDismissNotification;

typedef NS_ENUM(NSInteger, AFAlertViewButtonType) {
    AFAlertViewButtonTypeDefault = 0,
    AFAlertViewButtonTypeDestructive,
    AFAlertViewButtonTypeCancel,
    AFAlertViewButtonTypeDisable,
    AFAlertViewButtonTypeCustom
};

typedef NS_ENUM(NSInteger, AFAlertViewBackgroundStyle) {
    AFAlertViewBackgroundStyleGradient = 0,
    AFAlertViewBackgroundStyleSolid,
};

typedef NS_ENUM(NSInteger, AFAlertViewTransitionStyle) {
    AFAlertViewTransitionStyleFade = 0,
    AFAlertViewTransitionStyleSlideFromBottom,
    AFAlertViewTransitionStyleSlideFromTop,
    AFAlertViewTransitionStyleBounce,
    AFAlertViewTransitionStyleDropDown
};

@class AFAlertView;
typedef void(^AFAlertViewHandler)(AFAlertView *alertView);

@interface AFAlertView : UIView

@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *message;
@property (nonatomic, copy) NSString *backImage;
@property CGFloat customHeight;

@property (nonatomic, assign) AFAlertViewTransitionStyle transitionStyle;
@property (nonatomic, assign) AFAlertViewBackgroundStyle backgroundStyle;

@property (nonatomic, copy) AFAlertViewHandler willShowHandler;
@property (nonatomic, copy) AFAlertViewHandler didShowHandler;
@property (nonatomic, copy) AFAlertViewHandler willDismissHandler;
@property (nonatomic, copy) AFAlertViewHandler didDismissHandler;

@property (nonatomic, readonly, getter = isVisible) BOOL visible;

@property (nonatomic, strong) UIColor *titleColor NS_AVAILABLE_IOS(5_0) UI_APPEARANCE_SELECTOR;
@property (nonatomic, strong) UIColor *messageColor NS_AVAILABLE_IOS(5_0) UI_APPEARANCE_SELECTOR;
@property (nonatomic, strong) UIFont *titleFont NS_AVAILABLE_IOS(5_0) UI_APPEARANCE_SELECTOR;
@property (nonatomic, strong) UIFont *messageFont NS_AVAILABLE_IOS(5_0) UI_APPEARANCE_SELECTOR;
@property (nonatomic, strong) UIFont *buttonFont NS_AVAILABLE_IOS(5_0) UI_APPEARANCE_SELECTOR;
@property (nonatomic, assign) CGFloat cornerRadius NS_AVAILABLE_IOS(5_0) UI_APPEARANCE_SELECTOR; // default is 2.0
@property (nonatomic, assign) CGFloat shadowRadius NS_AVAILABLE_IOS(5_0) UI_APPEARANCE_SELECTOR; // default is 8.0

- (id)initWithTitle:(NSString *)title andMessage:(NSString *)message;
- (void)addButtonWithTitle:(NSString *)title type:(AFAlertViewButtonType)type handler:(AFAlertViewHandler)handler;
- (void)addCustomView:(UIView*)view;
- (void)show;
- (void)dismissAnimated:(BOOL)animated;

@end
