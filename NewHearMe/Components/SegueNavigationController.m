//
//  SegueNavigationController.m
//  NewHearMe
//
//  Created by Afra Tsai on 6/4/16.
//  Copyright © 2016 IUILAB. All rights reserved.
//

#import "SegueNavigationController.h"
#import "PushUpSegue.h"

@interface SegueNavigationController ()

@end

@implementation SegueNavigationController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)unwindForSegue:(UIStoryboardSegue *)unwindSegue towardsViewController:(UIViewController *)subsequentVC {

    NSString *restorationId = subsequentVC.restorationIdentifier;
    if ([@"backFromRecord" isEqualToString:restorationId]) {
       PushUpSegue *segue =  [[PushUpSegue alloc] initWithIdentifier:restorationId source:unwindSegue.sourceViewController destination:unwindSegue.destinationViewController];
        [segue perform];
    }else{
        [super unwindForSegue:unwindSegue towardsViewController:subsequentVC];
    }
}
/*
- (UIStoryboardSegue *)segueForUnwindingToViewController:(UIViewController *)toViewController fromViewController:(UIViewController *)fromViewController identifier:(NSString *)identifier
{
    if ([@"backFromRecord" isEqualToString:identifier]) {
        return [[PushUpSegue alloc] initWithIdentifier:identifier source:fromViewController destination:toViewController];
        
        source:unwindSegue.sourceViewController destination:unwindSegue.destinationViewController]
    } else {
        return [super segueForUnwindingToViewController:toViewController fromViewController:fromViewController identifier:identifier];
    }
}
*/
@end
