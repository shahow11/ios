//
//  ShareSettings.m
//  NewHearMe
//
//  Created by IUILab Air on 2016/3/5.
//  Copyright © 2016年 IUILAB. All rights reserved.
//

#import "ShareSettings.h"

@implementation ShareSettings

+ (id)sharedSettings
{
    static id shareSettings = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shareSettings = [[self alloc] init];
    });
    return shareSettings;
}

- (void)setUserLatitude:(CGFloat)userLatitude
{
    if (_userLatitude == userLatitude) {
        return;
    }
    _userLatitude = userLatitude;
}

- (void)setUserLongitude:(CGFloat)userLongitude
{
    if (_userLongitude == userLongitude) {
        return;
    }
    _userLongitude = userLongitude;
}

- (void)setLocation:(int)lid
{
    if (_lid == lid) {
        return;
    }
    _lid = lid;
}

- (NSString*)iosVersion
{
    return [[UIDevice currentDevice] systemVersion];
}

- (NSString*)device
{
    return [[UIDevice currentDevice] model];
}

@end

