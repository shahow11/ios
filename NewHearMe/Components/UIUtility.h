//
//  Utility.h
//  NewHearMe
//
//  Created by IUILab Air on 2016/3/6.
//  Copyright © 2016年 IUILAB. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#define IS_IPHONE_5 (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height == 568.0f)
#define IS_RETINA ([[UIScreen mainScreen] scale] == 2.0f)

#define IS_IOS7     ([[UIDevice currentDevice].systemVersion floatValue] >= 7.0)
#define IS_IOS8     ([[UIDevice currentDevice].systemVersion floatValue] >= 8.0)
#define IS_IOS9     ([[UIDevice currentDevice].systemVersion floatValue] >= 9.0)

#define GROUPITEM_WIDTH     94
#define MAX_TEXT_LENGTH     50
#define RESTRICT_CHARACTERS     @":\\/.,[]{}()!;\"'*?<>|$%^&"

#define SCREEN_NAME_MAIN @"首頁"
#define SCREEN_NAME_RECORD @"錄音"
#define SCREEN_NAME_SAVE @"儲存選單"
#define SCREEN_NAME_DATE @"日期清單"
#define SCREEN_NAME_DATE_IN_MONTH @"當月日期清單"
#define SCREEN_NAME_CATEGORY @"群組清單"
#define SCREEN_NAME_CATEGORY_EDIT @"編輯自訂群組"
#define SCREEN_NAME_LOCATION @"地點清單"
#define SCREEN_NAME_LOCATION_EDIT @"編輯自訂地點"
#define SCREEN_NAME_FILE @"檔案列表"
#define SCREEN_NAME_FILE_EDIT @"編輯檔案資訊"
#define SCREEN_NAME_SEARCH @"搜尋"
#define SCREEN_NAME_PLAY @"播放"
#define SCREEN_NAME_SHARE @"分享"

@interface UIUtility : NSObject

+ (UIColor *)WHITE;
+ (UIColor *)BLUE;
+ (UIColor *)BLUE_D;
+ (UIColor *)BLUE_XD;
+ (UIColor *)GRAY_D;
+ (UIColor *)GRAY_M;
+ (UIColor *)GRAY_L;
+ (UIColor *)RED;

+ (CGRect)newPosition:(CGFloat)y source:(CGRect)frame;
+ (CGRect)newHeight:(CGFloat)height source:(CGRect)frame;

+ (NSDateFormatter *)dateFormatterYYMMdd;
+ (NSDateFormatter *)dateFormatterYYMM;
+ (NSDateFormatter *)dateFormatterMMMM_dd;
+ (NSDateFormatter *)dateFormatterYYYY_MMMM;
+ (NSDateFormatter *)dateFormatterEEEE;
+ (NSDateFormatter *)dateFormatterHHmmss;
+ (NSDateFormatter *)dateFormatterYYMMddHHmmss;
+ (NSDateFormatter *)dateFormatterhhmm_a;

@end
