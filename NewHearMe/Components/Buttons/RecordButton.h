//
//  RecordButton.h
//  NewHearMe
//
//  Created by IUILAB on 2014/10/1.
//  Copyright (c) 2014年 iuilab. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RecordButton : UIButton

@property (strong, nonatomic) UIImageView *iconImage;
@property (strong, nonatomic) UILabel *sideLabel;
@property (strong, nonatomic) UILabel *leftLabel;
@property (strong, nonatomic) UIProgressView *progressBar;

- (void)setupButton;
- (void)setSideLabelText:(NSString*)text;
- (void)setProgressValue:(float)value;
- (void)setProgressHidden:(BOOL)hidden;

@end
