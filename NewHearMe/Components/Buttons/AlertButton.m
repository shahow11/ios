//
//  AlertButton.m
//  NewHearMe
//
//  Created by IUILAB on 2014/9/29.
//  Copyright (c) 2014年 iuilab. All rights reserved.
//

#import "AlertButton.h"

@implementation AlertButton

+ (id)button {
    AlertButton *button = [super buttonWithType:UIButtonTypeCustom];
    [button setupButton];
    return button;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupButton];
    }
    return self;
}

- (void)setupButton
{
    self.layer.cornerRadius = 4;
    self.layer.opacity = 0.9;
    self.backgroundColor = [UIColor clearColor];
    [self.titleLabel setFont:[UIFont systemFontOfSize:17]];
    [self setTitleColor:[UIColor colorWithRed:51.0/255.0 green:51.0/255.0 blue:51.0/255.0 alpha:1.0] forState:UIControlStateNormal];
    [self.titleLabel setIsAccessibilityElement:NO];
}

- (void) setHighlighted:(BOOL)highlighted {
    [super setHighlighted:highlighted];
    
    if (highlighted) {
        self.backgroundColor = [UIColor colorWithRed:194.0/255.0 green:196.0/255.0 blue:204.0/255.0 alpha:1.0];
    }
    else {
        self.backgroundColor = [UIColor clearColor];
    }
}

- (void)setUserInteractionEnabled:(BOOL)userInteractionEnabled {
    [super setUserInteractionEnabled:userInteractionEnabled];
    if (userInteractionEnabled) {
        self.backgroundColor = [UIColor clearColor];
        [self setTitleColor:[UIColor colorWithRed:51.0/255.0 green:51.0/255.0 blue:51.0/255.0 alpha:1.0] forState:UIControlStateNormal];
        
    } else {
        self.backgroundColor = [UIColor colorWithRed:194.0/255.0 green:196.0/255.0 blue:204.0/255.0 alpha:1.0];
        [self setTitleColor:[UIColor colorWithRed:128.0/255.0 green:128.0/255.0 blue:128.0/255.0 alpha:1.0] forState:UIControlStateNormal];
    }
}

@end
