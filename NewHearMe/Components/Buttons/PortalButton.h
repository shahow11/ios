//
//  PortalButton.h
//  NewHearMe
//
//  Created by IUILab Air on 2016/3/5.
//  Copyright © 2016年 IUILAB. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PortalButton : UIButton

@property (strong, nonatomic) UILabel *mainLabel;
@property (strong, nonatomic) UILabel *sideLabel;
@property (strong, nonatomic) NSString *voiceText;

- (void)setLabelsWithWidth:(CGFloat)width height:(CGFloat)height;

@end
