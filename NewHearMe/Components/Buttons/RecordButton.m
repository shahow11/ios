//
//  RecordButton.m
//  NewHearMe
//
//  Created by IUILAB on 2014/10/1.
//  Copyright (c) 2014年 iuilab. All rights reserved.
//

#import "RecordButton.h"
#import "UIUtility.h"

@implementation RecordButton

+ (id)button {
    RecordButton *button = [super buttonWithType:UIButtonTypeCustom];
    [button setupButton];
    return button;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupButton];
    }
    return self;
}

- (void)setupButton
{
    self.backgroundColor = [UIColor clearColor];
    self.layer.opacity = 0.9;
    [self setTitleColor:[UIUtility GRAY_M] forState:UIControlStateNormal];
    [self.titleLabel setFont:[UIFont systemFontOfSize:17]];
    [self.titleLabel setIsAccessibilityElement:NO];
    
    self.sideLabel = [[UILabel alloc] initWithFrame:CGRectMake(207, 12, 80, 20)];
    [self.sideLabel setBackgroundColor:[UIColor clearColor]];
    [self.sideLabel setFont:[UIFont systemFontOfSize:17]];
    [self.sideLabel setTextColor:[UIUtility WHITE]];
    [self.sideLabel setTextAlignment:NSTextAlignmentRight];
    [self.sideLabel setIsAccessibilityElement:NO];
    
    self.leftLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, 5, 45, 32)];
    [self.leftLabel setFont:[UIFont systemFontOfSize:15]];
    [self.leftLabel setBackgroundColor:[UIColor clearColor]];
    [self.leftLabel setTextColor:[UIUtility GRAY_M]];
    [self.leftLabel setText:NSLocalizedString(@"Intro", nil)];
    [self.leftLabel setIsAccessibilityElement:NO];
    
    self.iconImage = [[UIImageView alloc] initWithFrame:CGRectMake(12, 13, 13, 18)];
    [self.iconImage setImage:[UIImage imageNamed:@"name_b.png"]];
    [self.iconImage setIsAccessibilityElement:NO];
    
    self.progressBar = [[UIProgressView alloc] initWithFrame:CGRectMake(0, self.frame.size.height - 3, self.frame.size.width, 2)];
    self.progressBar.trackTintColor = [UIUtility WHITE];
    self.progressBar.progressTintColor = [UIUtility BLUE_D];
    self.progressBar.transform = CGAffineTransformMakeScale(1.0f, 2.5f);
    
    [self.progressBar setIsAccessibilityElement:NO];
    [self.progressBar setHidden:YES];
    
    [self setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    [self setContentEdgeInsets:UIEdgeInsetsMake(0, 70, 0, 0)];
    
    [self addSubview:self.leftLabel];
    [self addSubview:self.sideLabel];
    [self addSubview:self.progressBar];
}

- (void) setHighlighted:(BOOL)highlighted {
    [super setHighlighted:highlighted];
    
    if (highlighted) {
        self.backgroundColor = [UIUtility RED];
        [self setTitleColor:[UIUtility WHITE] forState:UIControlStateNormal];
        [self.iconImage setImage:[UIImage imageNamed:@"name_w.png"]];
    }
    else {
        self.backgroundColor = [UIColor clearColor];
        [self setTitleColor:[UIUtility GRAY_M] forState:UIControlStateNormal];
        [self.iconImage setImage:[UIImage imageNamed:@"name_b.png"]];
    }
}

- (void)setSideLabelText:(NSString*)text
{
    if (self.sideLabel) {
        [self.sideLabel setText:text];
    }
}

- (void)setProgressValue:(float)value
{
    if (self.progressBar) {
        self.progressBar.progress = value;
    }
}

- (void)setProgressHidden:(BOOL)hidden
{
    if (self.progressBar) {
        [self.progressBar setHidden:hidden];
    }
}

@end
