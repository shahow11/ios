//
//  PortalButton.m
//  NewHearMe
//
//  Created by IUILab Air on 2016/3/5.
//  Copyright © 2016年 IUILAB. All rights reserved.
//

#import "PortalButton.h"
#import "UIUtility.h"

@implementation PortalButton

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
    }
    return self;
}

- (void)setLabelsWithWidth:(CGFloat)width height:(CGFloat)height
{
    [self setBackgroundColor:[UIUtility WHITE]];
    
    CGFloat labelMargin = (height - 28 - 18 - 3) / 2;
    self.mainLabel = [[UILabel alloc] init];
    [self.mainLabel setFrame:CGRectMake(16, labelMargin, 170, 28)];
    [self.mainLabel setFont:[UIFont systemFontOfSize:25]];
    [self.mainLabel setBackgroundColor:[UIColor clearColor]];
    self.mainLabel.textColor = [UIUtility BLUE_D];
    self.mainLabel.isAccessibilityElement = NO;
    
    self.sideLabel = [[UILabel alloc] init];
    [self.sideLabel setFrame:CGRectMake(18, labelMargin + 28 + 3, 280, 18)];
    [self.sideLabel setFont:[UIFont systemFontOfSize:15]];
    [self.sideLabel setBackgroundColor:[UIColor clearColor]];
    self.sideLabel.textColor = [UIUtility GRAY_D];
    self.sideLabel.isAccessibilityElement = NO;
    
    UIImageView *arrow = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"arrow_index.png"]];
    [arrow setFrame:CGRectMake(width - 26, (height - 22) / 2, 10, 22)];
    [arrow setIsAccessibilityElement:NO];
    
    [self addSubview:self.mainLabel];
    [self addSubview:self.sideLabel];
    [self addSubview:arrow];
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Draw seperate line
    [[UIColor colorWithRed:128/255.0 green:128/255.0 blue:128/255.0 alpha:1.0] set];
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetLineWidth(context, 0.5);
    CGContextSetLineJoin(context, kCGLineJoinRound);
    CGContextMoveToPoint(context, 0, self.frame.size.height);
    CGContextAddLineToPoint(context, self.frame.size.width, self.frame.size.height);
    CGContextStrokePath(context);
}

- (void) setHighlighted:(BOOL)highlighted {
    [super setHighlighted:highlighted];
    
    if (highlighted) {
        self.backgroundColor = [UIUtility GRAY_L];
    }
    else {
        self.backgroundColor = [UIUtility WHITE];
    }
}

#pragma mark - Accessibility

- (NSString *)accessibilityLabel
{
    if ([self.voiceText isEqualToString:@""]) {
        self.voiceText = self.sideLabel.text;
    }
    return [NSString stringWithFormat:@"%@ %@, %@", self.mainLabel.text, NSLocalizedString(@"List", nil), self.voiceText];
}

- (NSString *)accessibilityHint
{
    return NSLocalizedString(@"Double tap to open", nil);
}

-(NSString *)accessibilityValue
{
    return @"";
}

@end
