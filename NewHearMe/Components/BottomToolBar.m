//
//  BottomToolBar.m
//  NewHearMe
//
//  Created by IUILab Air on 2016/3/6.
//  Copyright © 2016年 IUILAB. All rights reserved.
//

#import "BottomToolBar.h"

@implementation BottomToolBar

- (id)initWithCoder:(NSCoder *)aDecoder
{
    if ((self = [super initWithCoder:aDecoder])) {
        UIView *nibView = [[[NSBundle mainBundle] loadNibNamed:@"BottomToolBar" owner:self options:nil] objectAtIndex:0];
        nibView.frame = CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, nibView.frame.size.height);
        
        [self.btnHome setTitle:NSLocalizedString(@"Home", nil)];
        [self.btnHelp setTitle:NSLocalizedString(@"Help", nil)];
        
        [self addSubview:nibView];
    }
    return self;
}

@end
