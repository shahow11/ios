//
//  ListTableViewCell.m
//  NewHearMe
//
//  Created by IUILab Air on 2016/3/6.
//  Copyright © 2016年 IUILAB. All rights reserved.
//

#import "ListTableViewCell.h"

@implementation ListTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (IBAction)btnPreviewClicked:(id)sender {
    if ([self.btnPreview.imageView.image isEqual:[UIImage imageNamed:@"item_play_btn.png"]]) {
        if (self.delegate && [self.delegate respondsToSelector:@selector(playCellList:)])
            [self.delegate playCellList:self];
    } else {
        if (self.delegate && [self.delegate respondsToSelector:@selector(cancelPlay)])
            [self.delegate cancelPlay];
    }
}

#pragma mark - Accessibility

- (NSString*)accessibilityLabel
{
    return @"";
}

- (void)accessibilityElementDidBecomeFocused
{
    self.isAccessibilityFocused = YES;
    
    NSString* itemTitle = [NSString stringWithFormat:@"%@ , %@", self.lblTitle.text, self.lblSubTitle.text];
    if (self.voiceTitle)
        itemTitle = self.voiceTitle;

    UIAccessibilityPostNotification(UIAccessibilityAnnouncementNotification, itemTitle);
    
    self.focusTime = 0;
    self.timer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(ticking) userInfo:nil repeats:YES];
}

- (void)accessibilityElementDidLoseFocus
{
    self.isAccessibilityFocused = NO;
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(cancelPlay)])
        [self.delegate cancelPlay];
    
    if (self.timer) {
        [self.timer invalidate];
        self.timer = nil;
    }
    
    // GA
  //  [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:@"tracking" action:@"focus_on_cell" label:self.trackTag value:[NSNumber numberWithInt:self.focusTime]] build]];
}

- (void)ticking
{
    self.focusTime ++;
}

@end
