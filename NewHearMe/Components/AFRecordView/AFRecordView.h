//
//  AFRecordView.h
//  HearMe
//
//  Created by iuilab on 13/8/23.
//  Copyright (c) 2013年 iuilab. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface AFRecordView : UIView

- (void)clearMeterItem;
- (void)addSoundMeterItem:(int)lastValue;

@end
