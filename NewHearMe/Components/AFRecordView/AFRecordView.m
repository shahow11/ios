//
//  AFRecordView.m
//  HearMe
//
//  Created by iuilab on 13/8/23.
//  Copyright (c) 2013年 iuilab. All rights reserved.
//

#import "AFRecordView.h"

#define START_RED           0
#define START_GREEN         26
#define START_BLUE          80
#define END_RED             80
#define END_GREEN           200
#define END_BLUE            255

@interface AFRecordView ()
{
    NSMutableArray *metersArray;
    int metersMaxCount;
}

@end

@implementation AFRecordView

- (void)awakeFromNib
{
    [super awakeFromNib];
    if (self) {
        self.contentMode = UIViewContentModeRedraw;
        
        // 初始設定
        metersMaxCount = [[UIScreen mainScreen] bounds].size.height / 3;
        metersArray = [NSMutableArray new];
        for (int i = 0; i < metersMaxCount; i++) {
            [metersArray addObject:@(0)];
        }
    }
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Draw seperate line
    [[UIColor colorWithRed:END_RED/255.0 green:END_GREEN/255.0 blue:END_BLUE/255.0 alpha:1.0] set];
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetLineWidth(context, 0.5);
    CGContextSetLineJoin(context, kCGLineJoinRound);
    CGContextMoveToPoint(context, 0, self.frame.size.height);
    CGContextAddLineToPoint(context, self.frame.size.width, self.frame.size.height);
    CGContextStrokePath(context);
    
    // Draw sound meter wave
    int baseLine = self.frame.size.width / 2;
    float maxLengthOfWave = (self.frame.size.width - 20) / 2;
    float maxValueOfMeter = 60;
    CGFloat newY = self.frame.size.height - 2;
    for(int x = metersMaxCount - 1; x >= 0; x--)
    {
        if (labs([metersArray[x] integerValue]) > 0) {
            float enlargeTimes = 1;
            
            // Get line's percentage of position
            double percent = newY / self.frame.size.height;
            [[self colorAtPercent:percent] set];
            
            CGContextRef context = UIGraphicsGetCurrentContext();
            CGContextSetLineWidth(context, 1.0);
            CGContextSetLineJoin(context, kCGLineJoinRound);
            
            int lineWidth = (int)roundf(((maxValueOfMeter - labs([metersArray[x] integerValue])) / maxValueOfMeter) * maxLengthOfWave * enlargeTimes);
            
            CGContextMoveToPoint(context, baseLine - lineWidth, newY);
            CGContextAddLineToPoint(context, baseLine + lineWidth, newY);
            CGContextStrokePath(context);
            
            newY -= 2;
        }
    }
}

- (void)shiftSoundMeterLeft {
    for (int i = 0; i < metersMaxCount - 1; i++) {
        metersArray[i] = metersArray[i+1];
    }
}

- (void)clearMeterItem
{
    for (int i = 0; i < metersMaxCount; i++) {
        metersArray[i] = @(0);
    }

    [self setNeedsDisplay];
}

- (void)addSoundMeterItem:(int)lastValue {
    [self shiftSoundMeterLeft];
    metersArray[metersMaxCount - 1] = @(lastValue);
    
    [self setNeedsDisplay];
}

- (UIColor *)colorAtPercent:(double)percent
{
    double resultRed = START_RED + percent * (END_RED - START_RED);
    double resultGreen = START_GREEN + percent * (END_GREEN - START_GREEN);
    double resultBlue = START_BLUE + percent * (END_BLUE - START_BLUE);
    return [UIColor colorWithRed:resultRed/255 green:resultGreen/255 blue:resultBlue/255 alpha:1.0];
}

@end
