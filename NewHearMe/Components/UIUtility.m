//
//  Utility.m
//  NewHearMe
//
//  Created by IUILab Air on 2016/3/6.
//  Copyright © 2016年 IUILAB. All rights reserved.
//

#import "UIUtility.h"

@implementation UIUtility

+ (UIColor *)WHITE {
    return [self colorWithHexString:@"F2F3F6"];
}

+ (UIColor *)BLUE {
    return [self colorWithHexString:@"00AFFF"];
}

+ (UIColor *)BLUE_D {
    return [self colorWithHexString:@"00508D"];
}

+ (UIColor *)BLUE_XD {
    return [self colorWithHexString:@"001A50"];
}

+ (UIColor *)GRAY_D {
    return [self colorWithHexString:@"333333"];
}

+ (UIColor *)GRAY_M {
    return [self colorWithHexString:@"808080"];
}

+ (UIColor *)GRAY_L {
    return [self colorWithHexString:@"C2C4CC"];
}

+ (UIColor *)RED {
    return [self colorWithHexString:@"FF3E34"];
}

+ (UIColor *)colorWithHexString:(NSString*)hex {
    NSString *cString = [[hex stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    
    // String should be 6 or 8 characters
    if ([cString length] < 6) return [UIColor grayColor];
    
    // strip 0X if it appears
    if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];
    
    if ([cString length] != 6) return  [UIColor grayColor];
    
    // Separate into r, g, b substrings
    NSRange range;
    range.location = 0;
    range.length = 2;
    NSString *rString = [cString substringWithRange:range];
    
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    
    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f)
                           green:((float) g / 255.0f)
                            blue:((float) b / 255.0f)
                           alpha:1.0f];
}

+ (CGRect)newPosition:(CGFloat)y source:(CGRect)frame {
    return CGRectMake(frame.origin.x, y, frame.size.width, frame.size.height);
}

+ (CGRect)newHeight:(CGFloat)height source:(CGRect)frame {
    return CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, height);
}

+ (NSDateFormatter *)dateFormatterYYMMdd {
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"YYMMdd"];
    return dateFormatter;
}

+ (NSDateFormatter *)dateFormatterYYMM {
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"YYMM"];
    return dateFormatter;
}

+ (NSDateFormatter *)dateFormatterMMMM_dd {
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:[NSDateFormatter dateFormatFromTemplate:@"MMMM dd" options:0 locale:[NSLocale currentLocale]]];
    return dateFormatter;
}

+ (NSDateFormatter *)dateFormatterYYYY_MMMM {
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setLocale:[NSLocale currentLocale]];
    [dateFormatter setDateFormat:[NSDateFormatter dateFormatFromTemplate:@"YYYY MMMM" options:0 locale:[NSLocale currentLocale]]];
    return dateFormatter;
}

/// Week day
+ (NSDateFormatter *)dateFormatterEEEE {
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setLocale:[NSLocale currentLocale]];
    [dateFormatter setDateFormat:@"EEEE"];
    return dateFormatter;
}

+ (NSDateFormatter *)dateFormatterHHmmss {
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HHmmss"];
    return dateFormatter;
}

+ (NSDateFormatter *)dateFormatterYYMMddHHmmss {
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"YYMMddHHmmss"];
    return dateFormatter;
}

+ (NSDateFormatter *)dateFormatterhhmm_a {
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:[NSDateFormatter dateFormatFromTemplate:@"hh:mm a" options:0 locale:[NSLocale currentLocale]]];
    return dateFormatter;
}

@end
