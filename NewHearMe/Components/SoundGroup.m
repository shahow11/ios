//
//  SoundGroup.m
//  NewHearMe
//
//  Created by IUILab Air on 2016/3/5.
//  Copyright © 2016年 IUILAB. All rights reserved.
//

#import "SoundGroup.h"

@implementation SoundGroup

+ (id)groupByID:(int)gid name:(NSString*)name seq:(int)sequence
{
    SoundGroup *newGroup = [[self alloc] init];
    newGroup.gid = gid;
    newGroup.name = name;
    newGroup.seq = sequence;
    
    return newGroup;
}

@end
