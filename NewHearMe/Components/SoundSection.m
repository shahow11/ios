//
//  SoundSection.m
//  NewHearMe
//
//  Created by IUILab Air on 2016/3/5.
//  Copyright © 2016年 IUILAB. All rights reserved.
//

#import "SoundSection.h"

@implementation SoundSection

+ (id)sectionOfFile:(int)sid vid:(int)vid name:(NSString*)name timestamp:(NSTimeInterval)timestamp
{
    SoundSection *newSection = [[self alloc] init];
    newSection.sid = sid;
    newSection.vid = vid;
    newSection.name = name;
    newSection.timestamp = timestamp;
    
    return newSection;
}

@end
