//
//  AppDelegate.m
//  NewHearMe
//
//  Created by AfraTsai on 2016/2/15.
//  Copyright © 2016年 IUILAB. All rights reserved.
//

#import "AppDelegate.h"
#import <DropboxSDK/DropboxSDK.h>

/** Google Analytics configuration constants **/
/*
static NSString *const kGaPropertyId = @"UA-56773557-3";
static NSString *const kTrackingPreferenceKey = @"allowTracking";
static BOOL const kGaDryRun = NO;
static int const kGaDispatchPeriod = 30;
*/
@import Firebase;

@interface AppDelegate ()

- (void)initializeGoogleAnalytics;

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    // Optional: configure GAI options.
 //   GAI *gai = [GAI sharedInstance];
  //  gai.trackUncaughtExceptions = YES;  // report uncaught exceptions
    //gai.logger.logLevel = kGAILogLevelVerbose;  // remove before app release
    
    // Dropbox configering
    DBSession *dbSession = [[DBSession alloc] initWithAppKey:@"5ko5aj6ene9fcog" appSecret:@"1icra00oxpxahqn" root:kDBRootAppFolder];
    [DBSession setSharedSession:dbSession];
    
    NSString *docPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
    NSString *dbPath = [docPath stringByAppendingPathComponent:@"HearMe.db"];
    NSError *error = nil;
    
    // 刪除已有資料庫（for 測試）
    //if (![[NSFileManager defaultManager] removeItemAtPath:dbPath error:&error]) {NSLog(@"Error: %@", error);}
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:dbPath]) {
        // database doesn't exist in your library path... copy it from the bundle
        NSString *sourcePath = [[NSBundle mainBundle] pathForResource:@"HearMe" ofType:@"db"];
        
        if (![[NSFileManager defaultManager] copyItemAtPath:sourcePath toPath:dbPath error:&error]) {
            NSLog(@"Copy DB file Error: %@", error);
        }
    } else {
        //NSLog(@"db file exists.");
    }
    
    [self initializeGoogleAnalytics];
    [FIRApp configure];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
  //  [GAI sharedInstance].optOut =
    //![[NSUserDefaults standardUserDefaults] boolForKey:kTrackingPreferenceKey];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (void)initializeGoogleAnalytics {

    //[[GAI sharedInstance] setDispatchInterval:kGaDispatchPeriod];
    //[[GAI sharedInstance] setDryRun:kGaDryRun];
    //self.tracker = [[GAI sharedInstance] trackerWithTrackingId:kGaPropertyId];
}

@end
